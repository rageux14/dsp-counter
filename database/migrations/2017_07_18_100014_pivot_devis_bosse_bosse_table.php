<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotDevisBosseBosseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devis_bosse_bosse', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id')->unsigned();
            
            $table->integer('devis_bosse_id')->unsigned();
            $table->foreign('devis_bosse_id')->references('id')->on('devis_bosse')->onDelete('cascade');
            
            $table->integer('bosse_id')->unsigned();
            $table->foreign('bosse_id')->references('id')->on('bosse')->onDelete('cascade');


            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devis_bosse_bosse');
    }
}
