<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBosseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bosse', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id')->unsigned();

            $table->integer('partie_voiture_id')->unsigned();
            $table->foreign('partie_voiture_id')->references('id')->on('partie_voiture')->onDelete('cascade');

            $table->string('bosse1')->nullable();
            $table->string('bosse2')->nullable();
            $table->string('bosse3')->nullable();
            $table->boolean('dap')->default(false);

            $table->boolean('alu')->nullable();
            $table->string('ut')->nullable();
            $table->boolean('reserves')->default(false);
            $table->boolean('hs')->default(false);
            $table->boolean('tradi')->default(false);
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bosse');
    }
}
