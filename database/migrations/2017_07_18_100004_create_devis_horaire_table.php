<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevisHoraireTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devis_horaire', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id')->unsigned();

            $table->integer('devis_id')->unsigned();
            $table->foreign('devis_id')->references('id')->on('devis')->onDelete('cascade');

            $table->string('nb_heures');

            $table->string('total_dsp')->nullable();
            $table->string('degarnissage')->nullable();
            $table->string('frais_de_dossier')->nullable();

            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devis_horaire');
    }
}
