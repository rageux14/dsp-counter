<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartieVoitureForfaitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partie_voiture_forfait', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id')->unsigned();

            $table->integer('partie_voiture_id')->unsigned();
            $table->foreign('partie_voiture_id')->references('id')->on('partie_voiture')->onDelete('cascade');

            $table->integer('devis_horaire_id')->unsigned();
            $table->foreign('devis_horaire_id')->references('id')->on('devis_horaire')->onDelete('cascade');

            $table->string('prix')->nullable();
            $table->boolean('dap')->default(false);

            $table->boolean('alu')->default(false);
            $table->string('ut')->nullable();
            $table->boolean('reserves')->default(false);
            $table->boolean('hs')->default(false);
            $table->boolean('tradi')->default(false);
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partie_voiture_forfait');
    }
}
