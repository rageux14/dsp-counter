<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id')->unsigned();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('email')->nullable();
            $table->string('societe')->nullable();
            $table->string('adresse')->nullable();
            $table->string('CP')->nullable();
            $table->string('ville')->nullable();
            $table->string('nom')->nullable();
            $table->string('prenom')->nullable();
            $table->string('siren')->nullable();
            $table->string('telephone')->nullable();
            $table->string('taux_de_remise')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client');
    }
}
