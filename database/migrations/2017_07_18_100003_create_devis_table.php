<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devis', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id')->unsigned();

            $table->integer('voiture_id')->unsigned();
            $table->foreign('voiture_id')->references('id')->on('voiture')->onDelete('cascade');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('numero');
            $table->string('taux_horaire')->nullable();
            $table->string('total_ht')->nullable();
            $table->string('total_tva')->nullable();
            $table->string('total_ttc')->nullable();

            $table->string('frais_libelle')->nullable();
            $table->string('frais_prix')->nullable();

            $table->string('date_reparation')->nullable();
            
            $table->string('date_expertise')->nullable();
            
            $table->string('nom_expert')->nullable();

            $table->string('numero_sinistre')->nullable();

            $table->text('commentaires')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devis');
    }
}
