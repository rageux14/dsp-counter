<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoitureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voiture', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();

            $table->string('immat')->unique();


            $table->integer('voiture_modele_id');
            $table->foreign('voiture_modele_id')->references('id')->on('voiture_modele')->onDelete('cascade')->onUpdate('cascade');

            $table->string('assurance')->nullable();
            $table->string('kilometres')->nullable();
            $table->string('couleur')->nullable();
            $table->string('chassis')->nullable();
            $table->string('nom_client')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voiture');
    }
}
