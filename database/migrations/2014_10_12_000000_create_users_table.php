<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();

            $table->string('email')->unique();
            $table->string('password');
            $table->string('prenom')->nullable();
            $table->string('nom')->nullable();
            $table->string('societe')->nullable();
            $table->string('logo')->nullable();
            $table->string('adresse')->nullable();
            $table->string('cp')->nullable();
            $table->string('capital_social')->nullable();
            $table->string('ville')->nullable();
            $table->string('siren')->nullable();
            $table->string('telephone')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
