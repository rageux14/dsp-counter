<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotVoitureFactureLibreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voiture_facture_libre', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id')->unsigned();
            
            $table->integer('facture_libre_id')->unsigned();
            $table->foreign('facture_libre_id')->references('id')->on('facture_libre')->onDelete('cascade');
            
            $table->integer('voiture_id')->unsigned();
            $table->foreign('voiture_id')->references('id')->on('voiture')->onDelete('cascade');


            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voiture_facture_libre');
    }
}
