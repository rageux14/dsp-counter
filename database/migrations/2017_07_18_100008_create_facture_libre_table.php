<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactureLibreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facture_libre', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id')->unsigned();

            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('client')->onDelete('cascade');

            $table->integer('facture_id')->unsigned();
            $table->foreign('facture_id')->references('id')->on('facture')->onDelete('cascade');

            $table->string('frais_libelle')->nullable();
            $table->string('frais_prix')->nullable();
            
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facture_libre');
    }
}
