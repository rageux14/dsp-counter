<?php

use Illuminate\Database\Seeder;
use App\Client as Client;
use App\ClientVoiture as ClientVoiture;
use App\Voiture as Voiture;
use App\User as User;

class ScriptV1Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $voitures = Voiture::all();
        foreach ($voitures as $voiture) {
            //add user_id to voiture from client->user_id
            $clientVoiture = ClientVoiture::where('voiture_id',$voiture->id)->first();
            $client = Client::find($clientVoiture->client_id);
            $voiture->user_id = $client->user_id;
            // create slug for existing voiture
            $string=str_replace(' ', '',$voiture->immat);
            $string=str_replace('-', '',$string);
            $voiture->slug=strtoupper($string);
            $voiture->save();        
        }

    }
}
