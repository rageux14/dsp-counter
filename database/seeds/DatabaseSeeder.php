<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //users Table
        DB::table('users')->delete();
        DB::table('users')->insert([
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
            'prenom'  => 'Lucas',
            'nom' => 'Quinot',
            'societe' => 'Chirauto',
            'adresse' => '92 rue de montagny',
            'cp' => '69008',
            'ville' => 'Lyon',
            'siren' => '332984302',
            'id'=>1
        ]);
        DB::table('users')->insert([
            'nom' => str_random(10),
            'email' => 'secret@gmail.com',
            'password' => bcrypt('secret'),
            'prenom'  => 'Giovanni',
            'id'=>2
        ]);

        //voiture table
       $this->call(VoitureTableSeeder::class);

         //client table
        DB::table('client')->delete();
        DB::table('client')->insert([
            'email' => str_random(10).'@gmail.com',
            'prenom'  => 'Christian',
            'nom' => 'Sauvignet',
            'societe' => 'CTA Carrosserie',
            'adresse' => '84 rue de la republique',
            'cp' => '42530',
            'ville' => 'Lorette',
            'siren' => '332984302',
            'telephone' => '0684347382',
            'taux_de_remise' => '15',
            'user_id' => 1
        ]);
        DB::table('client')->insert([
            'email' => str_random(10).'@gmail.com',
            'prenom'  => 'Jacques',
            'nom' => 'Martin',
            'societe' => 'Garage de Lyon',
            'adresse' => '84 rue de la republique',
            'cp' => '42530',
            'ville' => 'Lorette',
            'siren' => '332984302',
            'telephone' => '0684347382',
            'taux_de_remise' => '20',
            'user_id' => 2
        ]);

        //partie voiture table
        DB::table('partie_voiture')->delete();
        DB::table('partie_voiture')->insert([
            'name' => 'aile_avant_gauche',
            'cat'  => 'gauche',
            'order' => 10
        ]);
        DB::table('partie_voiture')->insert([
            'name' => 'porte_avant_gauche',
            'cat'  => 'gauche',
            'order' => 11
        ]);
        DB::table('partie_voiture')->insert([
            'name' => 'porte_arriere_gauche',
            'cat'  => 'gauche',
            'order' => 12
        ]);
        DB::table('partie_voiture')->insert([
            'name' => 'aile_arriere_gauche',
            'cat'  => 'gauche',
            'order' => 13
        ]);
        DB::table('partie_voiture')->insert([
            'name' => 'montant_gauche',
            'cat'  => 'gauche',
            'order' => 14
        ]);
        DB::table('partie_voiture')->insert([
            'name' => 'aile_avant_droite',
            'cat'  => 'droite',
            'order' => 5
        ]);

        DB::table('partie_voiture')->insert([
            'name' => 'porte_avant_droite',
            'cat'  => 'droite',
            'order' => 6
        ]);

        DB::table('partie_voiture')->insert([
            'name' => 'porte_arriere_droite',
            'cat'  => 'droite',
            'order' => 7
        ]);

        DB::table('partie_voiture')->insert([
            'name' => 'aile_arriere_droite',
            'cat'  => 'droite',
            'order' => 8
        ]);
        DB::table('partie_voiture')->insert([
            'name' => 'montant_droit',
            'cat'  => 'droite',
            'order' => 9
        ]);
        DB::table('partie_voiture')->insert([
            'name' => 'capot_moteur',
            'cat'  => 'autre',
            'order' => 1
        ]);

        DB::table('partie_voiture')->insert([
            'name' => 'pavillon',
            'cat'  => 'autre',
            'order' => 2
        ]);
        DB::table('partie_voiture')->insert([
            'name' => 'hayon_superieur',
            'cat'  => 'autre',
            'order' => 3
        ]);
        DB::table('partie_voiture')->insert([
            'name' => 'hayon_inferieur',
            'cat'  => 'autre',
            'order' => 4
        ]);

        //devis table
        DB::table('devis')->delete();
        DB::table('devis')->insert([
            'voiture_id' => 1,
            'numero'  => '00045637',
            'taux_horaire' => '25',
            'total_ht' => '360',
            'total_tva' => '78',
            'total_ttc' => '538',
            'user_id' => 1
        ]);
        DB::table('devis')->insert([
            'voiture_id' => 2,
            'numero'  => '000313133',
            'taux_horaire' => '25',
            'total_ht' => '360',
            'total_tva' => '78',
            'total_ttc' => '538',
            'user_id' => 2
        ]);

        //devis_horaire table
        DB::table('devis_horaire')->delete();
        DB::table('devis_horaire')->insert([
            'devis_id' => 1,
            'nb_heures'  => '58'
        ]);

        //devis_bosse table
        DB::table('devis_bosse')->delete();
        DB::table('devis_bosse')->insert([
            'devis_id' => 2,
            'degarnissage'  => '158',
            'frais_de_dossier'  => '208',
            'dsp'  => '502',
        ]);

        //facture_statut table
        DB::table('facture_statut')->delete();
        DB::table('facture_statut')->insert([
            'order' => 1,
            'libelle'  => 'crée'
        ]);
        DB::table('facture_statut')->insert([
            'order' => 2,
            'libelle'  => 'généré'
        ]);
        DB::table('facture_statut')->insert([
            'order' => 3,
            'libelle'  => 'envoyée'
        ]);
        DB::table('facture_statut')->insert([
            'order' => 4,
            'libelle'  => 'réglement_partiel'
        ]);
        DB::table('facture_statut')->insert([
            'order' => 5,
            'libelle'  => 'réglement_complet'
        ]);

        //facture table
        DB::table('facture')->delete();
        DB::table('facture')->insert([
            'facture_statut_id' => 1,
            'numero'  => '00045637',
            'commentaires' => 'Facture correspondant au devis n°33410321',
            'total_ht' => '360',
            'total_tva' => '78',
            'total_ttc' => '538',
            'user_id' => 2
        ]);
        DB::table('facture')->insert([
            'facture_statut_id' => 2,
            'numero'  => '000313133',
            'commentaires' => 'Facture correspondant au devis n°33410323',
            'total_ht' => '360',
            'total_tva' => '78',
            'total_ttc' => '538',
            'user_id' => 1
        ]);

        //facture_devis table
        DB::table('facture_devis')->delete();
        DB::table('facture_devis')->insert([
            'devis_id' => 1,
            'facture_id' => 1
        ]);

        //facture_libre table
        DB::table('facture_libre')->delete();
        DB::table('facture_libre')->insert([
            'client_id' => 1,
            'facture_id' => 2
        ]);

        /*  BOSSE TABLE
        **
        **
        **   TO DO
        **
        **
         */

        /*  --------------------------------- TABLES PIVOTS ------------------------------------- */
        //client_voiture table
        DB::table('client_voiture')->delete();
        DB::table('client_voiture')->insert([
            'voiture_id' => 2,
            'client_id' => 2
        ]);
        DB::table('client_voiture')->insert([
            'voiture_id' => 1,
            'client_id' => 1
        ]);

        /*  DEVIS BOSSE BOSSE TABLE
        **
        **
        **   TO DO
        **
        **
         */


    }
}
