<?php

use Illuminate\Database\Seeder;

class VoitureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('voiture')->delete();

        DB::table('voiture')->insert([
            'immat' => '345-TE-23',
            'voiture_modele_id' => 5,
            'assurance'  => 'MAIF',
            'kilometres' => '350000',
            'couleur' => 'noire',
            'user_id'=> 1,
            'id'=>1
        ]);
        DB::table('voiture')->insert([
            'immat' => '353-GN-21',
            'voiture_modele_id' => 38,
            'assurance'  => 'Groupama',
            'kilometres' => '20000',
            'couleur' => 'verte',
            'user_id'=> 2,
            'id'=>2
        ]);
    }
}
