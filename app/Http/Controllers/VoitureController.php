<?php

namespace App\Http\Controllers;

use App\FactureDevis;
use App\FactureLibre;
use App\Library\Services\Slug;
use App\VoitureFactureLibre;
use Illuminate\Http\Request;
use App\Client as Client;
use App\Devis as Devis;
use App\DevisBosse as DevisBosse;
use App\Voiture as Voiture;
use App\Facture as Facture;
use App\VoitureModele as VoitureModele;
use App\ClientVoiture as ClientVoiture;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;

class VoitureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($clientId)
    {
        $client = Client::find($clientId);
        if (empty($client)) {
            return redirect()->route('client')->with('error', "Ce client n'existe pas !");
        }
        $user = Auth::user();
        if ($client->user_id != $user->id) {
            return redirect()->route('client')->with('error', "Vous n'êtes pas autorisé à accèder à ce fiche client!");
        }
        $clientVoitures = ClientVoiture::where('client_id', '=', $clientId)->orderBy('id', 'desc')->get();
        $voitures = array();
        foreach ($clientVoitures as $clientVoiture) {
            $voiture = Voiture::find($clientVoiture->voiture_id);
            $voitures[$voiture->id]['immat'] = $voiture->immat;
            $voitures[$voiture->id]['id'] = $voiture->id;
            $voitureModele = VoitureModele::find($voiture->voiture_modele_id);
            $voitures[$voiture->id]['marque'] = $voitureModele->rappel_marque;
            $voitures[$voiture->id]['modele'] = $voitureModele->rappel_modele;
            $voitures[$voiture->id]['assurance'] = $voiture->assurance;
            $voitures[$voiture->id]['kilometres'] = $voiture->kilometres;
            $voitures[$voiture->id]['couleur'] = $voiture->couleur;
            //test if devis exist
            $devi = Devis::where('voiture_id', $voiture->id)->first();
            if (isset($devi)) {
                $voitures[$voiture->id]['devis'] = $devi->id;
                //check type
                $devisBosseExist = DevisBosse::where('devis_id', $devi->id)->first();
                if (isset($devisBosseExist)) {
                    $voitures[$voiture->id]['devis_type'] = 'bosse';
                } else {
                    $voitures[$voiture->id]['devis_type'] = 'horaire';
                }
            } else {
                $voitures[$voiture->id]['devis'] = null;
            }
        }
        $devis = Devis::getAllDevisByClient($clientId);
        $factures = Facture::getAllFactureByClient($clientId);
        return view('Voiture/voiture', ['voitures' => $voitures, 'client' => $client, 'devis' => $devis, 'factures' => $factures]);
    }

    public function indexCreate($id)
    {
        $marques = VoitureModele::getAllMarques();
        $clients= Client::where('user_id', $id)->get()->sortBy('societe');
        return view(' Voiture/voiture-create', [
            'id' => $id,
            'marques' => $marques,
            'clients'=>$clients
        ]);
    }

    public function IndexCreateOther($route)
    {
        if ($route == 'facturelibre') {
            return view('Voiture/client-create', ['route' => $route]);
        }
        return view('Client/client-create');
    }

    public function getVoiture($clientId, $voitureId, $previous="")
    {
        //test if the last parameter of the url is an integer|nullable
        if (is_numeric($clientId)) {
            $client = Client::find($clientId);
            if (!isset($client)) {
                return redirect()->route('client')->with('error', "Ce client n'existe pas !");
            }
            if (is_numeric($voitureId)) {
                $voiture = Voiture::find($voitureId);
                if (!isset($voiture)) {
                    return redirect()->route('voiture', ['id' => $clientId])->with('error', "Cette voiture n'existe pas !");
                }
            }
            $user = Auth::user();
            if ($client->user_id != $user->id) {
                return redirect()->route('client')->with('error', "Vous n'êtes pas autorisé à modifier cette voiture!");
            }
            $voitureModele = VoitureModele::find($voiture->voiture_modele_id);
            $selectMarque = $voitureModele->rappel_marque;
            $modeles = VoitureModele::select('rappel_modele', 'rappel_marque', 'id')->groupBy('rappel_modele', 'rappel_marque')->having('rappel_marque', $selectMarque)->get();
            $marques = VoitureModele::getAllMarques();
            $clients= Client::where('user_id', $user->id)->get()->sortBy('societe');
            return view('Voiture/voiture-modify', [
                'client' => $client,
                'clients'=>$clients,
                'voiture' => $voiture,
                'modeles' => $modeles,
                'marques' => $marques,
                'selectMarque' => $selectMarque
            ]);
        }
        $user = Auth::user();
        return redirect()->route('voiture', ['id' => $clientId]);
    }

    public function create(Request $data, Slug $slug, $previous = "")
    {

        $user = Auth::user();
        //data validation
        $validator = Validator::make($data->all(), [
            'immat' => 'required|string|max:10',
            'assurance' => 'string|nullable',
            'kilometres' => 'numeric|nullable',
            'couleur' => 'string|nullable',
            'nom_client' => 'string|nullable',
            'chassis' => 'string|nullable',
        ]);
        if ($validator->fails()) {
            //return errors & the old user input's
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        } else {
            //test select value
            $voitureModeleId = $data->get('select_modele');
            if (!isset($voitureModeleId) || $voitureModeleId == 0) {
                return redirect()->back()->with('error', "Veuillez selectionner un modèle de voiture ! ")->withInput();
            }
            // test unique immat
            $voiture = Voiture::where('immat', $data->immat)->first();
            if (isset($voiture)) {
                return redirect()->back()->with('error', "Cette immatriculation est déjà utilisée ! ")->withInput();
            }

            //create an instance in the BDD
            $user = Auth::user();
            $voiture = Voiture::create([
                'immat' => $data->immat,
                'voiture_modele_id' => $voitureModeleId,
                'assurance' => $data->assurance,
                'kilometres' => $data->kilometres,
                'nom_client' => $data->nom_client,
                'chassis' => $data->chassis,
                'couleur' => $data->couleur,
                'created_at' => date("d.m.y"),
                'user_id' => $user->id,
                'slug' => $slug->slugify($data->immat)
            ]);
            // creation client_voiture en ajoutant le client dans le formulaire de la voiture

            if ($data->get('client')!='null' && $data->get('client')!=null){
                $client=Client::where('societe', $data->get('client'))->first();
                $client_voiture= ClientVoiture::create([
                    'client_id'=>$client->id,
                    'voiture_id'=>$voiture->id,
                    'created_at' => date("d.m.y"),
                ]);
            } else if (!empty($data->client_id)){
                // revenir a voiture index sans crée client_voiture
                $id = $data->client_id;
                ClientVoiture::create([
                    'client_id' => $id,
                    'voiture_id' => $voiture->id,
                    'created_at' => date("d.m.y"),
                ]);
                    
            } else {
                return redirect()->back()->with('error', 'Veuillez sélectionner un client')->withInput();
            }

            if ($previous==='voiture') {
                return redirect()->action('VoitureController@show', ['voitureId'=>$voiture->id])->with('status', 'Voiture créée !');
            }
            
            return redirect()->action('VoitureController@show', ['voitureId'=>$voiture->id])->with('status', 'Voiture créée !');
        }
    }

    public function modify(Request $data)
    {
        //data validation
        $validator = Validator::make($data->all(), [
            'immat' => 'required|string|max:10',
            'assurance' => 'string|nullable',
            'kilometres' => 'numeric|nullable',
            'couleur' => 'string|nullable',
            'nom_client' => 'string|nullable',
            'chassis' => 'string|nullable'
        ]);
        if ($validator->fails()) {
            //return errors & the old user input's
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        } else {
            //test select value
            $voitureModeleId = $data->get('select_modele');
            if (!isset($voitureModeleId) || $voitureModeleId == 0) {
                return redirect()->back()->with('error', "Veuillez selectionner un modèle de voiture ! ")->withInput();
            }
            $voiture = Voiture::find($data->voiture_id);
            $voiture->immat = $data->immat;
            $voiture->assurance = $data->assurance;
            $voiture->kilometres = $data->kilometres;
            $voiture->chassis = $data->chassis;
            $voiture->nom_client = $data->nom_client;
            $voiture->couleur = $data->couleur;
            $voiture->voiture_modele_id = $voitureModeleId;

            if ($voiture->save()) {
                if ( ClientVoiture::where('voiture_id', $data->voiture_id)->first()===null ){
                    ClientVoiture::create([
                        'client_id' => $data->client_id,
                        'voiture_id' => $data->voiture_id,
                        'created_at' => date("d.m.y"),
                    ]);
                }else{
                    $clientVoiture=ClientVoiture::where('voiture_id', $data->voiture_id)->first();
                    $clientVoiture->client_id = $data->client_id;
                    $clientVoiture->voiture_id = $data->voiture_id;
                    $clientVoiture->save();
                }

                return redirect()->action('VoitureController@show', ['voitureId'=>$data->voiture_id])->with('status', 'Voiture enregistrée!');
            }
            return redirect()->back()->with('error', "Problème lors de l'enregistrement...");
        }
    }

    public function delete($clientId, $voitureId, $previous = '')
    {
        //test if the last parameter of the url is an integer
        if (is_numeric($clientId) && is_numeric($voitureId)) {
            $voiture = Voiture::find($voitureId);
            if (!isset($voiture)) {
                if ($previous === 'index') {
                    return redirect()->route('voiture-index', ['id' => $clientId])->with('error', "Cette voiture n'existe pas !");
                }
                return redirect()->route('voiture', ['id' => $clientId])->with('error', "Cette voiture n'existe pas !");
            }
            $client = Client::find($clientId);
            $user = Auth::user();
            $devis = Devis::where('voiture_id', $voiture->id)->first();
            if (isset($devis)) {
                if ($previous === 'index') {
                    return redirect()->route('voiture-index', ['id' => $clientId])->with('error', "Impossible de supprimer cette voiture, elle est utilisée pour un devis !");

                }
                return redirect()->route('voiture', ['id' => $clientId])->with('error', "Impossible de supprimer cette voiture, elle est utilisée pour un devis !");
            }
            if ($client->user_id != $user->id) {
                if ($previous === 'index') {
                    return redirect()->route('voiture-index', ['id' => $clientId])->with('error', "Vous n'êtes pas autorisé à supprimer cette fiche voiture!");

                }
                return redirect()->route('voiture', ['id' => $clientId])->with('error', "Vous n'êtes pas autorisé à supprimer cette fiche voiture!");
            }
            $clientVoiture = ClientVoiture::where('client_id', $clientId)->where('voiture_id', $voiture->id)->first();
            if ($clientVoiture != null) {
                $clientVoiture->delete();
            }
            if ($voiture->delete()) {
                if ($previous === 'index') {
                    return redirect()->route('voiture-index', ['id' => $clientId])->with('status', 'Voiture effacée !');

                }
                return redirect()->route('voiture', ['id' => $clientId])->with('status', 'Voiture effacée !');
            }
            if ($previous === 'index') {
                return redirect()->route('voiture-index', ['id' => $clientId])->with('error', "Problème lors de l'effacement");

            }
            return redirect()->route('voiture', ['id' => $clientId])->with('error', "Problème lors de l'effacement");

            if ($previous === 'index') {
                return redirect()->route('voiture-index', ['id' => $clientId])->with('error', "Problème lors de l'effacement");
            }
            return redirect()->route('voiture', ['id' => $clientId])->with('error', "Problème lors de l'effacement");
        }
        if ($previous === 'index') {
            return redirect()->action('voiture-index ', ['id' => $clientId]);

        }
        return redirect()->route('voiture', ['id' => $clientId]);
    }

    public function getModeleByMarque()
    {
        $marque = $_GET['marque'];
        $modeles = VoitureModele::select('rappel_modele', 'rappel_marque', 'id')->groupBy('rappel_modele', 'rappel_marque')->having('rappel_marque', $marque)->get();
        return $modeles;
    }

    public function index2()
    {
        $user = Auth::user();
        $data = [];
        $voitures = Voiture::getAllVoitureByUser($user->id);
        foreach ($voitures as $voiture) {
            $clientVoiture = ClientVoiture::where('voiture_id', $voiture['id'])->first();
            if ($clientVoiture === null) {
                $data[$voiture['id']] = [
                    'client' => false,
                    'immat' => $voiture['immat'],
                    'assurance' => $voiture['assurance'],
                    'kilometres' => $voiture['kilometres'],
                    'couleur' => $voiture['couleur'],
                    'id' => $voiture['id'],
                    'modele'=>VoitureModele::where('id', $voiture['voiture_modele_id'])->first()
                ];
            } else {
                $data[$voiture['id']] = [
                    'client' => true,
                    'client_id' => $clientVoiture->client_id,
                    'immat' => $voiture['immat'],
                    'assurance' => $voiture['assurance'],
                    'kilometres' => $voiture['kilometres'],
                    'couleur' => $voiture['couleur'],
                    'id' => $voiture['id'],
                    'modele'=>VoitureModele::where('id', $voiture['voiture_modele_id'])->first()
                ];
            }
        }
        return view('Voiture/voiture-index', [
            'voitures' => $data,
            'client' => $user,
            'data' => $data
        ]);
    }


    public function search(Request $request, Slug $slug)
    {
        $user=Auth::user();
        $search = $request->get('search');
        $search = $slug->slugify($search);
        $voitures = Voiture::whereIn('slug', [$search])->first();
        if ($voitures===null){
            $voitures=Voiture::where("immat", "LIKE", "%" . $search . "%")->get();
           // var_dump($voitures);die();
            return view ('Voiture/voiture-index',[
                'client'=>$user,
                'voitures'=>$voitures
            ]);
        }
        return redirect()->action('VoitureController@show', ['voitureId'=>$voitures->id]);
    }

    public function autocomplete(Request $request, $id)
    {
        $search = $request->get('immat');
        $data = Voiture::where("immat", "LIKE", "%" . $search . "%")
            ->where("user_id", $id)
            ->get();
        return response()->json($data);
    }

    public function show($voitureID)
    {
        $facture = $typeFacture = $typeDevis = null;
        $voiture = Voiture::find($voitureID);
        $devis = Devis::where('voiture_id', $voitureID)->first();
        if ($devis != null) {
            $facture = FactureDevis::where('devis_id', $devis->id)->first();
            if ($facture === null) {
                $typeFacture = 'bosse';
                $facture = VoitureFactureLibre::where('voiture_id', $voitureID)->first();

                if ($facture != null) {
                    $facture = FactureLibre::where('id', $facture->facture_libre_id)->first();
                    $facture = Facture::find($facture->facture_id);
                }
            } else {
                $facture = Facture::find($facture->facture_id);
                $typeFacture = 'devis';
            }
            $typeDevis = Devis::getDevisType($devis->id);
        }

        $client = ClientVoiture::where('voiture_id', $voitureID)->first();
        if ($client === null) {
            $client = false;
        }
        $modele = VoitureModele::find($voiture->voiture_modele_id);


        return view('Voiture/voiture-show', [
            'voiture' => $voiture,
            'devis' => $devis,
            'facture' => $facture,
            'modele' => $modele,
            'client' => $client,
            'typeFacture' => $typeFacture,
            'typeDevis' => $typeDevis
        ]);
    }
}
