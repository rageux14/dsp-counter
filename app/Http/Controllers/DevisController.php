<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Devis as Devis;
use App\DevisHoraire as DevisHoraire;
use App\Client as Client;
use App\ClientVoiture as ClientVoiture;
use App\PartieVoiture as PartieVoiture;
use App\PartieVoitureForfait as PartieVoitureForfait;
use App\Voiture as Voiture;
use App\VoitureModele as VoitureModele;
use App\Bosse as Bosse;
use App\DevisBosse as DevisBosse;
use App\DevisBosseBosse as DevisBosseBosse;

use Illuminate\Support\Facades\Validator;

use Auth;
use Carbon\Carbon;
use PDF;
use Mail;
use Image;

class DevisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $user = Auth::user();
        $devis = Devis::getAllDevisByUser($user->id);
        return view('Devis/devis', ['devis' =>$devis]);
    }

    public function indexCreateBosseFirst() {
        $user = Auth::user();
        $clients = Client::where('user_id','=', $user->id)->orderBy('societe')->get();
        $data = array();
        foreach($clients as $client) {
            $voitureClients = ClientVoiture::where('client_id',$client->id)->get();
            foreach ($voitureClients as $voitureClient) {
                $devisExist = Devis::where('voiture_id',$voitureClient->voiture_id)->first();
                  if (!isset($devisExist)) {
                    $voiture = Voiture::find($voitureClient->voiture_id);
                    $data[$voiture->id]['immat'] = $voiture->immat;
                    $data[$voiture->id]['societe'] = $client->societe;
                    $data[$voiture->id]['id'] = $voiture->id;
                }
            }
        }
        //generate unique numero for devis
        $numeroDevis = date("YmdHi");
        return view('Devis/devis-create-bosse-first', ['voitures' => $data, 'numDevis' => $numeroDevis]);
    }

    public function indexCreateFirstVoiture($clientId, $voitureId) {
        $user = Auth::user();
        $clients = Client::where('user_id','=', $user->id)->orderBy('societe')->get();
        $data = array();
        foreach($clients as $client) {
            $voitureClients = ClientVoiture::where('client_id',$client->id)->get();
            foreach ($voitureClients as $voitureClient) {
                $devisExist = Devis::where('voiture_id',$voitureClient->voiture_id)->first();
                if (!isset($devisExist)) {
                    $voiture = Voiture::find($voitureClient->voiture_id);
                    $data[$voiture->id]['immat'] = $voiture->immat;
                    $data[$voiture->id]['societe'] = $client->societe;
                    $data[$voiture->id]['id'] = $voiture->id;
                }
            }
        }
        //generate unique numero for devis
        $numeroDevis = date("YmdHi");
        if (isset($voitureId)) {
            $voiture = Voiture::find($voitureId);
            if (isset($voiture)) {
                $clientVoiture = ClientVoiture::where('voiture_id', $voiture->id)->first();
                $client = Client::find($clientVoiture->client_id);
                return view('Devis/devis-create-bosse-first', ['voitures' => $data, 'voitureSelect' => $voiture, 'clientId' => $client->id, 'numDevis' => $numeroDevis]);
            }
        }
        return view('Devis/devis-create-bosse-first', ['voitures' => $data, 'voitureSelect' => null, 'client' => null, 'numDevis' => $numeroDevis]);
    }

    public function createBosseFirst(Request $data) {
        //data validation
        $validator = Validator::make($data->all(), [
            'numero' => 'required|string',
            'sinistre' => 'nullable|string',
            'expert' => 'nullable|string',
            'date_reparation' => 'date|nullable',
            'date_expertise' => 'required|date',
        ]);
        if ($validator->fails()) {
            //return errors & the old user input's
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else {
            //test select value
            if($data->typedevis == 'bosse') {
                $voitureId = $data->get('select_voiture');
                if(!isset($voitureId) || $voitureId == 0) {
                    return redirect()->back()->with('error', "Veuillez selectionner une voiture ! ")->withInput();
                }
            }
            if($data->typedevis == 'bosse') {
                $degarnissage = $data->get('select_degarnissage');
                if(!isset($degarnissage)) {
                    return redirect()->back()->with('error', "Veuillez selectionner un degarnissage ! ")->withInput();
                }
            }
            if($data->typedevis == 'bosse') {
                $fraisDossier = $data->get('select_frais_de_dossier');
                if(!isset($fraisDossier)) {
                    return redirect()->back()->with('error', "Veuillez selectionner des frais de dossier ! ")->withInput();
                }
            }
            if($data->typedevis == 'bosse') {
                $tauxHoraire = $data->get('select_taux_horaire');
                if(!isset($tauxHoraire)) {
                    return redirect()->back()->with('error', "Veuillez selectionner un taux horaire ! ")->withInput();
                }
            }
            // test if numero devis is unique
            $devisExist = Devis::where('numero',$data->numero)->first();
            if(isset($devisExist)) {
                return redirect()->back()->with('error', "Ce numéro de devis est déja utilisé !")->withInput();
            }

            //create an instance in the BDD
            $user = Auth::user();
            $devis = Devis::create([
                'voiture_id' => $data->get('select_voiture'),
                'numero' => $data->numero,
                'nom_expert' => $data->expert,
                'numero_sinistre' => $data->sinistre,
                'date_reparation' => $data->date_reparation,
                'date_expertise' => $data->date_expertise,
                'frais_libelle' => $data->frais_libelle,
                'frais_prix' => $data->frais_prix,
                'taux_horaire' => $data->get('select_taux_horaire'),
                'created_at' => date("Y-m-d"),
                'user_id' => $user->id
            ]);
            if(isset($devis)){
                if($data->typedevis == 'bosse') {
                    $devisBosse = DevisBosse::create([
                        'devis_id' => $devis->id,
                        'degarnissage' => $data->get('select_degarnissage'),
                        'frais_de_dossier' => $data->get('select_frais_de_dossier'),
                        'created_at' => date("Y-m-d"),
                    ]);
                }
            }
            if (!empty($data->client_id)) {
                $clientId = $data->client_id;
                return redirect()->route('devis-create-bosse-second',['devisId' => $devis->id, 'devisBosseId' => $devisBosse->id, 'clientId' => $clientId]);
            }
            return redirect()->route('devis-create-bosse-second',['devisId' => $devis->id, 'devisBosseId' => $devisBosse->id]);
        }
    }

    public function indexCreateBosseSecond() {
        $partiesVoitures = PartieVoiture::orderBy('order')->get();
        $tabPartieVoitures = array();
        $devisBosseId = $_GET['devisBosseId'];
        foreach($partiesVoitures as $partiesVoiture) {
            $tabPartieVoitures[$partiesVoiture->id]['id'] = $partiesVoiture->id;
            $name = str_replace('_', ' ', $partiesVoiture->name);
            $name = ucwords($name);
            $tabPartieVoitures[$partiesVoiture->id]['name'] = $name;
            $bosses = Bosse::where('partie_voiture_id', $partiesVoiture->id)->get();
            foreach ($bosses as $bosse) {
                $devisBosseBosse = DevisBosseBosse::where('bosse_id',$bosse->id)->where('devis_bosse_id',$devisBosseId)->first();
                if (isset($devisBosseBosse)) {
                    $tabPartieVoitures[$partiesVoiture->id]['exist'] = true;
                }
            }
        }
        $devisId = $_GET['devisId'];
        if(isset($_GET['clientId'])) {
            $clientId = $_GET['clientId'];
            return view('Devis/devis-create-bosse-second', ['partiesVoitures' => $tabPartieVoitures, 'devisBosseId' => $devisBosseId, 'devisId' => $devisId, 'clientId' => $clientId]);
        }
        return view('Devis/devis-create-bosse-second', ['partiesVoitures' => $tabPartieVoitures, 'devisBosseId' => $devisBosseId, 'devisId' => $devisId]);
    }

      public function createBosseSecond(Request $data) {
        $devisId = $data->devisId;
        $devisBosseId = $data->devisBosseId;
        if (!empty($data->client_id)) {
            $clientId = $data->client_id;
            return redirect()->route('devis-create-bosse-third',['devisId' => $devisId, 'devisBosseId' => $devisBosseId, 'clientId' => $clientId]);
        }
        return redirect()->route('devis-create-bosse-third',['devisId' => $devisId, 'devisBosseId' => $devisBosseId]);
      }

    public function indexCreateBosseThird() {
        $partiesVoitures = PartieVoiture::orderBy('order')->get();
        $devisBosseId = $_GET['devisBosseId'];
        $devisId = $_GET['devisId'];
        $devis = Devis::find($devisId);
        $bosses = array();
        $totalDsp = 0;
        $devisBosseBosses = DevisBosseBosse::where('devis_bosse_id',$devisBosseId)->get();
        foreach($devisBosseBosses as $devisBosseBosse) {
            $bosse = Bosse::find($devisBosseBosse->bosse_id);
            $partieVoiture = PartieVoiture::find($bosse->partie_voiture_id);
            $name = str_replace('_', ' ', $partieVoiture->name);
            $name = ucwords($name);
            $bosses[$bosse->id]['name'] = $name;
            $bosses[$bosse->id]['bosse1'] = $bosse->bosse1;
            $bosses[$bosse->id]['bosse2'] = $bosse->bosse2;
            $bosses[$bosse->id]['bosse3'] = $bosse->bosse3;
            $total = (float)$bosse->ut * (float)$devis->taux_horaire;
            $bosses[$bosse->id]['total'] = $total;
            if ($bosse->dap) {
                $bosses[$bosse->id]['dap'] = 'Oui';
                $red = $total * 0.25;
                $total = $total - $red;
                $bosses[$bosse->id]['total'] = $total;
            } else {
                $bosses[$bosse->id]['dap'] = 'Non';
            }
            if ($bosse->alu) {
                $bosses[$bosse->id]['alu'] = 'Oui';
                $total = $total * 1.25;
                $bosses[$bosse->id]['total'] = round($total,2);
            } else {
                $bosses[$bosse->id]['alu'] = 'Non';
            }
            if ($bosse->reserves) {
                $bosses[$bosse->id]['reserves'] = 'Oui';
            } else {
                $bosses[$bosse->id]['reserves'] = 'Non';
            }
            if ($bosse->hs) {
                $bosses[$bosse->id]['hs'] = 'HS';
                $bosses[$bosse->id]['bosse1'] = 'HS';
                $bosses[$bosse->id]['bosse2'] = 'HS';
                $bosses[$bosse->id]['bosse3'] = 'HS';
                $bosses[$bosse->id]['alu'] = 'HS';
                $bosses[$bosse->id]['dap'] = 'HS';
                $bosses[$bosse->id]['reserves'] = 'HS';
                $bosses[$bosse->id]['total'] = 'HS';
            } else {
                $bosses[$bosse->id]['hs'] = 'Non';
                $totalDsp = round($totalDsp + $total,2);
            }
        }
        $infoDevis['total_dsp'] = round($totalDsp,2);
        $devisBosse = DevisBosse::where('devis_id',$devis->id)->first();
        $totalHt = $totalDsp + (float)$devisBosse->degarnissage + (float)$devisBosse->frais_de_dossier + (float)$devis->frais_prix;
        $infoDevis['total_ht'] = round($totalHt,2);
        $totalTva = round($totalHt * 0.20,2);
        $infoDevis['total_tva'] = round($totalTva,2);
        $infoDevis['total_ttc'] = round($totalTva + $totalHt,2);
        $infoDevis['degarnissage'] = $devisBosse->degarnissage;
        $infoDevis['frais_de_dossier'] = $devisBosse->frais_de_dossier;
        $infoDevis['frais_prix'] = $devis->frais_prix;
        $infoDevis['frais_libelle'] = $devis->frais_libelle;
        $infoDevis['numero'] = $devis->numero;
        $infoDevis['devis_id'] = $devis->id;
        $infoDevis['devis_bosse_id'] = $devisBosse->id;
        if(isset($_GET['clientId'])) {
            $clientId = $_GET['clientId'];
            return view('Devis/devis-create-bosse-third', ['bosses' => $bosses, 'devis' => $infoDevis, 'clientId' => $clientId]);
        }
        return view('Devis/devis-create-bosse-third', ['bosses' => $bosses, 'devis' => $infoDevis]);
    }

    public function createBosseThird(Request $data){
        //data validation
        $devisId = $data->devis_id;
        $devis = Devis::findOrFail($devisId);
        $devis->total_ht = $data->total_ht;
        $devis->total_tva = $data->total_tva;
        $devis->total_ttc = $data->total_ttc;
        $devis->commentaires = $data->commentaires;
        if ($devis->save()) {
            $devisBosse = DevisBosse::findOrFail($data->devis_bosse_id);
            $devisBosse->dsp = $data->total_dsp_bosse_third;
            if ($devisBosse->save()) {
                if (!empty($data->client_id)) {
                    $clientId = $data->client_id;
                    return redirect()->route('voiture', $clientId)->with('status', 'Devis validé !');
                }
                return redirect()->route('devis')->with('status', 'Devis validé !');
            }
        }
        return redirect()->back()->with('error', "Erreur d'enregistrement en base de données !")->withInput();
    }

    public function indexCreateHoraire() {
        $user = Auth::user();
        $clients = Client::where('user_id','=', $user->id)->orderBy('societe')->get();
        $data = array();
        foreach($clients as $client) {
            $voitureClients = ClientVoiture::where('client_id',$client->id)->get();
            foreach ($voitureClients as $voitureClient) {
                $devisExist = Devis::where('voiture_id', $voitureClient->voiture_id)->first();
                if(empty($devisExist)) {
                    $voiture = Voiture::find($voitureClient->voiture_id);
                    $data[$voiture->id]['immat'] = $voiture->immat;
                    $data[$voiture->id]['societe'] = $client->societe;
                    $data[$voiture->id]['id'] = $voiture->id;
                }
            }
        }
        $partieVoitures = PartieVoiture::orderBy('order')->get();
        $tabPartieVoitures = array();
        foreach($partieVoitures as $partieVoiture) {
            $tabPartieVoitures[$partieVoiture->id]['id'] = $partieVoiture->id;
            $name = str_replace('_', ' ', $partieVoiture->name);
            $name = ucwords($name);
            $tabPartieVoitures[$partieVoiture->id]['name'] = $name;
        }
        $numeroDevis = date("YmdHi");
        return view('Devis/devis-create-horaire', ['voitures' => $data, 'numDevis' => $numeroDevis, 'partieVoitures' => $tabPartieVoitures]);
    }

    public function indexCreateHoraireVoiture($clientId, $voitureId) {
        $user = Auth::user();
        $clients = Client::where('user_id','=', $user->id)->orderBy('societe')->get();
        $data = array();
        foreach($clients as $client) {
            $voitureClients = ClientVoiture::where('client_id',$client->id)->get();
            foreach ($voitureClients as $voitureClient) {
                $voiture = Voiture::find($voitureClient->voiture_id);
                $data[$voiture->id]['immat'] = $voiture->immat;
                $data[$voiture->id]['societe'] = $client->societe;
                $data[$voiture->id]['id'] = $voiture->id;
            }
        }
        $voiture = Voiture::find($voitureId);
        $clientVoiture = ClientVoiture::where('voiture_id', $voiture->id)->first();
        $client = Client::find($clientVoiture->client_id);
        $numeroDevis = date("YmdHi");
        $partieVoitures = PartieVoiture::all();
        $tabPartieVoitures = array();
        foreach($partieVoitures as $partieVoiture) {
            $tabPartieVoitures[$partieVoiture->id]['id'] = $partieVoiture->id;
            $name = str_replace('_', ' ', $partieVoiture->name);
            $name = ucwords($name);
            $tabPartieVoitures[$partieVoiture->id]['name'] = $name;
        }
        return view('Devis/devis-create-horaire', ['voitures' => $data, 'voitureSelect' => $voiture, 'clientId' => $client->id, 'numDevis' => $numeroDevis, 'partieVoitures' => $tabPartieVoitures]);
    }

    public function createHoraire(Request $data)
    {
        //data validation
        $validator = Validator::make($data->all(), [
            'numero' => 'required|string',
            'date_expertise' => 'required|date',
            'total_ht_horaire' => 'required|numeric',
            'total_tva' => 'required|numeric',
            'total_ttc' => 'required|numeric',
            'date_reparation' => 'date|nullable',
            'frais_prix' => 'numeric|nullable',
        ]);
        if ($validator->fails()) {
            //return errors & the old user input's
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else {
            //create an instance in the BDD
            if($data->typedevis == 'horaire') {
                $voitureId = $data->get('select_voiture');
                if(!isset($voitureId) || $voitureId == 0) {
                    return redirect()->back()->with('error', "Veuillez selectionner une voiture ! ")->withInput();
                }
            }
            $devisExist = Devis::where('numero',$data->numero)->first();
            if(isset($devisExist)) {
                return redirect()->back()->with('error', "Ce numéro de devis est déja utilisé !")->withInput();
            }
            $degarnissage = $data->get('select_degarnissage');
            if(!isset($degarnissage)) {
                return redirect()->back()->with('error', "Veuillez selectionner un degarnissage ! ")->withInput();
            }
            $fraisDossier = $data->get('select_frais_de_dossier');
            if(!isset($fraisDossier)) {
                return redirect()->back()->with('error', "Veuillez selectionner des frais de dossier ! ")->withInput();
            }
            // test if numero devis is unique
            $devisExist = Devis::where('numero',$data->numero)->first();
            if(isset($devisExist)) {
                return redirect()->back()->with('error', "Ce numéro de devis est déja utilisé !")->withInput();
            }
            $user = Auth::user();

            $devis = Devis::create([
                'voiture_id' => $data->get('select_voiture'),
                'numero' => $data->numero,
                'total_ht' => $data->total_ht_horaire,
                'frais_libelle' => $data->frais_libelle,
                'frais_prix' => $data->frais_prix,
                'total_tva' => $data->total_tva,
                'total_ttc' => $data->total_ttc,
                'taux_horaire' => $data->horaire,
                'date_reparation' => $data->date_reparation,
                'date_expertise' => $data->date_expertise,
                'nom_expert' => $data->expert,
                'numero_sinistre' => $data->sinistre,
                'commentaires' => $data->commentaires,
                'created_at' => date("Y-m-d"),
                'user_id' => $user->id
            ]);
            if(isset($devis)){
                if($data->typedevis == 'horaire') {
                    $devisHoraire = DevisHoraire::create([
                        'devis_id' => $devis->id,
                        'nb_heures' => '0',
                        'total_dsp' => $data->total_dsp,
                        'degarnissage' => $data->get('select_degarnissage'),
                        'frais_de_dossier' => $data->get('select_frais_de_dossier'),
                        'created_at' => date("Y-m-d"),
                    ]);
                }
            }
            $partiesVoitures = PartieVoiture::get();
            $countPartiesVoitures = count($partiesVoitures);
            for($i=1; $i <= $countPartiesVoitures; $i++) {
                $name = 'partie_voiture' . $i;
                $dap = 'check_dap' . $i;
                $alu = 'check_alu' . $i;
                $reserves = 'check_reserves' . $i;
                $tradi = 'check_tradi' . $i;
                $valuedap = $data->$dap;
                if(empty($valuedap)){
                    $valuedap = 0;
                }
                $valuealu = $data->$alu;
                if(empty($valuealu)){
                    $valuealu = 0;
                }
                $valuereserves = $data->$reserves;
                if(empty($valuereserves)){
                    $valuereserves = 0;
                }
                $valuetradi = $data->$tradi;
                if(empty($valuetradi)){
                    $valuetradi = 0;
                }
                if(!empty($data->$name)) {
                    PartieVoitureForfait::create([
                        'partie_voiture_id' => $i,
                        'devis_horaire_id' => $devisHoraire->id,
                        'prix' => $data->$name,
                        'alu' => $valuealu,
                        'dap' => $valuedap,
                        'reserves' => $valuereserves,
                        'tradi' => $valuetradi,
                        'created_at' => date("Y-m-d"),
                    ]);
                }
            }
            if (!empty($data->client_id)) {
                $clientId = $data->client_id;
                return redirect()->route('voiture', $clientId)->with('status', 'Devis crée!');
            }
            return redirect()->route('devis')->with('status', 'Devis crée!');
        }
    }

    public function getDevisBosse($id)
    {
        //test if the last parameter of the url is an integer
        if(is_numeric($id)) {
            $devis = Devis::find($id);
             if (!isset($devis)) {
                return redirect()->route('devis')->with('error', "Ce devis n'existe pas !");
            }
            $user = Auth::user();
            if ($devis->user_id != $user->id) {
                return redirect()->route('devis')->with('error', "Vous n'êtes pas autorisé à modifier ce devis!");
            }
            $data['id'] = $devis->id;
            $data['voiture_id'] = $devis->voiture_id;
            $data['user_id'] = $devis->user_id;
            $data['numero'] = $devis->numero;
            $data['taux_horaire'] = $devis->taux_horaire;
            $data['date_reparation'] = $devis->date_reparation;
            $data['date_expertise'] = $devis->date_expertise;
            $data['nom_expert'] = $devis->nom_expert;
            $data['frais_prix'] = $devis->frais_prix;
            $data['frais_libelle'] = $devis->frais_libelle;
            $data['numero_sinistre'] = $devis->numero_sinistre;
            $data['type'] = 'bosse';
            $devisBosse = DevisBosse::where('devis_id',$devis->id)->first();
            $data['degarnissage'] = $devisBosse->degarnissage;
            $data['devis_bosse_id'] = $devisBosse->id;
            $data['frais_de_dossier'] = $devisBosse->frais_de_dossier;
            //for redirect
           // $data['client_id'] = null;
            $clients = Client::where('user_id','=', $user->id)->orderBy('societe')->get();
            $voitures = array();
            foreach($clients as $client) {
                $voitureClients = ClientVoiture::where('client_id',$client->id)->get();
                foreach ($voitureClients as $voitureClient) {
                    $devisExist = Devis::where('voiture_id', $voitureClient->voiture_id)->first();
                    if(empty($devisExist) || $devisExist->id == $devis->id) {
                        $voiture = Voiture::find($voitureClient->voiture_id);
                        $voitures[$voiture->id]['immat'] = $voiture->immat;
                        $voitures[$voiture->id]['societe'] = $client->societe;
                        $voitures[$voiture->id]['id'] = $voiture->id;
                    }
                }
            }
            return view('Devis/devis-modify-bosse', ['devis' => $data, 'voitures' => $voitures]);
        }
        return redirect()->route('devis');
    }

    public function getDevisBosseVoiture($clientId, $id)
    {
        //test if the last parameter of the url is an integer
        if(is_numeric($id)) {
            $devis = Devis::find($id);
             if (!isset($devis)) {
                return redirect()->route('voiture',$clientId)->with('error', "Ce devis n'existe pas !");
            }
            $user = Auth::user();
            if ($devis->user_id != $user->id) {
                return redirect()->route('voiture',$clientId)->with('error', "Vous n'êtes pas autorisé à modifier ce devis!");
            }
            $data['id'] = $devis->id;
            $data['voiture_id'] = $devis->voiture_id;
            $data['user_id'] = $devis->user_id;
            $data['numero'] = $devis->numero;
            $data['taux_horaire'] = $devis->taux_horaire;
            $data['date_reparation'] = $devis->date_reparation;
            $data['date_expertise'] = $devis->date_expertise;
            $data['nom_expert'] = $devis->nom_expert;
            $data['frais_prix'] = $devis->frais_prix;
            $data['frais_libelle'] = $devis->frais_libelle;
            $data['numero_sinistre'] = $devis->numero_sinistre;
            $data['type'] = 'bosse';
            $devisBosse = DevisBosse::where('devis_id',$devis->id)->first();
            if (!empty($devisBosse)) {
                $data['degarnissage'] = $devisBosse->degarnissage;
                $data['devis_bosse_id'] = $devisBosse->id;
                $data['frais_de_dossier'] = $devisBosse->frais_de_dossier;
            }
            if($clientId != null){
                $data['client_id'] = $clientId;
            } else {
                $data['client_id'] = null;
            }
            $clients = Client::where('user_id','=', $user->id)->orderBy('societe')->get();
            $voitures = array();
            foreach($clients as $client) {
                $voitureClients = ClientVoiture::where('client_id',$client->id)->get();
                foreach ($voitureClients as $voitureClient) {
                    $devisExist = Devis::where('voiture_id', $voitureClient->voiture_id)->first();
                    if(empty($devisExist) || $devisExist->id == $devis->id) {
                        $voiture = Voiture::find($voitureClient->voiture_id);
                        $voitures[$voiture->id]['immat'] = $voiture->immat;
                        $voitures[$voiture->id]['societe'] = $client->societe;
                        $voitures[$voiture->id]['id'] = $voiture->id;
                    }
                }
            }
            return view('Devis/devis-modify-bosse', ['devis' => $data, 'voitures' => $voitures]);
        }
        return redirect()->route('voiture',$clientId);
    }

    public function setDevisBosse(Request $data) {
        //data validation
        $validator = Validator::make($data->all(), [
            'numero' => 'required|string',
            'sinistre' => 'nullable|string',
            'expert' => 'nullable|string',
            'date_reparation' => 'date|nullable',
            'date_expertise' => 'required|date',
        ]);
        if ($validator->fails()) {
            //return errors & the old user input's
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else {
            //test select value
            if($data->typedevis == 'bosse') {
                $voitureId = $data->get('select_voiture');
                if(!isset($voitureId) || $voitureId == 0) {
                    return redirect()->back()->with('error', "Veuillez selectionner une voiture ! ")->withInput();
                }
            }
            if($data->typedevis == 'bosse') {
                $degarnissage = $data->get('select_degarnissage');
                if(!isset($degarnissage)) {
                    return redirect()->back()->with('error', "Veuillez selectionner un degarnissage ! ")->withInput();
                }
            }
            if($data->typedevis == 'bosse') {
                $fraisDossier = $data->get('select_frais_de_dossier');
                if(!isset($fraisDossier)) {
                    return redirect()->back()->with('error', "Veuillez selectionner des frais de dossier ! ")->withInput();
                }
            }
            if($data->typedevis == 'bosse') {
                $tauxHoraire = $data->get('select_taux_horaire');
                if(!isset($tauxHoraire)) {
                    return redirect()->back()->with('error', "Veuillez selectionner un taux horaire ! ")->withInput();
                }
            }
            $devis = Devis::find($data->devis_id);
            // test if numero devis is unique
            $devisExist = Devis::where('numero',$data->numero)->first();
            if(isset($devisExist)) {
                if ($devisExist->id != $devis->id) {
                     return redirect()->back()->with('error', "Ce numéro de devis est déja utilisé !")->withInput();
                }
            }
            //create an instance in the BDD
            $user = Auth::user();
            $devis->voiture_id = $data->get('select_voiture');
            $devis->numero = $data->numero;
            $devis->nom_expert = $data->expert;
            $devis->numero_sinistre = $data->sinistre;
            $devis->date_reparation = $data->date_reparation;
            $devis->date_expertise = $data->date_expertise;
            $devis->frais_prix = $data->frais_prix;
            $devis->frais_libelle = $data->frais_libelle;
            $devis->taux_horaire = $data->get('select_taux_horaire');
            if($devis->save()){
                if($data->typedevis == 'bosse') {
                    $devisBosse = DevisBosse::find($data->devis_bosse_id);
                    $devisBosse->devis_id = $devis->id;
                    $devisBosse->degarnissage = $data->get('select_degarnissage');
                    $devisBosse->frais_de_dossier = $data->get('select_frais_de_dossier');
                    if ($devisBosse->save()) {
                        //get old clients route if exist
                        if($data->client_id) {
                            $clientId = $data->client_id;
                            return redirect()->route('devis-create-bosse-second',['devisId' => $devis->id, 'devisBosseId' => $devisBosse->id, 'clientId' => $clientId]);
                        }
                        return redirect()->route('devis-create-bosse-second',['devisId' => $devis->id, 'devisBosseId' => $devisBosse->id]);
                    }
                    return redirect()->back()->with('error', "Erreur d'enregistrement en base de données !")->withInput();
                }
            }
            return redirect()->back()->with('error', "Erreur d'enregistrement en base de données !")->withInput();
        }
    }

     public function getDevisHoraireVoiture($clientId, $id)
    {
        //test if the last parameter of the url is an integer
        if(is_numeric($id)) {
            $devis = Devis::find($id);
             if (!isset($devis)) {
                return redirect()->route('voiture',$clientId)->with('error', "Ce devis n'existe pas !");
            }
            $user = Auth::user();
            if ($devis->user_id != $user->id) {
                return redirect()->route('voiture',$clientId)->with('error', "Vous n'êtes pas autorisé à modifier ce devis!");
            }
            $data['id'] = $devis->id;
            $data['voiture_id'] = $devis->voiture_id;
            $data['user_id'] = $devis->user_id;
            $data['numero'] = $devis->numero;
            $data['date_reparation'] = $devis->date_reparation;
            $data['date_expertise'] = $devis->date_expertise;
            $data['expert'] = $devis->expert_nom;
            $data['sinistre'] = $devis->numero_sinistre;
            $data['total_ht'] = $devis->total_ht;
            $data['total_tva'] = $devis->total_tva;
            $data['total_ttc'] = $devis->total_ttc;
            $data['commentaires'] = $devis->commentaires;
            $data['frais_prix'] = $devis->frais_prix;
            $data['frais_libelle'] = $devis->frais_libelle;
            $data['taux_horaire'] = $devis->taux_horaire;
            $data['type'] = 'horaire';
            $devisHoraire = DevisHoraire::where('devis_id',$devis->id)->first();
            $data['degarnissage'] = $devisHoraire->degarnissage;
            $data['frais_de_dossier'] = $devisHoraire->frais_de_dossier;
            $data['total_dsp'] = $devisHoraire->total_dsp;
            $data['devis_horaire_id'] = $devisHoraire->id;

            $clients = Client::where('user_id','=', $user->id)->orderBy('societe')->get();
            $voitures = array();
            foreach($clients as $client) {
                $voitureClients = ClientVoiture::where('client_id',$client->id)->get();
                foreach ($voitureClients as $voitureClient) {
                    $devisExist = Devis::where('voiture_id', $voitureClient->voiture_id)->first();
                    if(empty($devisExist) || $devisExist->id == $devis->id) {
                        $voiture = Voiture::find($voitureClient->voiture_id);
                        $voitures[$voiture->id]['immat'] = $voiture->immat;
                        $voitures[$voiture->id]['societe'] = $client->societe;
                        $voitures[$voiture->id]['id'] = $voiture->id;
                    }
                }
            }
            $partieVoitures = PartieVoiture::orderBy('order')->get();
            $tabPartieVoitures = array();
            foreach($partieVoitures as $partieVoiture) {
                $partieVoitureForfait = PartieVoitureForfait::where('devis_horaire_id', $devisHoraire->id)->where('partie_voiture_id', $partieVoiture->id)->first();
                if (!empty($partieVoitureForfait) && isset($partieVoitureForfait)) {
                    $tabPartieVoitures[$partieVoiture->id]['prix'] = $partieVoitureForfait->prix;
                    $tabPartieVoitures[$partieVoiture->id]['alu'] = $partieVoitureForfait->alu;
                    $tabPartieVoitures[$partieVoiture->id]['dap'] = $partieVoitureForfait->dap;
                    $tabPartieVoitures[$partieVoiture->id]['reserves'] = $partieVoitureForfait->reserves;
                    $tabPartieVoitures[$partieVoiture->id]['tradi'] = $partieVoitureForfait->tradi;
                }
                $name = str_replace('_', ' ', $partieVoiture->name);
                $name = ucwords($name);
                $tabPartieVoitures[$partieVoiture->id]['id'] = $partieVoiture->id;
                $tabPartieVoitures[$partieVoiture->id]['name'] = $name;
            }
            return view('Devis/devis-modify-horaire', ['devis' => $data, 'voitures' => $voitures, 'clientId' => $clientId, 'partieVoitures' => $tabPartieVoitures]);
        }
        return redirect()->route('devis');
    }

    public function getDevisHoraire($id)
    {
        //test if the last parameter of the url is an integer
        if(is_numeric($id)) {
            $devis = Devis::find($id);
             if (!isset($devis)) {
                return redirect()->route('devis')->with('error', "Ce devis n'existe pas !");
            }
            $user = Auth::user();
            if ($devis->user_id != $user->id) {
                return redirect()->route('devis')->with('error', "Vous n'êtes pas autorisé à modifier ce devis!");
            }
            $data['id'] = $devis->id;
            $data['voiture_id'] = $devis->voiture_id;
            $data['user_id'] = $devis->user_id;
            $data['numero'] = $devis->numero;
            $data['date_reparation'] = $devis->date_reparation;
            $data['date_expertise'] = $devis->date_expertise;
            $data['expert'] = $devis->nom_expert;
            $data['sinistre'] = $devis->numero_sinistre;
            $data['total_ht'] = $devis->total_ht;
            $data['total_tva'] = $devis->total_tva;
            $data['total_ttc'] = $devis->total_ttc;
            $data['commentaires'] = $devis->commentaires;
            $data['frais_prix'] = $devis->frais_prix;
            $data['frais_libelle'] = $devis->frais_libelle;
            $data['taux_horaire'] = $devis->taux_horaire;
            $data['type'] = 'horaire';
            $devisHoraire = DevisHoraire::where('devis_id',$devis->id)->first();
            $data['degarnissage'] = $devisHoraire->degarnissage;
            $data['frais_de_dossier'] = $devisHoraire->frais_de_dossier;
            $data['total_dsp'] = $devisHoraire->total_dsp;
            $data['devis_horaire_id'] = $devisHoraire->id;

            $clients = Client::where('user_id','=', $user->id)->orderBy('societe')->get();
            $voitures = array();
            foreach($clients as $client) {
                $voitureClients = ClientVoiture::where('client_id',$client->id)->get();
                foreach ($voitureClients as $voitureClient) {
                    $devisExist = Devis::where('voiture_id', $voitureClient->voiture_id)->first();
                    if(empty($devisExist) || $devisExist->id == $devis->id) {
                        $voiture = Voiture::find($voitureClient->voiture_id);
                        $voitures[$voiture->id]['immat'] = $voiture->immat;
                        $voitures[$voiture->id]['societe'] = $client->societe;
                        $voitures[$voiture->id]['id'] = $voiture->id;
                    }
                }
            }
            $partieVoitures = PartieVoiture::orderBy('order')->get();
            $tabPartieVoitures = array();
            foreach($partieVoitures as $partieVoiture) {
                $partieVoitureForfait = PartieVoitureForfait::where('devis_horaire_id', $devisHoraire->id)->where('partie_voiture_id', $partieVoiture->id)->first();
                if (!empty($partieVoitureForfait) && isset($partieVoitureForfait)) {
                    $tabPartieVoitures[$partieVoiture->id]['prix'] = $partieVoitureForfait->prix;
                    $tabPartieVoitures[$partieVoiture->id]['alu'] = $partieVoitureForfait->alu;
                    $tabPartieVoitures[$partieVoiture->id]['dap'] = $partieVoitureForfait->dap;
                    $tabPartieVoitures[$partieVoiture->id]['reserves'] = $partieVoitureForfait->reserves;
                    $tabPartieVoitures[$partieVoiture->id]['tradi'] = $partieVoitureForfait->tradi;
                }
                $name = str_replace('_', ' ', $partieVoiture->name);
                $name = ucwords($name);
                $tabPartieVoitures[$partieVoiture->id]['id'] = $partieVoiture->id;
                $tabPartieVoitures[$partieVoiture->id]['name'] = $name;
            }
            return view('Devis/devis-modify-horaire', ['devis' => $data, 'voitures' => $voitures, 'partieVoitures' => $tabPartieVoitures]);
        }
        return redirect()->route('devis');
    }

     public function setDevisHoraire(Request $data) {
       //data validation
        $validator = Validator::make($data->all(), [
            'numero' => 'required|string',
            'date_expertise' => 'required|date',
            'total_ht' => 'required|numeric',
            'total_tva' => 'required|numeric',
            'total_ttc' => 'required|numeric',
            'date_reparation' => 'date|nullable',
            'frais_prix' => 'numeric|nullable',
        ]);
        if ($validator->fails()) {
            //return errors & the old user input's
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else {
            $degarnissage = $data->get('select_degarnissage');
            if(!isset($degarnissage)) {
                return redirect()->back()->with('error', "Veuillez selectionner un degarnissage ! ")->withInput();
            }
            $fraisDossier = $data->get('select_frais_de_dossier');
            if(!isset($fraisDossier)) {
                return redirect()->back()->with('error', "Veuillez selectionner des frais de dossier ! ")->withInput();
            }
            $devis = Devis::find($data->devis_id);
            // test if numero devis is unique
            $devisExist = Devis::where('numero',$data->numero)->first();
            if(isset($devisExist)) {
                if ($devisExist->id != $devis->id) {
                     return redirect()->back()->with('error', "Ce numéro de devis est déja utilisé !")->withInput();
                }
            }
            $user = Auth::user();
            $devis->numero = $data->numero;
            $devis->date_reparation = $data->date_reparation;
            $devis->date_expertise = $data->date_expertise;
            $devis->numero_sinistre = $data->sinistre;
            $devis->nom_expert = $data->expert;
            $devis->total_ht = $data->total_ht;
            $devis->total_tva = $data->total_tva;
            $devis->total_ttc = $data->total_ttc;
            $devis->commentaires = $data->commentaires;
            $devis->frais_libelle = $data->frais_libelle;
            $devis->frais_prix = $data->frais_prix;
            if($devis->save()){
                if($data->typedevis == 'horaire') {
                    $devisHoraire = DevisHoraire::find($data->devis_horaire_id);
                    $devisHoraire->total_dsp = $data->total_dsp;
                    $devisHoraire->degarnissage = $data->get('select_degarnissage');
                    $devisHoraire->frais_de_dossier = $data->get('select_frais_de_dossier');
                    if ($devisHoraire->save()) {
                        for($i=1; $i < 15; $i++) {
                            $name = 'partie_voiture' . $i;
                            $dap = 'check_dap' . $i;
                            $alu = 'check_alu' . $i;
                            $reserves = 'check_reserves' . $i;
                            $valuedap = $data->$dap;
                            if(empty($valuedap)){
                                $valuedap = 0;
                            }
                            $valuealu = $data->$alu;
                            if(empty($valuealu)){
                                $valuealu = 0;
                            }
                            $valuereserves = $data->$reserves;
                            if(empty($valuereserves)){
                                $valuereserves = 0;
                            }
                            $exist = PartieVoitureForfait::where('devis_horaire_id', $devisHoraire->id)->where('partie_voiture_id',$i)->first();
                            if (empty($exist)) {
                                 if(!empty($data->$name)) {
                                    PartieVoitureForfait::create([
                                        'partie_voiture_id' => $i,
                                        'devis_horaire_id' => $devisHoraire->id,
                                        'prix' => $data->$name,
                                        'alu' => $valuealu,
                                        'dap' => $valuedap,
                                        'reserves' => $valuereserves,
                                        'created_at' => date("Y-m-d"),
                                    ]);
                                }
                            } else {
                                if(!empty($data->$name)) {
                                    $exist->prix = $data->$name;
                                } else {
                                    $exist->prix = '0';
                                }
                               $exist->alu = $valuealu;
                               $exist->dap = $valuedap;
                               $exist->reserves = $valuereserves;
                               if(!$exist->save()){
                                    return redirect()->back()->with('error', "Erreur d'enregistrement en base de données !")->withInput();
                               }
                            }
                        }
                        if (!empty($data->client_id)) {
                            $clientId = $data->client_id;
                         return redirect()->route('voiture', $clientId)->with('status', 'Devis au forfait modifié !');
                        }
                        return redirect()->route('devis')->with('status', 'Devis au forfait modifié !');
                    }
                    return redirect()->back()->with('error', "Erreur d'enregistrement en base de données !")->withInput();
                }
            }
            return redirect()->back()->with('error', "Erreur d'enregistrement en base de données !")->withInput();
        }
    }

    public function delete($id, $type, $previous='')
    {
        //test if the last parameter of the url is an integer

       if(is_numeric($id)) {
            $devis = Devis::find($id);
             if (!isset($devis)) {
                 if($previous==='voiture'){
                     return redirect()->back()->with('error', "Ce devis n'existe pas !");
                 }
                return redirect()->route('devis')->with('error', "Ce devis n'existe pas !");
            }
            $user = Auth::user();
            if ($devis->user_id != $user->id) {
                return redirect()->route('devis')->with('error', "Vous n'êtes pas autorisé à supprimer ce devis!");
            }
            if ($type === 'bosse') {
                $devisBosse = DevisBosse::where('devis_id','=',$id)->first();
                $devisBosseBosses = DevisBosseBosse::where('devis_bosse_id',$devisBosse->id)->get();
                foreach($devisBosseBosses as $devisBosseBosse) {
                    $bosse = Bosse::find($devisBosseBosse->id);
                    if (!$devisBosseBosse->delete()){
                        return redirect()->route('devis')->with('error', "Problème lors de l'effacement dans la table Devis Bosse Bosse !");
                    }
                    if (!$bosse->delete()){
                        return redirect()->route('devis')->with('error', "Problème lors de l'effacement dans la table Bosse !");
                    }
                }
                if (!$devisBosse->delete()){
                    if($previous==='voiture'){
                        return redirect()->back()->with('error', "Problème lors de l'effacement");
                    }
                    return redirect()->route('devis')->with('error', "Problème lors de l'effacement");
                }
                if ($devis->delete()) {
                    if($previous==='voiture'){
                        return redirect()->back()->with('status', "Devis effacé !");
                    }
                    return redirect()->route('devis')->with('status', "Devis effacé !");
                } else {
                    if($previous==='voiture'){
                        return redirect()->back()->with('error', "Problème lors de l'effacement");
                    }
                    return redirect()->route('devis')->with('error', "Problème lors de l'effacement");
                }
            } else {
                $devisHoraire = devisHoraire::where('devis_id','=',$id)->first();
                $partieVoitureForfaits = PartieVoitureForfait::where('devis_horaire_id', $devisHoraire->id)->get();
                foreach($partieVoitureForfaits as $partieVoitureForfait) {
                   if(!$partieVoitureForfait->delete()) {
                    return redirect()->route('devis')->with('error', "Problème lors de l'effacement dans la table Partie Voiture Forfait!");
                   }
                }
                if (!$devisHoraire->delete()){
                    if($previous==='voiture'){
                        return redirect()->back()->with('error', "Problème lors de l'effacement");
                    }
                    return redirect()->route('devis')->with('error', "Problème lors de l'effacement");
                }

                if ($devis->delete()) {
                    if($previous==='voiture'){
                        return redirect()->back()->with('status', "Devis effacé !");
                    }
                    return redirect()->route('devis')->with('status', "Devis effacé !");
                } else {
                    if($previous==='voiture'){
                        return redirect()->back()->with('error', "Problème lors de l'effacement");
                    }
                    return redirect()->route('devis')->with('error', "Problème lors de l'effacement");
                }
            }

        }
        return redirect()->route('devis');
    }

    public function deleteVoiture($clientId, $id, $type)
    {
        //test if the last parameter of the url is an integer
       if(is_numeric($id)) {
            $devis = Devis::find($id);
             if (!isset($devis)) {
                return redirect()->route('voiture', $clientId)->with('error', "Ce devis n'existe pas !");
            }
            $user = Auth::user();
            if ($devis->user_id != $user->id) {
                return redirect()->route('voiture', $clientId)->with('error', "Vous n'êtes pas autorisé à supprimer ce devis!");
            }
            if ($type == 'bosse') {
                $devisBosse = DevisBosse::where('devis_id','=',$id)->first();
                $devisBosseBosses = DevisBosseBosse::where('devis_bosse_id',$devisBosse->id)->get();
                foreach($devisBosseBosses as $devisBosseBosse) {
                    $bosse = Bosse::find($devisBosseBosse->id);
                    if (!$devisBosseBosse->delete()){
                        return redirect()->route('voiture', $clientId)->with('error', "Problème lors de l'effacement dans la table Devis Bosse Bosse !");
                    }
                    if (!$bosse->delete()){
                        return redirect()->route('voiture', $clientId)->with('error', "Problème lors de l'effacement dans la table Bosse !");
                    }
                }
                if (!$devisBosse->delete()){
                    return redirect()->route('voiture', $clientId)->with('error', "Problème lors de l'effacement");
                }
                if ($devis->delete()) {
                    return redirect()->route('voiture', $clientId)->with('status', "Devis effacé !");
                } else {
                    return redirect()->route('voiture', $clientId)->with('error', "Problème lors de l'effacement");
                }
            } else {
                $devisHoraire = devisHoraire::where('devis_id','=',$id)->first();
                $partieVoitureForfaits = PartieVoitureForfait::where('devis_horaire_id', $devisHoraire->id)->get();
                foreach($partieVoitureForfaits as $partieVoitureForfait) {
                   if(!$partieVoitureForfait->delete()) {
                    return redirect()->route('voiture', $clientId)->with('error', "Problème lors de l'effacement dans la table Partie Voiture Forfait!");
                   }
                }
                if (!$devisHoraire->delete()){
                    return redirect()->route('voiture', $clientId)->with('error', "Problème lors de l'effacement");
                }
                if ($devis->delete()) {
                    return redirect()->route('voiture', $clientId)->with('status', "Devis effacé !");
                } else {
                    return redirect()->route('voiture', $clientId)->with('error', "Problème lors de l'effacement");
                }
            }

        }
        return redirect()->route('devis');
    }
     //PDF Download
    public function downloadPdf($id, $action) {
        //test if the last parameter of the url is an integer
        if(is_numeric($id)) {
            $devis = Devis::find($id);
            if (!isset($devis)) {
                return redirect()->route('devis')->with('error', "Ce devis n'existe pas !");
            }
            $user = Auth::user();
            if ($devis->user_id != $user->id) {
                return redirect()->route('devis')->with('error', "Vous n'êtes pas autorisé à télécharger ce devis!");
            }
            $data = array();
            $data['numero'] = $devis->numero;
            $data['total_ht'] = $devis->total_ht;
            $data['total_tva'] = $devis->total_tva;
            $data['total_ttc'] = $devis->total_ttc;
            $data['taux_horaire'] = $devis->taux_horaire * 10;
            $data['commentaires'] = $devis->commentaires;
            $data['id'] = $devis->id;
            $data['date_reparation'] = Carbon::parse($devis->date_reparation)->format('d/m/Y');
            $data['date_expertise'] = Carbon::parse($devis->date_expertise)->format('d/m/Y');
            $data['nom_expert'] = $devis->nom_expert;
            $data['numero_sinistre'] = $devis->numero_sinistre;
            $data['frais_prix'] = $devis->frais_prix;
            $data['frais_libelle'] = $devis->frais_libelle;
            $data['user_societe'] = $user->societe;
            $data['user_adresse'] = $user->adresse;
            $data['user_nom'] = $user->nom;
            $data['user_prenom'] = $user->prenom;
            $data['user_telephone'] = $user->telephone;
            $data['user_cp'] = $user->cp;
            $data['user_ville'] = $user->ville;
            $data['user_siren'] = $user->siren;
            $data['user_email'] = $user->email;
            $data['user_logo'] = $user->logo;
            $data['user_capital_social'] = $user->capital_social;
            $voiture = Voiture::find($devis->voiture_id);
            $data['type'] = 'devis';
            $voitureModele = VoitureModele::find($voiture->voiture_modele_id);
            $data['marque'] = $voitureModele->rappel_marque;
            $data['modele'] = $voitureModele->rappel_modele;
            $data['immat'] = $voiture->immat;
            $data['chassis'] = $voiture->chassis;
            $data['nom_client'] = $voiture->nom_client;
            $data['devis_numero'] = $devis->numero;
            $clientVoiture = ClientVoiture::where('voiture_id','=',$voiture->id)->first();
            $client = Client::find($clientVoiture->client_id);
            $data['client_societe'] = $client->societe;
            $data['client_adresse'] = $client->adresse;
            $data['client_CP'] = $client->CP;
            $data['client_ville'] = $client->ville;
            $data['client_siren'] = $client->siren;
            $data['client_email'] = $client->email;
            $devisBosse = DevisBosse::where('devis_id','=',$devis->id)->first();
            if (isset($devisBosse)) {
                $data['type'] = 'bosse';
                $data['degarnissage'] = $devisBosse->degarnissage;
                $data['frais_de_dossier'] = $devisBosse->frais_de_dossier;
                $data['dsp'] = $devisBosse->dsp;
                $devisBosseBosses = DevisBosseBosse::where('devis_bosse_id',$devisBosse->id)->get();
                $img = Image::make(public_path('img/voiture_calculation.png'));
                foreach($devisBosseBosses as $devisBosseBosse) {
                    $bosse = Bosse::find($devisBosseBosse->bosse_id);
                    $partieVoiture = PartieVoiture::find($bosse->partie_voiture_id);
                    $color = '#8dc65b';
                    if ($bosse->tradi) {
                        $color = '#db2525';
                    }
                    if ($bosse->dap) {
                        $color = '#f97c22';
                    }
                    if ($bosse->reserves) {
                        $color = '#c9b516';
                    }
                    $totalBosse = $bosse->bosse1 + $bosse->bosse2 + $bosse->bosse3;
                    if ($bosse->alu) {
                        $totalBosse = $totalBosse . '(ALU)';
                    }
                    if ($bosse->hs) {
                        $color = '#5495ff';
                        $data[$partieVoiture->name] = 'HS';
                        $totalBosse = 'HS';
                    } else {
                        $data[$partieVoiture->name] = (float) $bosse->ut * 10;
                    }
                    switch($partieVoiture->id) {
                        case 1 :
                            $img->fill($color, 185, 100);
                            $img->text($totalBosse, 185, 100, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 2 :
                            $img->fill($color, 185, 490);
                            $img->text($totalBosse, 185, 490, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 3 :
                            $img->fill($color, 185, 700);
                            $img->text($totalBosse, 185, 700, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 4 :
                            $img->fill($color, 435, 1030);
                            $img->text($totalBosse, 435, 1030, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 5 :
                            $img->fill($color, 444, 100);
                            $img->text($totalBosse, 444, 100, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 6 :
                            $img->fill($color, 1269, 100);
                            $img->text($totalBosse, 1269, 100, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 7 :
                            $img->fill($color, 1269, 490);
                            $img->text($totalBosse, 1269, 490, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 8 :
                            $img->fill($color, 1269, 700);
                            $img->text($totalBosse, 1269, 700, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 9 :
                            $img->fill($color, 1020, 1030);
                            $img->text($totalBosse, 1020, 1030, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 10 :
                            $img->fill($color, 1000, 100);
                            $img->text($totalBosse, 1000, 100, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 11 :
                            $img->fill($color, 728, 100);
                            $img->text($totalBosse, 728, 100, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 12 :
                            $img->fill($color, 728, 460);
                            $img->text($totalBosse, 728, 460, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 13 :
                            $img->fill($color, 728, 780);
                            $img->text($totalBosse, 728, 780, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 14 :
                            $img->fill($color, 728, 1030);
                            $img->text($totalBosse, 728, 1030, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                    }

                }
                //create unique image by devis
                $img->save(public_path('img/voiture_calculation' . $devis->numero . '.png'));
                $pdf = PDF::loadView('pdf/devis-bosse-pdf',compact('data'));
            } else {
                $devisHoraire = DevisHoraire::where('devis_id',$devis->id)->first();
                $data['type'] = 'horaire';
                $data['degarnissage'] = $devisHoraire->degarnissage;
                $data['frais_de_dossier'] = $devisHoraire->frais_de_dossier;
                $data['dsp'] = $devisHoraire->total_dsp;
                $data['nb_heures'] = $devisHoraire->total_dsp / $devis->taux_horaire / 10;
                $partieVoitureForfaits = PartieVoitureForfait::where('devis_horaire_id', $devisHoraire->id)->get();
                $img = Image::make(public_path('img/voiture_calculation.png'));
                foreach( $partieVoitureForfaits as $partieVoitureForfait) {
                    $partieVoiture = PartieVoiture::find($partieVoitureForfait->partie_voiture_id);
                    $data[$partieVoiture->name]['prix'] =  $partieVoitureForfait->prix;
                    $data[$partieVoiture->name]['alu'] =  $partieVoitureForfait->alu;
                    $data[$partieVoiture->name]['dap'] =  $partieVoitureForfait->dap;
                    $data[$partieVoiture->name]['reserves'] =  $partieVoitureForfait->reserves;
                    $data[$partieVoiture->name]['tradi'] =  $partieVoitureForfait->tradi;
                    $color = '#8dc65b';
                    if ($partieVoitureForfait->tradi) {
                        $color = '#db2525';
                    }
                    if ($partieVoitureForfait->dap) {
                        $color = '#f97c22';
                    }
                    if ($partieVoitureForfait->reserves) {
                        $color = '#c9b516';
                    }
                    if ($partieVoitureForfait->alu) {
                        $totalBosse = $totalBosse = $partieVoitureForfait->prix . ' h(ALU)';
                    } else {
                        $totalBosse = $partieVoitureForfait->prix . ' h';
                    }
                    if ($partieVoitureForfait->hs) {
                        $color = '#5495ff';
                        $data[$partieVoitureForfait->name] = 'HS';
                        $totalBosse = 'HS';
                    }
                    switch($partieVoiture->id) {
                        case 1 :
                            $img->fill($color, 185, 100);
                            $img->text($totalBosse, 185, 100, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 2 :
                            $img->fill($color, 185, 490);
                            $img->text($totalBosse, 185, 490, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 3 :
                            $img->fill($color, 185, 700);
                            $img->text($totalBosse, 185, 700, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 4 :
                            $img->fill($color, 435, 1030);
                            $img->text($totalBosse, 435, 1030, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 5 :
                            $img->fill($color, 444, 100);
                            $img->text($totalBosse, 444, 100, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 6 :
                            $img->fill($color, 1269, 100);
                            $img->text($totalBosse, 1269, 100, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 7 :
                            $img->fill($color, 1269, 490);
                            $img->text($totalBosse, 1269, 490, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 8 :
                            $img->fill($color, 1269, 700);
                            $img->text($totalBosse, 1269, 700, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 9 :
                            $img->fill($color, 1020, 1030);
                            $img->text($totalBosse, 1020, 1030, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 10 :
                            $img->fill($color, 1000, 100);
                            $img->text($totalBosse, 1000, 100, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 11 :
                            $img->fill($color, 728, 100);
                            $img->text($totalBosse, 728, 100, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 12 :
                            $img->fill($color, 728, 460);
                            $img->text($totalBosse, 728, 460, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 13 :
                            $img->fill($color, 728, 780);
                            $img->text($totalBosse, 728, 780, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                        case 14 :
                            $img->fill($color, 728, 1030);
                            $img->text($totalBosse, 728, 1030, function($font) {
                              $font->file(public_path('fonts/SourceSansPro-Regular.ttf'));
                              $font->size(42);
                              $font->color('#161616');
                              $font->align('center');
                              $font->valign('bottom');
                              });
                            break;
                    }
                }
                //create unique image by devis
                $img->save(public_path('img/voiture_calculation' . $devis->numero . '.png'));
                $pdf = PDF::loadView('pdf/devis-forfait-pdf',compact('data'));
            }

            $name = 'devis_' . $devis->numero . '_' . $devis->id . '.pdf';
            $path = public_path(). '/pdf/' . $name;
            $pdf->save($path);
            if ($action == 'view') {
                return $pdf->stream();
            }
            return $pdf->download($name);
        }
        return redirect()->route('devis');
    }

    public function sendMail($id)
    {
        //test if the last parameter of the url is an integer
       if(is_numeric($id)) {
            $devis = Devis::find($id);
            if (!isset($devis)) {
                return redirect()->route('devis')->with('error', "Ce devis n'existe pas !");
            }
            $user = Auth::user();
            if ($devis->user_id != $user->id) {
                return redirect()->route('devis')->with('error', "Vous n'êtes pas autorisé à consulter ce devis!");
            }
            $file = public_path() . '/pdf' . '/devis_' . $devis->numero . '_' . $devis->id . '.pdf';
            if (!file_exists($file)) {
                return redirect()->route('devis')->with('error', "Le devis n'a pas été généré. Cliquez sur la flèche verte pour le créer et le télécharger !");
            }
            $voiture = Voiture::find($devis->voiture_id);
            $clientVoiture = ClientVoiture::where('voiture_id','=',$voiture->id)->first();
            $client = Client::find($clientVoiture->client_id);
             if(empty($client->email)) {
                 return redirect()->route('devis')->with('error', "Veuillez renseigner l'email du client dans sa fiche !");
            }
            $data = array();
            $data['id'] = $devis->id;
            $data['numero'] = $devis->numero;
            $data['user_societe'] = $user->societe;
            $data['user_nom'] = $user->nom;
            $data['user_prenom'] = $user->prenom;
            $data['user_adresse'] = $user->adresse;
            $data['user_cp'] = $user->cp;
            $data['user_ville'] = $user->ville;
            $data['user_siren'] = $user->siren;
            $data['user_email'] = $user->email;
            $data['user_telephone'] = $user->telephone;
            $data['client_nom'] = $client->nom;
            $data['client_prenom'] = $client->prenom;
            $data['client_email'] = $client->email;
            return view('Devis/devis-send-mail',['data' => $data]);

       }
       return redirect()->route('devis');
    }

    public function sendMailVoiture($clientId,$id)
    {
        //test if the last parameter of the url is an integer
       if(is_numeric($id)) {
            $devis = Devis::find($id);
            if (!isset($devis)) {
                return redirect()->route('voiture',$clientId)->with('error', "Ce devis n'existe pas !");
            }
            $user = Auth::user();
            if ($devis->user_id != $user->id) {
                return redirect()->route('voiture',$clientId)->with('error', "Vous n'êtes pas autorisé à consulter ce devis!");
            }
            $file = public_path() . '/pdf' . '/devis_' . $devis->numero . '_' . $devis->id . '.pdf';
            if (!file_exists($file)) {
                return redirect()->route('voiture',$clientId)->with('error', "Le devis n'a pas été généré. Cliquez sur la flèche verte pour le créer et le télécharger !");
            }
            $voiture = Voiture::find($devis->voiture_id);
            $clientVoiture = ClientVoiture::where('voiture_id','=',$voiture->id)->first();
            $client = Client::find($clientVoiture->client_id);
             if(empty($client->email)) {
                 return redirect()->route('voiture',$clientId)->with('error', "Veuillez renseigner l'email du client dans sa fiche !");
            }
            $data = array();
            $data['id'] = $devis->id;
            $data['numero'] = $devis->numero;
            $data['user_societe'] = $user->societe;
            $data['user_nom'] = $user->nom;
            $data['user_prenom'] = $user->prenom;
            $data['user_adresse'] = $user->adresse;
            $data['user_cp'] = $user->cp;
            $data['user_ville'] = $user->ville;
            $data['user_siren'] = $user->siren;
            $data['user_email'] = $user->email;
            $data['user_telephone'] = $user->telephone;
            $data['client_nom'] = $client->nom;
            $data['client_prenom'] = $client->prenom;
            $data['client_email'] = $client->email;
            return view('Devis/devis-send-mail',['data' => $data, 'clientId' => $clientId]);
       }
       return redirect()->route('voiture',$clientId);
    }

    public function
    sendDevis(Request $data) {
        $content = $data->content;
        $subject = $data->object;
        $dest = $data->email;
        $user = Auth::user();
        $from = $user->email;
        $clientId = $data->client_id;
        if (isset($data->devisId)) {
            $devis = Devis::find($data->devisId);
        }
        $file = public_path() . '/pdf' . '/devis_' . $devis->numero . '_' . $devis->id . '.pdf';
        if (!file_exists($file)) {
            if (!empty($clientId)) {
                return redirect()->route('voiture',$clientId)->with('error', "Le devis n'a pas été généré. Cliquez sur la flèche verte pour le créer et le télécharger !");
            } else {
                return redirect()->route('devis')->with('error', "Le devis n'a pas été généré. Cliquez sur la flèche verte pour le créer et le télécharger !");
            }
        }
        try {
            Mail::raw($content, function ($message) use ($dest,$from,$subject,$content,$devis,$file) {
                $message->to($dest)
                        ->from($from)
                        ->subject($subject)
                        ->setBody($content, 'text/html');
                $message->attach($file , [
                        'as' => 'devis_' . $devis->numero . '.pdf',
                        'mime' => 'application/pdf'
                    ]);
            });
        }
        catch(\Exception $e) {
            if (!empty($clientId)) {
                return redirect()->route('voiture',$clientId)->with('error', "Erreur lors de l'envoi du mail !");
            } else {
                return redirect()->route('devis')->with('error', "Erreur lors de l'envoi du mail !");
            }
        }
        if (!empty($clientId)) {
            return redirect()->route('voiture',$clientId)->with('status', "Devis envoyé par mail !");
        } else {
            return redirect()->route('devis')->with('status', "Devis envoyé par mail !");
        }
    }
//---------------------AJAX-------------------------
    public function generateTab(){
        $partieVoitureId = $_GET['partieVoitureId'];
        $partieVoiture = PartieVoiture::find($partieVoitureId);
        $name = str_replace('_', ' ', $partieVoiture->name);
        $name = ucwords($name);
        $devisBosseId = $_GET['devisBosseId'];
        $devisId = $_GET['devisId'];
        $devisBosseBosses = DevisBosseBosse::where('devis_bosse_id',$devisBosseId)->get();
        foreach($devisBosseBosses as $devisBosseBosse){
            $bosse = Bosse::find($devisBosseBosse->bosse_id);
            if ($partieVoitureId == $bosse->partie_voiture_id) {
                return view('Devis/devis-tab-generate', ['devisBosseId' => $devisBosseId, 'devisId' => $devisId, 'partieVoitureId' => $partieVoitureId, 'bosse' => $bosse, 'name' => $name]);
            }
        }
        return view('Devis/devis-tab-generate', ['devisBosseId' => $devisBosseId, 'devisId' => $devisId, 'partieVoitureId' => $partieVoitureId, 'bosse' => null, 'name' => $name]);
    }

    public function saveBosse(){
        $devisbosseid = $_GET['devisbosseid'];
        $devisid = $_GET['devisid'];
        $partieVoitureid = $_GET['partieVoitureid'];
        $bosse1 = $_GET['bosse1'];
        $bosse2 = $_GET['bosse2'];
        $bosse3 = $_GET['bosse3'];
        $alu = $_GET['alu'];
        $dsp = $_GET['dsp'];
        $hs = $_GET['hs'];
        $reserve = $_GET['reserve'];
        $tradi = $_GET['tradi'];
        //create response tab
        $response = array();
        // Test & prepare data for save in BDD
        if (empty($bosse1)) {
            $bosse1 = '0';
        } else {
            if (!is_numeric($bosse1)) {
                $response['statut'] = 'error';
                $response['message'] = 'Les champs bosses doivent être numériques et non vides !';
                return $response;
            }
        }
        if (empty($bosse2)) {
            $bosse2 = '0';
        } else {
            if (!is_numeric($bosse2)) {
                $response['statut'] = 'error';
                $response['message'] = 'Les champs bosses doivent être numériques et non vides !';
                return $response;
            }
        }
        if (empty($bosse3)) {
            $bosse3 = '0';
        } else {
            if (!is_numeric($bosse3)) {
                $response['statut'] = 'error';
                $response['message'] = 'Les champs bosses doivent être numériques et non vides !';
                return $response;
            }
        }
        if (empty($alu)) {
            $alu = 0;
        }
        if (empty($dsp)) {
            $dsp = 0;
        }
        if (empty($hs)) {
            $hs = 0;
        }
        if (empty($reserve)) {
            $reserve = 0;
        }
        if (empty($tradi)) {
            $tradi = 0;
        }
        // Calculate UT
        $ut1 = '0';
        switch (true) {
            case ($bosse1 == '0'):
                $ut1 = '0';
            break;
            case ($bosse1 < '3'):
                $ut1 = '4.5';
            break;
            case ($bosse1 < '6'):
                $ut1 = '8';
            break;
            case ($bosse1 < '11'):
                $ut1 = '11';
            break;
            case ($bosse1 < '16'):
                $ut1 = '14.5';
            break;
            case ($bosse1 < '21'):
                $ut1 = '18.5';
            break;
            case ($bosse1 < '31'):
                $ut1 = '23.5';
            break;
            case ($bosse1 < '41'):
                $ut1 = '28.5';
            break;
            case ($bosse1 < '51'):
                $ut1 = '31.5';
            break;
            case ($bosse1 < '61'):
                $ut1 = '38.5';
            break;
            case ($bosse1 < '71'):
                $ut1 = '43.5';
            break;
            case ($bosse1 < '81'):
                $ut1 = '48';
            break;
            case ($bosse1 < '91'):
                $ut1 = '52';
            break;
            case ($bosse1 < '101'):
                $ut1 = '56.5';
            break;
            case ($bosse1 < '126'):
                $ut1 = '61';
            break;
            case ($bosse1 < '151'):
                $ut1 = '66';
            break;
            case ($bosse1 < '176'):
                $ut1 = '70.5';
            break;
            case ($bosse1 < '201'):
                $ut1 = '80';
            break;
            case ($bosse1 < '251'):
                $ut1 = '95';
            break;
            case ($bosse1 < '301'):
                $ut1 = '110';
            break;
            case ($bosse1 > '300'):
                $ut1 = '125';
            break;
        }

        $ut2 = '0';
        switch (true) {
            case ($bosse2 == '0'):
                $ut2 = '0';
            break;
            case ($bosse2 < '3'):
                $ut2 = '7.5';
            break;
            case ($bosse2 < '6'):
                $ut2 = '12';
            break;
            case ($bosse2 < '11'):
                $ut2 = '17';
            break;
            case ($bosse2 < '16'):
                $ut2 = '20';
            break;
            case ($bosse2 < '21'):
                $ut2 = '28';
            break;
            case ($bosse2 < '31'):
                $ut2 = '38';
            break;
            case ($bosse2 < '41'):
                $ut2 = '44';
            break;
            case ($bosse2 < '51'):
                $ut2 = '55';
            break;
            case ($bosse2 < '61'):
                $ut2 = '65';
            break;
            case ($bosse2 < '71'):
                $ut2 = '75';
            break;
            case ($bosse2 < '81'):
                $ut2 = '85';
            break;
            case ($bosse2 < '91'):
                $ut2 = '90';
            break;
            case ($bosse2 < '101'):
                $ut2 = '95';
            break;
            case ($bosse2 < '126'):
                $ut2 = '100';
            break;
            case ($bosse2 < '151'):
                $ut2 = '115';
            break;
            case ($bosse2 < '176'):
                $ut2 = '125';
            break;
            case ($bosse2 > '175'):
                $ut2 = '130';
            break;
        }

        $ut3 = '0';
        switch (true) {
            case ($bosse3 == '0'):
                $ut3 = '0';
            break;
            case ($bosse3 < '3'):
                $ut3 = '18';
            break;
            case ($bosse3 == '3'):
                $ut3 = '23.5';
            break;
            case ($bosse3 < '6'):
                $ut3 = '24';
            break;
            case ($bosse3 < '11'):
                $ut3 = '28';
            break;
            case ($bosse3 < '21'):
                $ut3 = '33';
            break;
            case ($bosse3 < '31'):
                $ut3 = '45';
            break;
            case ($bosse3 < '41'):
                $ut3 = '52';
            break;
            case ($bosse3 < '51'):
                $ut3 = '66';
            break;
            case ($bosse3 < '61'):
                $ut3 = '78';
            break;
            case ($bosse3 < '71'):
                $ut3 = '90';
            break;
            case ($bosse3 < '81'):
                $ut3 = '102';
            break;
            case ($bosse3 < '91'):
                $ut3 = '108';
            break;
            case ($bosse3 < '101'):
                $ut3 = '114';
            break;
            case ($bosse3 < '126'):
                $ut3 = '120';
            break;
            case ($bosse3 < '151'):
                $ut3 = '138';
            break;
            case ($bosse3 < '176'):
                $ut3 = '150';
            break;
            case ($bosse3 > '175'):
                $ut3 = '156';
            break;

        }
        //global UT
       (float)$ut = (float)$ut1 + (float)$ut2 + (float)$ut3;
       //test if occurence exist
       $bosseId = null;
       $devisBosseBosses = DevisBosseBosse::where('devis_bosse_id',$devisbosseid)->get();
       foreach($devisBosseBosses as $devisBosseBosse){
            $bosse = Bosse::find($devisBosseBosse->bosse_id);
            if ($partieVoitureid== $bosse->partie_voiture_id) {
                $bosseId = $bosse->id;
            }
        }
        if(isset($bosseId)) {
            $bosseUpdate = Bosse::find($bosseId);
            $bosseUpdate->partie_voiture_id = $partieVoitureid;
            $bosseUpdate->bosse1 = $bosse1;
            $bosseUpdate->bosse2 = $bosse2;
            $bosseUpdate->bosse3 = $bosse3;
            $bosseUpdate->dap = $dsp;
            $bosseUpdate->alu = $alu;
            $bosseUpdate->ut = (string)$ut;
            $bosseUpdate->hs = $hs;
            $bosseUpdate->reserves = $reserve;
            $bosseUpdate->tradi = $tradi;
            if ($bosseUpdate->save()) {
                $response['statut'] = 'success';
                $partieVoiture = PartieVoiture::find($partieVoitureid);
                if (isset($partieVoiture)) {
                    $name = str_replace('_', ' ', $partieVoiture->name);
                    $name = ucwords($name);
                    $response['message'] = 'La partie ' . $name . ' a bien été modifié!';
                } else {
                    $response['message'] = 'La partie de la voiture a bien été modifié !';
                }
                return $response;
            } else {
                $response['statut'] = 'error';
                $response['message'] = 'Erreur d\'enregistrement en base de données';
                return $response;
            }
        } else {
            //Create instance in BDD
         $bosseCreate = Bosse::create([
                'partie_voiture_id' => $partieVoitureid,
                'bosse1' => $bosse1,
                'bosse2' => $bosse2,
                'bosse3' => $bosse3,
                'dap' => $dsp,
                'alu' => $alu,
                'ut' => number_format($ut,2,'.',''),
                'hs' => $hs,
                'reserves' => $reserve,
                'tradi' => $tradi,
                'created_at' => date("Y-m-d"),
            ]);
         if (isset($bosseCreate)) {
            $devisBosseBosse = DevisBosseBosse::create([
                'devis_bosse_id' => $devisbosseid,
                'bosse_id' => $bosseCreate->id,
                'created_at' => date("Y-m-d"),
            ]);
            if (!isset($devisBosseBosse)) {
                $response['statut'] = 'error';
                $response['message'] = 'Erreur d\'enregistrement en base de données';
                return $response;
            }
         }

        $response['statut'] = 'success';
        $response['id'] = $partieVoitureid;
        $partieVoiture = PartieVoiture::find($partieVoitureid);
        if (isset($partieVoiture)) {
            $name = str_replace('_', ' ', $partieVoiture->name);
            $name = ucwords($name);
            $response['message'] = 'La partie ' . $name . ' a bien été enregistré !';
        } else {
            $response['message'] = 'La partie de la voiture a bien été enregistré !';
        }
        return $response;
        }

    }
}
