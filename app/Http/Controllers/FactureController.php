<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Facture as Facture;
use App\FactureLibre as FactureLibre;
use App\FactureDevis as FactureDevis;
use App\FactureStatut as FactureStatut;
use App\Client as Client;
use App\Devis as Devis;
use App\Voiture as Voiture;
use App\VoitureModele as VoitureModele;
use App\ClientVoiture as ClientVoiture;
use App\VoitureFactureLibre as VoitureFactureLibre;


use Auth;
use Carbon\Carbon;
use PDF;
use Mail;

class FactureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        $user = Auth::user();
        $factures = Facture::getAllFactureByUser($user->id);
        return view('Facture/facture', ['factures' =>$factures]);
    }

    public function indexCreateLibre()
    {
        $user = Auth::user();
        $clients = Client::where('user_id','=', $user->id)->orderBy('societe')->get();
        $marques = VoitureModele::getAllMarques();
        $numero = $user->facture_number;
        return view('Facture/facture-create-libre', ['clients' => $clients, 'marques' => $marques, 'numero' => $numero]);
    }

    public function indexCreateDevis()
    {
        $user = Auth::user();
        $clients = Client::where('user_id', $user->id)->get();
        /*$data = array();
        $devisUsers = Devis::where('user_id','=',$user->id)->get();
        foreach($devisUsers as $devisUser) {
            $voiture = Voiture::find($devisUser->voiture_id);
            $clientVoiture = ClientVoiture::where('voiture_id','=',$voiture->id)->first();
            $client = Client::find($clientVoiture->client_id);
            $data[$devisUser->id]['numero'] = $devisUser->numero;
            $data[$devisUser->id]['societe'] = $client->societe;
            $data[$devisUser->id]['id'] = $devisUser->id;
        } */
        $marques = VoitureModele::getAllMarques();
        $numero = $user->facture_number;
        return view('Facture/facture-create-devis',['clients' => $clients, 'marques' => $marques, 'numero' => $numero]);
    }

    public function indexCreateDevisFromDevis($devisId) {
        $user = Auth::user();
        $clients = Client::where('user_id', $user->id)->get();
        $devis = Devis::find($devisId);
        $clientVoiture = ClientVoiture::where('voiture_id','=',$devis->voiture_id)->first();
        $clientVoitures = ClientVoiture::where('client_id',$clientVoiture->client_id)->get();
        $clientSelect = Client::find($clientVoiture->client_id);
        $tabDevis = array();
        foreach ($clientVoitures as $cli) {
            $dev = Devis::where('voiture_id',$cli->voiture_id)->first();
            if(!empty($dev)) {
                $voiture = Voiture::find($cli->voiture_id);
                if(!empty($voiture)) {
                    $tabDevis[$dev->id]['text'] = $dev->numero . ' - ' . $voiture->immat;
                    $tabDevis[$dev->id]['value'] = $dev->id;
                }
            }
        }
        $numero = $user->facture_number;
        return view('Facture/facture-create-devis',['clients' => $clients, 'devisSelect' => $devis, 'clientSelect' => $clientSelect, 'devis' => $tabDevis, 'numero' => $numero]);
    }

    public function indexCreateDevisFromDevisVoiture($clientId, $devisId) {
        $user = Auth::user();
        $clients = Client::where('user_id', $user->id)->get();
        $devis = Devis::find($devisId);
        $clientVoiture = ClientVoiture::where('voiture_id','=',$devis->voiture_id)->first();
        $clientVoitures = ClientVoiture::where('client_id',$clientVoiture->client_id)->get();
        $clientSelect = Client::find($clientVoiture->client_id);
        $tabDevis = array();
        foreach ($clientVoitures as $cli) {
            $dev = Devis::where('voiture_id',$cli->voiture_id)->first();
            if(!empty($dev)) {
                $voiture = Voiture::find($cli->voiture_id);
                if(!empty($voiture)) {
                    $tabDevis[$dev->id]['text'] = $dev->numero . ' - ' . $voiture->immat;
                    $tabDevis[$dev->id]['value'] = $dev->id;
                }
            }
        }
        $numero = $user->facture_number;
        return view('Facture/facture-create-devis',['clients' => $clients, 'devisSelect' => $devis, 'clientSelect' => $clientSelect, 'devis' => $tabDevis, 'numero' => $numero, 'clientId' => $clientId]);
    }

    public function getFacture($id)
    {
        //test if the last parameter of the url is an integer
        if(is_numeric($id)) {
            $facture = Facture::find($id);
            if (!isset($facture)) {
                return redirect()->route('facture')->with('error', "Cette facture n'existe pas !");
            }
            $user = Auth::user();
            if ($facture->user_id != $user->id) {
                return redirect()->route('facture')->with('error', "Vous n'êtes pas autorisé à modifier cette facture!");
            }
            $data = array();
            $data['numero'] = $facture->numero;
            $data['total_ht'] = $facture->total_ht;
            $data['total_tva'] = $facture->total_tva;
            $data['total_ttc'] = $facture->total_ttc;
            if(isset($facture->nouveau_total_ht)) {
                $data['new_total_ht'] = $facture->nouveau_total_ht;
            } else {
                 $data['new_total_ht'] = null;
            }
            $data['commentaires'] = $facture->commentaires;
            $data['id'] = $facture->id;
            $factureStatut = FactureStatut::find($facture->facture_statut_id);
            $data['statut_id'] = $facture->facture_statut_id;
            $data['statut_libelle'] = $factureStatut->libelle;
            $factureStatuts = FactureStatut::all();
            foreach ($factureStatuts as $factureStatut) {
                $factureStatut->libelle = str_replace('_', ' ', $factureStatut->libelle);
            }
            $factureLibre = FactureLibre::where('facture_id','=',$facture->id)->first();
            if (isset($factureLibre)) {
                $data['type'] = 'factureLibre';
                $data['frais_libelle'] = $factureLibre->frais_libelle;
                $data['frais_prix'] = $factureLibre->frais_prix;
                $client = Client::find($factureLibre->client_id);
                $clients = Client::where('user_id', $user->id)->get();
                $clientVoitures = CLientVoiture::where('client_id',$client->id)->get();
                $allVoitures = array();
                foreach($clientVoitures as $clientVoiture) {
                    $voiture = Voiture::find($clientVoiture->voiture_id);
                    $allVoitures[$voiture->id]['text'] = $voiture->immat . ' - ' . $voiture->nom_client;
                    $allVoitures[$voiture->id]['id'] = $voiture->id;
                }
                $voitureFactureLibres = VoitureFactureLibre::where('facture_libre_id',$factureLibre->id)->get();
                foreach($voitureFactureLibres as $voitureFactureLibre) {
                    $voitures[] = Voiture::find($voitureFactureLibre->voiture_id);
                }
                $data['taux_de_remise'] = $client->taux_de_remise;
                $data['client_id'] = $client->id;
                return view('Facture/facture-modify',['facture' => $data, 'factureStatuts' => $factureStatuts, 'clients' => $clients, 'voitureSelect' => $voitures, 'voitures' => $allVoitures ]);
            } else {
                $factureDevis = FactureDevis::where('facture_id','=',$facture->id)->get();
                foreach ($factureDevis as $factureDevi) {
                    $devis[] = Devis::find($factureDevi->devis_id);
                }
                $voiture = Voiture::find($devis[0]->voiture_id);
                $clientVoiture = ClientVoiture::where('voiture_id',$voiture->id)->first();
                $client = Client::find($clientVoiture->client_id);
                $data['type'] = 'devis';

                $data['date_reparation'] = Carbon::parse($facture->created_at)->format('Y-m-d');
                $data['taux_de_remise'] = $client->taux_de_remise;
                $data['client_id'] = $client->id;
                //data by user
                $clients = Client::where('user_id', $user->id)->get();
                $allDevis = Devis::where('user_id', $user->id)->get();
                $arrayDevis = array();
                foreach($allDevis as $devi) {
                       $arrayDevis[$devi->id]['text'] =  $devi->numero . ' - ' . $voiture->immat;
                       $arrayDevis[$devi->id]['id'] =  $devi->id;
                }
                return view('Facture/facture-devis-modify',['facture' => $data, 'factureStatuts' => $factureStatuts, 'clients' => $clients, 'devisSelect' => $devis, 'devis' => $arrayDevis ]);
            }

        }
        return redirect()->route('facture');
    }

     public function getFactureVoiture($clientId, $id)
    {
        //test if the last parameter of the url is an integer
        if(is_numeric($id)) {
            $facture = Facture::find($id);
            if (!isset($facture)) {
                return redirect()->route('voiture', $clientId)->with('error', "Cette facture n'existe pas !");
            }
            $user = Auth::user();
            if ($facture->user_id != $user->id) {
                return redirect()->route('voiture', $clientId)->with('error', "Vous n'êtes pas autorisé à modifier cette facture!");
            }
            $data = array();
            $data['numero'] = $facture->numero;
            $data['total_ht'] = $facture->total_ht;
            $data['total_tva'] = $facture->total_tva;
            $data['total_ttc'] = $facture->total_ttc;
            if(isset($facture->nouveau_total_ht)) {
                $data['new_total_ht'] = $facture->nouveau_total_ht;
            } else {
                 $data['new_total_ht'] = null;
            }
            $data['commentaires'] = $facture->commentaires;
            $data['id'] = $facture->id;
            $factureStatut = FactureStatut::find($facture->facture_statut_id);
            $data['statut_id'] = $facture->facture_statut_id;
            $data['statut_libelle'] = $factureStatut->libelle;
            $factureStatuts = FactureStatut::all();
            foreach ($factureStatuts as $factureStatut) {
                $factureStatut->libelle = str_replace('_', ' ', $factureStatut->libelle);
            }
            $factureLibre = FactureLibre::where('facture_id','=',$facture->id)->first();
            if (isset($factureLibre)) {
                $data['type'] = 'factureLibre';
                $data['frais_libelle'] = $factureLibre->frais_libelle;
                $data['frais_prix'] = $factureLibre->frais_prix;
                $client = Client::find($factureLibre->client_id);
                $clients = Client::where('user_id', $user->id)->get();
                $clientVoitures = CLientVoiture::where('client_id',$client->id)->get();
                $allVoitures = array();
                foreach($clientVoitures as $clientVoiture) {
                    $voiture = Voiture::find($clientVoiture->voiture_id);
                    $allVoitures[$voiture->id]['text'] = $voiture->immat . ' - ' . $voiture->nom_client;
                    $allVoitures[$voiture->id]['id'] = $voiture->id;
                }
                $voitureFactureLibres = VoitureFactureLibre::where('facture_libre_id',$factureLibre->id)->get();
                foreach($voitureFactureLibres as $voitureFactureLibre) {
                    $voitures[] = Voiture::find($voitureFactureLibre->voiture_id);
                }
                $data['taux_de_remise'] = $client->taux_de_remise;
                $data['client_id'] = $client->id;
                return view('Facture/facture-modify',['facture' => $data, 'factureStatuts' => $factureStatuts, 'clients' => $clients, 'voitureSelect' => $voitures, 'voitures' => $allVoitures, 'clientId' => $clientId ]);
            } else {
                $factureDevis = FactureDevis::where('facture_id','=',$facture->id)->get();
                foreach ($factureDevis as $factureDevi) {
                    $devis[] = Devis::find($factureDevi->devis_id);
                }
                $voiture = Voiture::find($devis[0]->voiture_id);
                $clientVoiture = ClientVoiture::where('voiture_id',$voiture->id)->first();
                $client = Client::find($clientVoiture->client_id);
                $data['type'] = 'devis';
                $data['date_reparation'] = Carbon::parse($facture->created_at)->format('Y-m-d');
                $data['taux_de_remise'] = $client->taux_de_remise;
                $data['client_id'] = $client->id;
                //data by user
                $clients = Client::where('user_id', $user->id)->get();
                $allDevis = Devis::where('user_id', $user->id)->get();
                $arrayDevis = array();
                foreach($allDevis as $devi) {
                       $arrayDevis[$devi->id]['text'] =  $devi->numero . ' - ' . $voiture->immat;
                       $arrayDevis[$devi->id]['id'] =  $devi->id;
                }
                return view('Facture/facture-devis-modify',['facture' => $data, 'factureStatuts' => $factureStatuts, 'clients' => $clients, 'devisSelect' => $devis, 'devis' => $arrayDevis, 'clientId' => $clientId ]);
            }

        }
        return redirect()->route('voiture', $clientId);
    }

    public function createLibre(Request $data)
    {
        //data validation
        $validator = Validator::make($data->all(), [
            'numero' => 'required|string',
            'total_ht' => 'required|numeric',
            'total_tva' => 'required|numeric',
            'total_ttc' => 'required|numeric',
            'date_reparation' => 'date|nullable',
            'chassis' => 'numeric|nullable',
        ]);
        if ($validator->fails()) {
            //return errors & the old user input's
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else {
                if ($data->devis == 'facturedevis') {
                    $selectDevis = $data->get('select-devis-facture');
                    $factExist = FactureDevis::where('devis_id',$selectDevis)->first();
                    if (isset($factExist)) {
                          if(!empty($data->client_id)) {
                                $clientId = $data->client_id;
                                return redirect()->route('voiture', $clientId)->with('error', 'Facture deja existante');
                            }
                            return redirect()->route('facture')->with('error', 'Facture deja existante');
                    }
                }
            $user = Auth::user();
            $facture = Facture::create([
                'facture_statut_id' => 1,
                'numero' => $data->numero,
                'total_ht' => $data->total_ht,
                'total_tva' => $data->total_tva,
                'total_ttc' => $data->total_ttc,
                'nouveau_total_ht' => $data->new_total_ht,
                'commentaires' => $data->commentaires,
                'created_at' => date("m.d.y"),
                'user_id' => $user->id,
            ]);
            if(isset($facture)){

                if($data->devis == 'facturedevis') {
                    $selectDevis = $data->get('select-devis-facture');
                    foreach ($selectDevis as $selectDevi) {
                        FactureDevis::create([
                            'devis_id' => $selectDevi,
                            'facture_id' => $facture->id,
                            'created_at' => date("m.d.y"),
                        ]);
                    }
                } else {
                    $factureLibre = FactureLibre::create([
                        'client_id' => $data->get('select_client_voiture'),
                        'facture_id' => $facture->id,
                        'frais_libelle' => $data->frais_libelle,
                        'frais_prix' => $data->frais_prix,
                        'date_reparation' => $data->date_reparation,
                        'created_at' => date("m.d.y"),
                    ]);
                    $selectVoitures = $data->get('select-voiture-facture');
                    foreach ($selectVoitures as $selectVoiture) {
                        VoitureFactureLibre::create([
                            'voiture_id' => $selectVoiture,
                            'facture_libre_id' => $factureLibre->id,
                            'created_at' => date("m.d.y")
                        ]);
                    }
                }
            }
            $user->facture_number = ((int) $user->facture_number) + 1;
            $user->save();
            if(!empty($data->client_id)) {
                $clientId = $data->client_id;
                return redirect()->route('voiture', $clientId)->with('status', 'Facture crée!');
            }
            return redirect()->route('facture')->with('status', 'Facture créee!');
        }
    }

      public function modify($id, Request $data)
    {
        //data validation
        $validator = Validator::make($data->all(), [
            'numero' => 'required|string',
            'total_ht' => 'required|numeric',
            'total_tva' => 'required|numeric',
            'total_ttc' => 'required|numeric',
            'commentaires' => 'string|nullable',
            'date_reparation' => 'date|nullable',
        ]);
        if ($validator->fails()) {
            //return errors & the old user input's
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else {
            $facture = Facture::find($id);
            $facture->numero = $data->numero;
            $facture->total_ht = $data->total_ht;
            $facture->nouveau_total_ht = $data->new_total_ht;
            $facture->total_tva = $data->total_tva;
            $facture->total_ttc = $data->total_ttc;
            $facture->commentaires = $data->commentaires;
            $facture->facture_statut_id = $data->get('statut');
            if ($data->get('statut') == 5 ) {
                $facture->pay_at = date("Y-m-d H:i:s");

            }
            if (isset($data->facture_type) && $data->facture_type == 'libre') {
                $factureLibre = FactureLibre::where('facture_id','=',$facture->id)->first();
                $factureLibre->frais_prix = $data->frais_prix;
                $factureLibre->frais_libelle = $data->frais_libelle;
                if (!$factureLibre->save()) {
                    return redirect()->back()->with('error', "Problème lors de l'enregistrement...");
                }
                 $selectVoitures = $data->get('select-voiture-facture');
                //add new Select Devis
                foreach ($selectVoitures as $selectVoiture) {
                    $factureVoiture = VoitureFactureLibre::where('facture_libre_id','=',$factureLibre->id)->where('voiture_id',$selectVoiture)->first();
                    if(!isset($factureVoiture)) {
                         VoitureFactureLibre::create([
                            'voiture_id' => $selectVoiture,
                            'facture_libre_id' => $factureLibre->id,
                            'created_at' => date("m.d.y"),
                        ]);
                    }
                }
                $factureVoitures = VoitureFactureLibre::where('facture_libre_id',$factureLibre->id)->get();
                $notExist = false;
                foreach ($factureVoitures as $factureVoiture) {
                    foreach ($selectVoitures as $selectVoiture) {
                        if ($factureVoiture->voiture_id == $selectVoiture) {
                            $notExist = false;
                            break 1;
                        } else {
                            $notExist = true;
                        }
                    }
                    if ($notExist) {
                        $factureVoiture->delete();
                    }
                }
            } else {
                $selectDevis = $data->get('select-devis-facture');
                //add new Select Devis
                foreach ($selectDevis as $selectDevi) {
                    $factureDevis = FactureDevis::where('facture_id','=',$facture->id)->where('devis_id',$selectDevi)->first();
                    if(!isset($factureDevis)) {
                         FactureDevis::create([
                            'devis_id' => $selectDevi,
                            'facture_id' => $facture->id,
                            'created_at' => date("m.d.y"),
                        ]);
                    }
                }
                $factureDevis = FactureDevis::where('facture_id',$facture->id)->get();
                $notExist = false;
                foreach ($factureDevis as $factureDevi) {
                    foreach ($selectDevis as $selectDevi) {
                        if ($factureDevi->devis_id == $selectDevi) {
                            $notExist = false;
                            break 1;
                        } else {
                            $notExist = true;
                        }
                    }
                    if ($notExist) {
                        $factureDevi->delete();
                    }
                }
            }
            if($facture->save()){
                if(!empty($data->client_id)) {
                    $clientId = $data->client_id;
                    return redirect()->route('voiture', $clientId)->with('status', 'Facture enregistrée!');
                } else {
                    return redirect()->route('facture')->with('status', 'Facture enregistrée!');
                }
            }
            return redirect()->back()->with('error', "Problème lors de l'enregistrement...");
        }
    }

     public function delete($id, $type, $previous='')
    {
        //test if the last parameter of the url is an integer
       if(is_numeric($id)) {
            $facture = Facture::find($id);
            if (!isset($facture)) {
                if($previous==='voiture'){
                    return redirect()->back()->with('error', "Cette facture n'existe pas !");
                }
                return redirect()->route('facture')->with('error', "Cette facture n'existe pas !");
            }
            $user = Auth::user();
            if ($facture->user_id != $user->id) {
                if($previous==='voiture'){
                    return redirect()->back()->with('error', "Vous n'êtes pas autorisé à supprimer cette facture!");
                }
                return redirect()->route('facture')->with('error', "Vous n'êtes pas autorisé à supprimer cette facture!");
            }
            if ($type == 'devis') {
                $factureDeviss = FactureDevis::where('facture_id','=',$id)->get();
                foreach ($factureDeviss as $factureDevis) {
                    if (!$factureDevis->delete()){
                        if($previous==='voiture'){
                            return redirect()->back()->with('error', "Problème lors de l'effacement");
                        }
                        return redirect()->route('facture')->with('error', "Problème lors de l'effacement");
                    }
                }
                if ($facture->delete()) {
                    if($previous==='voiture'){
                        return redirect()->back()->with('status', "Facture effacée");
                    }
                    return redirect()->route('facture')->with('status', "Facture effacée");
                } else {
                    if($previous==='voiture'){
                        return redirect()->back()->with('error', "Problème lors de l'effacement");
                    }
                    return redirect()->route('facture')->with('error', "Problème lors de l'effacement");
                }
            } else {
                $factureLibre = FactureLibre::where('facture_id','=',$id)->first();
                $voitureFactureLibres = VoitureFactureLibre::where('facture_libre_id',$factureLibre->id)->get();
                foreach($voitureFactureLibres as $voitureFactureLibre) {
                    if (!$voitureFactureLibre->delete()){
                        if($previous==='voiture'){
                            return redirect()->back()->with('error', "Problème lors de l'effacement");
                        }
                        return redirect()->route('facture')->with('error', "Problème lors de l'effacement");
                    }
                }
                if (!$factureLibre->delete()){
                    if($previous==='voiture'){
                        return redirect()->back()->with('error', "Problème lors de l'effacement");
                    }
                    return redirect()->route('facture')->with('error', "Problème lors de l'effacement");
                }
                if ($facture->delete()) {
                    if($previous==='voiture'){
                        return redirect()->back()->with('status', "Facture effacée");
                    }
                    return redirect()->route('facture')->with('status', "Facture effacée");
                } else {
                    if($previous==='voiture'){
                        return redirect()->back()->with('error', "Problème lors de l'effacement");
                    }
                    return redirect()->route('facture')->with('error', "Problème lors de l'effacement");
                }
            }
        }
        if($previous==='voiture'){
            return redirect()->back();
        }
        return redirect()->route('facture');
    }

      public function deleteVoiture($clientId, $id, $type)
    {
        //test if the last parameter of the url is an integer
       if(is_numeric($id)) {
            $facture = Facture::find($id);
            if (!isset($facture)) {
                return redirect()->route('voiture', $clientId)->with('error', "Cette facture n'existe pas !");
            }
            $user = Auth::user();
            if ($facture->user_id != $user->id) {
                return redirect()->route('voiture', $clientId)->with('error', "Vous n'êtes pas autorisé à supprimer cette facture!");
            }
            if ($type == 'devis') {
                $factureDeviss = FactureDevis::where('facture_id','=',$id)->get();
                foreach ($factureDeviss as $factureDevis) {
                    if (!$factureDevis->delete()){
                        return redirect()->route('voiture', $clientId)->with('error', "Problème lors de l'effacement");
                    }
                }
                if ($facture->delete()) {
                    return redirect()->route('voiture', $clientId)->with('status', "Facture effacée");
                } else {
                    return redirect()->route('voiture', $clientId)->with('error', "Problème lors de l'effacement");
                }
            } else {
                $factureLibre = FactureLibre::where('facture_id','=',$id)->first();
                $voitureFactureLibres = VoitureFactureLibre::where('facture_libre_id',$factureLibre->id)->get();
                foreach($voitureFactureLibres as $voitureFactureLibre) {
                    if (!$voitureFactureLibre->delete()){
                        return redirect()->route('voiture', $clientId)->with('error', "Problème lors de l'effacement");
                    }
                }
                if (!$factureLibre->delete()){
                    return redirect()->route('voiture', $clientId)->with('error', "Problème lors de l'effacement");
                }
                if ($facture->delete()) {
                    return redirect()->route('voiture', $clientId)->with('status', "Facture effacée");
                } else {
                    return redirect()->route('voiture', $clientId)->with('error', "Problème lors de l'effacement");
                }
            }
        }
        return redirect()->route('voiture', $clientId);
    }

    //PDF Download
    public function downloadPdf($id, $action) {
        //test if the last parameter of the url is an integer
        if(is_numeric($id)) {
            $facture = Facture::find($id);
            if (!isset($facture)) {
                return redirect()->route('facture')->with('error', "Cette facture n'existe pas !");
            }
            $user = Auth::user();
            if ($facture->user_id != $user->id) {
                return redirect()->route('facture')->with('error', "Vous n'êtes pas autorisé à télécharger cette facture!");
            }
            $data = array();
            $data['numero'] = $facture->numero;
            $data['total_ht'] = $facture->total_ht;
            $data['total_tva'] = $facture->total_tva;
            $data['total_ttc'] = $facture->total_ttc;
            $data['new_total_ht'] = $facture->nouveau_total_ht;
            $data['remise'] = (float) $facture->total_ht - (float) $facture->nouveau_total_ht;
            $data['commentaires'] = $facture->commentaires;
            $data['id'] = $facture->id;
            $data['date_reparation'] = Carbon::parse($facture->created_at)->format('d/m/Y');;
            $data['user_societe'] = $user->societe;
            $data['user_adresse'] = $user->adresse;
            $data['user_cp'] = $user->cp;
            $data['user_ville'] = $user->ville;
            $data['user_siren'] = $user->siren;
            $data['user_email'] = $user->email;
            $data['user_logo'] = $user->logo;
            $data['user_capital_social'] = $user->capital_social;
            $factureLibre = FactureLibre::where('facture_id','=',$facture->id)->first();
            if (isset($factureLibre)) {
                $data['type'] = 'factureLibre';
                $data['frais_prix'] = $factureLibre->frais_prix;
                $data['frais_libelle'] = $factureLibre->frais_libelle;
                $voitureFactureLibres = VoitureFactureLibre::where('facture_libre_id',$factureLibre->id)->get();
                $voitureData = array();
                foreach($voitureFactureLibres as $voitureFactureLibre) {
                    $voiture = Voiture::find($voitureFactureLibre->voiture_id);
                    $voitureData[$voiture->id]['immat'] = $voiture->immat;
                    $voitureModele = VoitureModele::find($voiture->voiture_modele_id);
                    $voitureData[$voiture->id]['marque'] = $voitureModele->rappel_marque;
                    $voitureData[$voiture->id]['modele'] = $voitureModele->rappel_modele;
                }
                $client = Client::find($factureLibre->client_id);
                $data['client_societe'] = $client->societe;
                $data['client_adresse'] = $client->adresse;
                $data['client_CP'] = $client->CP;
                $data['client_ville'] = $client->ville;
                $data['client_siren'] = $client->siren;
                $data['client_email'] = $client->email;
                $data['client_taux_de_remise'] = $client->taux_de_remise;
                $pdf = PDF::loadView('pdf/facture-libre',compact('data', 'voitureData'));
            } else {
                $factureDevis = FactureDevis::where('facture_id','=',$facture->id)->get();
                $devisData = array();
                foreach ($factureDevis as $factureDevi) {
                    $devis = Devis::find($factureDevi->devis_id);
                    $voiture = Voiture::find($devis->voiture_id);
                    $voitureModele= VoitureModele::find($voiture->voiture_modele_id);
                    $devisData[$devis->id]['marque'] = $voitureModele->rappel_marque;
                    $devisData[$devis->id]['modele'] = $voitureModele->rappel_modele;
                    $devisData[$devis->id]['immat'] = $voiture->immat;
                    $devisData[$devis->id]['chassis'] = $voiture->chassis;
                    $devisData[$devis->id]['nom_client'] = $voiture->nom_client;
                    $devisData[$devis->id]['devis_numero'] = $devis->numero;
                    $devisData[$devis->id]['total_ht'] = $devis->total_ht;
                    $devisData[$devis->id]['date_reparation'] = Carbon::parse($devis->date_expertise)->format('d/m/Y');
                }
                $devisClient = Devis::find($factureDevis[0]->devis_id);
                $voitureClient = Voiture::find($devisClient->voiture_id);
                $clientVoiture = ClientVoiture::where('voiture_id','=',$voitureClient->id)->first();
                $client = Client::find($clientVoiture->client_id);
                $data['client_societe'] = $client->societe;
                $data['client_adresse'] = $client->adresse;
                $data['client_CP'] = $client->CP;
                $data['client_ville'] = $client->ville;
                $data['client_siren'] = $client->siren;
                $data['client_email'] = $client->email;
                $data['client_taux_de_remise'] = $client->taux_de_remise;
                $data['type'] = 'devis';
                $pdf = PDF::loadView('pdf/facture-devis',compact('data', 'devisData'));
            }
            $filename = time() . '.pdf';
            $name = 'facture_' . $facture->numero . '_' . $facture->id . '.pdf';
            $path = public_path(). '/pdf/' . $name;
            $pdf->save($path);
            $facture->changeStatut(2);
            if ($action == 'view') {
                return $pdf->stream();
            } else {
                return $pdf->download($name);
            }
        }
        return redirect()->route('facture');
    }

      public function sendMail($id,$type)
    {
        //test if the last parameter of the url is an integer
       if(is_numeric($id)) {
            $facture = Facture::find($id);
            if (!isset($facture)) {
                return redirect()->route('facture')->with('error', "Cette facture n'existe pas !");
            }
            $user = Auth::user();
            if ($facture->user_id != $user->id) {
                return redirect()->route('facture')->with('error', "Vous n'êtes pas autorisé à consulter cette facture !");
            }
            //test if file was generated
            $file = public_path() . '/pdf' . '/facture_' . $facture->numero . '_' . $facture->id . '.pdf';
            if (!file_exists($file)) {
                return redirect()->route('facture')->with('error', "La facture n'a pas été générée. Cliquez sur la flèche verte pour la créer et la télécharger !");
            }
            $data = array();
            $data['id'] = $facture->id;
            $data['type'] = $type;
            $data['numero'] = $facture->numero;
            $data['user_societe'] = $user->societe;
            $data['user_nom'] = $user->nom;
            $data['user_prenom'] = $user->prenom;
            $data['user_adresse'] = $user->adresse;
            $data['user_cp'] = $user->cp;
            $data['user_ville'] = $user->ville;
            $data['user_siren'] = $user->siren;
            $data['user_email'] = $user->email;
            $data['user_telephone'] = $user->telephone;
             if ($type == 'devis') {
                $factureDevis = FactureDevis::where('facture_id',$facture->id)->get();
                if(isset($factureDevis)) {
                    $devis = Devis::find($factureDevis[0]->devis_id);
                    $voiture = Voiture::find($devis->voiture_id);
                    $clientVoiture = ClientVoiture::where('voiture_id','=',$voiture->id)->first();
                    $client = Client::find($clientVoiture->client_id);
                    if(empty($client->email)) {
                         return redirect()->route('facture')->with('error', " Veuillez renseigner l'email du client dans sa fiche !");
                    }
                    $data['client_nom'] = $client->nom;
                    $data['client_prenom'] = $client->prenom;
                    $data['client_email'] = $client->email;
                }
            } else {
                $factureLibre = FactureLibre::where('facture_id','=',$facture->id)->first();
                if (isset($factureLibre)) {
                    $voitureFactureLibres = VoitureFactureLibre::where('facture_libre_id',$factureLibre->id)->get();
                    $voiture = Voiture::find($voitureFactureLibres[0]->voiture_id);
                    $clientVoiture = ClientVoiture::where('voiture_id','=',$voiture->id)->first();
                    $client = Client::find($clientVoiture->client_id);
                    if(empty($client->email)) {
                         return redirect()->route('facture')->with('error', " Veuillez renseigner l'email du client dans sa fiche !");
                    }
                    $data['client_nom'] = $client->nom;
                    $data['client_prenom'] = $client->prenom;
                    $data['client_email'] = $client->email;
                }
            }
            return view('Facture/facture-send-mail',['data' => $data]);
       }
       return redirect()->route('facture');
    }

     public function sendMailVoiture($clientId,$id,$type)
    {
        //test if the last parameter of the url is an integer
       if(is_numeric($id)) {
            $facture = Facture::find($id);
            if (!isset($facture)) {
                return redirect()->route('voiture', $clientId)->with('error', "Cette facture n'existe pas !");
            }
            $user = Auth::user();
            if ($facture->user_id != $user->id) {
                return redirect()->route('voiture', $clientId)->with('error', "Vous n'êtes pas autorisé à consulter cette facture !");
            }
            //test if file was generated
            $file = public_path() . '/pdf' . '/facture_' . $facture->numero . '_' . $facture->id . '.pdf';
            if (!file_exists($file)) {
                return redirect()->route('voiture', $clientId)->with('error', "La facture n'a pas été générée. Cliquez sur la flèche verte pour la créer et la télécharger !");
            }
            //test if client mail exist

            $data = array();
            $data['id'] = $facture->id;
            $data['type'] = $type;
            $data['numero'] = $facture->numero;
            $data['user_societe'] = $user->societe;
            $data['user_nom'] = $user->nom;
            $data['user_prenom'] = $user->prenom;
            $data['user_adresse'] = $user->adresse;
            $data['user_cp'] = $user->cp;
            $data['user_ville'] = $user->ville;
            $data['user_siren'] = $user->siren;
            $data['user_email'] = $user->email;
            $data['user_telephone'] = $user->telephone;
            if ($type == 'devis') {
                $factureDevis = FactureDevis::where('facture_id',$facture->id)->get();
                if(isset($factureDevis)) {
                    $devis = Devis::find($factureDevis[0]->devis_id);
                    $voiture = Voiture::find($devis->voiture_id);
                    $clientVoiture = ClientVoiture::where('voiture_id','=',$voiture->id)->first();
                    $client = Client::find($clientVoiture->client_id);
                    if(empty($client->email)) {
                         return redirect()->route('voiture', $clientId)->with('error', " Veuillez renseigner l'email du client dans sa fiche !");
                    }
                    $data['client_nom'] = $client->nom;
                    $data['client_prenom'] = $client->prenom;
                    $data['client_email'] = $client->email;
                }
            } else {
                $factureLibre = FactureLibre::where('facture_id','=',$facture->id)->first();
                if (isset($factureLibre)) {
                    $voitureFactureLibres = VoitureFactureLibre::where('facture_libre_id',$factureLibre->id)->get();
                    $voiture = Voiture::find($voitureFactureLibres[0]->voiture_id);
                    $clientVoiture = ClientVoiture::where('voiture_id','=',$voiture->id)->first();
                    $client = Client::find($clientVoiture->client_id);
                    if(empty($client->email)) {
                         return redirect()->route('voiture', $clientId)->with('error', " Veuillez renseigner l'email du client dans sa fiche !");
                    }
                    $data['client_nom'] = $client->nom;
                    $data['client_prenom'] = $client->prenom;
                    $data['client_email'] = $client->email;
                }
            }
            return view('Facture/facture-send-mail',['data' => $data, 'clientId' => $clientId]);
       }
       return redirect()->route('voiture', $clientId);
    }

    public function sendFacture(Request $data) {
        $content = $data->content;
        $subject = $data->object;
        $dest = $data->email;
        $user = Auth::user();
        $from = $user->email;
        $clientId = $data->client_id;
        if (isset($data->factureId)) {
            $facture = Facture::find($data->factureId);
        }
        $file = public_path() . '/pdf' . '/facture_' . $facture->numero . '_' . $facture->id . '.pdf';
        if (!file_exists($file)) {
              if(!empty($clientId)) {
                return redirect()->route('voiture', $clientId)->with('error', "La facture n'a pas été générée. Cliquez sur la flèche verte pour la créer et la télécharger !");
            } else {
                return redirect()->route('facture')->with('error', "La facture n'a pas été générée. Cliquez sur la flèche verte pour la créer et la télécharger !");
            }
        }
        try {
            Mail::raw($content, function ($message) use ($dest,$from,$subject,$content,$facture,$file) {
                $message->to($dest)
                        ->from($from)
                        ->subject($subject)
                        ->setBody($content, 'text/html');
                $message->attach($file , [
                        'as' => 'facture_' . $facture->numero . '.pdf',
                        'mime' => 'application/pdf'
                    ]);
            });
        }
        catch(\Exception $e) {
            if(!empty($clientId)) {
                return redirect()->route('voiture', $clientId)->with('error', "Erreur lors de l'envoi du mail !");
            } else {
                return redirect()->route('facture')->with('error', "Erreur lors de l'envoi du mail !");
            }
        } 
        $facture->changeStatut(3);
        if(!empty($clientId)) {
            return redirect()->route('voiture', $clientId)->with('status', "Facture envoyée par mail au client !");
        } else {
            return redirect()->route('facture')->with('status', "Facture envoyée par mail au client !");
        }
    }

    // Ajax functions
    public function getDevisByClient() {
        $clientId = $_GET['id'];
        $client = Client::find($clientId);
        $clientVoitures = ClientVoiture::where('client_id',$client->id)->get();
        $data = array();
        foreach ($clientVoitures as $clientVoiture) {
            $voiture = Voiture::find($clientVoiture->voiture_id);
            $devis = Devis::where('voiture_id',$voiture->id)->first();
            if (!empty($devis)) {
                $devisFactureExist = FactureDevis::where('devis_id',$devis->id)->first();
                if(empty($devisFactureExist)) {
                    $data[$devis->id]['devis_id'] = $devis->id;
                    $data[$devis->id]['devis'] = $devis->numero . ' - ' . $voiture->immat;
                }
            }
        }
        return response()->json(array('data' => $data), 200);
    }

     public function getVoituresByClient() {
        $clientId = $_GET['id'];
        $client = Client::find($clientId);
        $clientVoitures = ClientVoiture::where('client_id',$client->id)->get();
        $data = array();
        foreach ($clientVoitures as $clientVoiture) {
            $voiture = Voiture::find($clientVoiture->voiture_id);
            if (!empty($voiture)) {
                $data[$voiture->id]['voiture_id'] = $voiture->id;
                $data[$voiture->id]['voiture'] = $voiture->immat . ' - ' . $voiture->nom_client;
            }
        }
        return response()->json(array('data' => $data), 200);
    }

    public function getDevisInformations() {
        $devisId = $_GET['id'];
        $devis = Devis::find($devisId);
        $tabDevis['total_ht'] = $devis->total_ht;
        $tabDevis['total_ttc'] = $devis->total_ttc;
        $tabDevis['total_tva'] = $devis->total_tva;
        return response()->json(array('data' => $tabDevis), 200);

    }

    public function getTauxRemise() {
        $clientId = $_GET['client'];
        $client = Client::find($clientId);
        $taux_de_remise = $client->taux_de_remise;
        return response()->json(array('data' => $taux_de_remise), 200);
    }

}
