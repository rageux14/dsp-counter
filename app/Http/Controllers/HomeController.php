<?php

namespace App\Http\Controllers;

use App\Facture as Facture;
use App\FactureLibre as FactureLibre;
use App\FactureDevis as FactureDevis;
use App\Client as Client;
use App\ClientVoiture as ClientVoiture;
use App\Voiture as Voiture;
use App\Devis as Devis;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $clients = Client::where('user_id','=',$user->id)->get();
        $nbClients = 0;
        $nbDevis= 0;
        foreach ($clients as $client) {
            $nbClients++;
            $clientVoitures = ClientVoiture::where('client_id','=',$client->id)->get();
            foreach($clientVoitures as $clientVoiture) {
                $voiture = Voiture::find($clientVoiture->voiture_id);
                $devis = Devis::where('voiture_id','=',$voiture->id)->get();
                foreach ($devis as $devi) {
                     $nbDevis++;
                 } 
            }
        }
        $factures = Facture::where('user_id','=',$user->id)->get();
        $nbFactureLibre = 0;
        $nbFactureDevis= 0;
        foreach ($factures as $facture) {
            $factureLibre = FactureLibre::where('facture_id','=',$facture->id)->first();
            if (isset($factureLibre)) {
                $nbFactureLibre++;
            }
            $factureDevis = FactureDevis::where('facture_id','=',$facture->id)->first();
            if (isset($factureDevis)) {
                $nbFactureDevis++;
            }
        }
        return view('home',['nbClients' => $nbClients, 'nbFactureDevis' => $nbFactureDevis, 'nbDevis' => $nbDevis, 'nbFactureLibre' => $nbFactureLibre, 'userLogo' => $user->logo]);
    }

    public function indexAddLogo() {
        $user = Auth::user();
        $contents = $user->logo;
        return view('add-logo',['logo' => $contents]);
    }

    public function addLogo(Request $request) {
        if (filesize($request->file('image')) < 2048){
                return redirect()->route('add-logo')->with('error', "Le fichier est trop volumineux !"); 
            }
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = time() . '.'. $image->getClientOriginalExtension();
            $extension = \File::extension($filename);
            if ($extension == 'jpg' || $extension == 'png') {
                $imagepath = Storage::disk('public')->put('', $image);
                $user = Auth::user();
                $user->logo = $imagepath;
                if ($user->save()) {
                    return redirect()->route('add-logo')->with('status', "Logo enregistré");
                }
            }
            return redirect()->route('add-logo')->with('error', "L'extension du fichier est incorrecte !");
        }
        return redirect()->route('add-logo')->with('error', "Pas de fichier trouvé");    
    }
}
