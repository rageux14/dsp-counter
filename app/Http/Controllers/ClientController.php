<?php

namespace App\Http\Controllers;

use App\Library\Services\Slug;
use Illuminate\Http\Request;
use App\Client as Client;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $clients = Client::where('user_id','=',$user->id)->orderBy('id','desc')->get();
        return view('Client/client', ['clients' => $clients]);
    }

    public function indexCreate($previous="")
    {
        return view('Client/client-create',['route' => 'clients']);
    }

    public function indexCreateOther($route)
    {
        if ($route == 'facturelibre'){
            return view('Client/client-create',['route' => $route]);
        }
        if ($route == 'devisbosse') {
            return view('Client/client-create',['route' => $route]);
        }
        return view('Client/client-create',['route' => 'clients']);
    }
    public function getClient($id)
    {
        //test if the last parameter of the url is an integer
        if(is_numeric($id)) {
            $client = Client::find($id);
            if (!isset($client)) {
                return redirect()->route('client')->with('error', "Ce client n'existe pas !");
            }
            $user = Auth::user();
            if ($client->user_id != $user->id) {
                return redirect()->route('client')->with('error', "Vous n'êtes pas autorisé à modifier cette fiche client!");
            }
            return view('Client/client-modify', ['client' =>$client]);
        }
        $user = Auth::user();
        $clients = Client::where('user_id','=', $user->id)->get();
        return redirect()->route('client', ['clients' =>$clients]);
    }

    public function create(Request $data, $previous="")
    {


        //data validation
        $validator = Validator::make($data->all(), [
            'nom' => 'string|max:255|nullable',
            'prenom' => 'string|max:255|nullable',
            'email' => 'nullable|string|email|max:255',
            'societe' => 'nullable|string',
            'adresse' => 'required|string',
            'cp' => 'required|string|size:5',
            'ville' => 'required|string',
            'siret' => 'string|size:14|nullable',
            'telephone' => 'string|size:10|nullable',
            'taux' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            //return errors & the old user input's
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else {
            //Check unique mail
            $user = Auth::user();
            $clients = CLient::where('user_id', $user->id)->get();
            foreach ($clients as $client) {
                if ($client->email == $data->email) {
                    return redirect()->back()->withInput()->with('error', "Ce client existe déja (mail existant) ! ");
                }
            }
            //create an instance in the BDD
            Client::create([
                'nom' => $data->nom,
                'prenom' => $data->prenom,
                'email' => $data->email,
                'societe' => $data->societe,
                'adresse' => $data->adresse,
                'cp' => $data->cp,
                'ville' => $data->ville,
                'siren' => $data->siret,
                'telephone' => $data->telephone,
                'taux_de_remise' => $data->taux,
                'created_at' => date("d.m.y"),
                'user_id' => $user->id,
            ]);

            if ($previous==='modifyVoit'){
                return redirect()->action('VoitureController@index2')->with('status', 'Client ajouté');
            }
            if ($previous==='index') {
                return redirect()->action('VoitureController@indexCreate',['id'=>$user->id,'previous'=>'index'])->with('status', 'Client ajouté')->withInput();

            }
            // if the old route is facture-create

            if(isset($data->route) && $data->route == 'clients') {
                return redirect()->route('client')->with('status', 'Fiche client crée!');
            } else if(isset($data->route) && $data->route == 'devishoraire') {
                return redirect()->route('devis-create-horaire')->with('status', 'Fiche client crée!');
            }

            return redirect()->route('facture-create-libre')->with('status', 'Fiche client crée!');
        }
    }

      public function modify($id, Request $data)
    {
        //data validation
        $validator = Validator::make($data->all(), [
            'nom' => 'string|max:255|nullable',
            'prenom' => 'string|max:255|nullable',
            'email' => 'required|string|email|max:255',
            'societe' => 'nullable|string',
            'adresse' => 'required|string',
            'cp' => 'required|string|size:5',
            'ville' => 'required|string',
            'siret' => 'string|size:14|nullable',
            'telephone' => 'string|size:10|nullable',
            'taux' => 'required|string|size:2',
        ]);
        if ($validator->fails()) {
            //return errors & the old user input's
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else {
            //Check unique mail
            $user = Auth::user();
            $clients = CLient::where('user_id', $user->id)->get();
            foreach ($clients as $client) {
                if ($client->email == $data->email) {
                    $clientCourant = Client::find($id);
                    if ($clientCourant->email != $client->email) {
                        return redirect()->back()->withInput()->with('error', "Ce client existe déja (mail existant) ! ");
                    }
                }
            }
            $client = Client::find($id);
            $client->nom = $data->nom;
            $client->prenom = $data->prenom;
            $client->email = $data->email;
            $client->societe = $data->societe;
            $client->adresse = $data->adresse;
            $client->CP = $data->cp;
            $client->ville = $data->ville;
            $client->siren = $data->siret;
            $client->telephone = $data->telephone;
            $client->taux_de_remise = $data->taux;
            if($client->save()){
                return redirect()->route('client')->with('status', 'Fiche client enregistrée!');
            }
                return redirect()->back()->with('error', "Problème lors de l'enregistrement...");
        }
    }

     public function delete($id)
    {
        //test if the last parameter of the url is an integer
        if(is_numeric($id)) {
            $client = Client::find($id);
            if (!isset($client)) {
                return redirect()->route('client')->with('error', "Ce client n'existe pas !");
            }
            $user = Auth::user();
            if ($client->user_id != $user->id) {
                return redirect()->route('client')->with('error', "Vous n'êtes pas autorisé à supprimer cette fiche client!");
            }
            if($client->delete()){
                return redirect()->route('client')->with('status', 'Fiche client effacée');
            }
            return redirect()->route('client')->with('error', "Problème lors de l'effacement");
        }
        $user = Auth::user();
        $clients = Client::where('user_id','=', $user->id)->get();
        return redirect()->route('client', ['clients' =>$clients]);
    }

    public function getAllClientByUser($id=""){

        $clients=Client::orderBy('societe')->get();
        return $clients;
    }

   /* public function search (Request $request , Slug $slug)
    {
        $user= Auth::user();
        $search= $request->get('search');
        $search=$slug->slugify($search);
        $clients=Client::whereIn('slug', [$search])->get();
        return view('Client/client', [
            'clients' => $clients
        ]);
    } */



}
