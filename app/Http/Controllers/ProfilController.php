<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\User as User;

use Auth;

class ProfilController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('profil',['user'=> $user]);
    }

    public function modify(Request $data) {
        //data validation
        $validator = Validator::make($data->all(), [
            'nom' => 'required|string|max:255',
            'prenom' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'societe' => 'required|string',
            'adresse' => 'required|string',
            'cp' => 'required|string|size:5',
            'ville' => 'required|string',
            'siret' => 'required|string|size:14',
            'capital_social' => 'required|numeric',
            'telephone' => 'required|string|size:10',
            'facture_number' => 'nullable|numeric'
        ]);
        if ($validator->fails()) {
            //return errors & the old user input's
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else {
            //Check unique mail
            $me = Auth::user();
            $users = User::all();
            foreach ($users as $user) {
                if ($user->email == $data->email && $me->email != $data->email) {
                    return redirect()->back()->withInput()->with('error', "Ce mail est déjà utilisé !");
                }
            }
            //check numeric SIRET
            if(!is_numeric($data->siret)) {
                return redirect()->back()->withInput()->with('error', "Le numéro de SIRET doit être numérique !");
            }
            $user = User::find($data->id);
            $user->nom = $data->nom;
            $user->prenom = $data->prenom;
            $user->email = $data->email;
            $user->societe = $data->societe;
            $user->adresse = $data->adresse;
            $user->cp = $data->cp;
            $user->capital_social = $data->capital_social;
            $user->ville = $data->ville;
            $user->siren = $data->siret;
            $user->telephone = $data->telephone;
            $user->facture_number = $data->facture_number;
            if($user->save()){
                return redirect()->route('home')->with('status', 'Informations modifiées');
            }
                return redirect()->back()->with('error', "Problème lors de l'enregistrement...");
        }
    }
}
