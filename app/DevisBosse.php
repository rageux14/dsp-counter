<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DevisBosse extends Model
{
    //link to bdd table
    protected $table = 'devis_bosse';

    public $timestamps = false;

    protected $fillable = [
        'devis_id','degarnissage', 'frais_de_dossier', 'dsp','created_at'
    ];

    public function devis () {
    	return $this->hasOne('Devis');
    }

    public function bosses() {
    	return $this->belongsToMany('Bosse');
    }
}
