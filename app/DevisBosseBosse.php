<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DevisBosseBosse extends Model
{
    //link to bdd table
    protected $table = 'devis_bosse_bosse';

    public $timestamps = false;

    protected $fillable = [
        'devis_bosse_id','bosse_id','created_at'
    ];

}
