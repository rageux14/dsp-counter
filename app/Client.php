<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //link to bdd table
    protected $table = 'client';

    protected $fillable = [
        'nom','prenom', 'email', 'nom','societe','logo','adresse','cp','ville','siren','telephone','taux_de_remise','created_at','user_id'
    ];

    public $timestamps = false;

    public function factureLibre() {
    	return $this->belongsTo('FactureLibre');
    }

    public function voitures() {
    	return $this->belongsToMany('Voiture');
    }

    public function users() {
    	return $this->belongsToMany('User');
    }
}
