<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FactureStatut extends Model
{
    //link to bdd table
    protected $table = 'facture_statut';

    public $timestamps = false;

    protected $fillable = [
        'order','libelle'
    ];

    public function facture() {
    	return $this->hasOne('Facture');
    }
}
