<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\DevisBosse as DevisBosse;
use App\DevisHoraire as DevisHoraire;
use App\Voiture as Voiture;
use App\ClientVoiture as ClientVoiture;
use App\Client as Client;

class Devis extends Model
{
    //link to bdd table
    protected $table = 'devis';

    protected $fillable = [
        'voiture_id','numero', 'total_ht', 'total_tva','total_ttc','commentaires','user_id','taux_horaire', 'date_reparation', 'date_expertise','nom_expert','numero_sinistre', 'frais_libelle', 'frais_prix'
    ];

    public $timestamps = false;

    public function voiture() {
    	return $this->belongsTo('\App\Voiture');
    }

    public function devisBosse() {
    	return $this->belongsTo('\App\DevisBosse');
    }

    public function devisHoraire() {
    	return $this->belongsTo('\App\DevisHoraire');
    }

    public function facture() {
    	return $this->belongsTo('\App\Facture');
    }


     public static function getAllDevisByUser($userId) {
        //init new array for put data
        $data = array();
        // get all factures by user
        $devis = Devis::where('user_id','=',$userId)->orderBy('id','desc')->get();
        // get all type of factures
        $devisBosses = DevisBosse::all();
        $devisHoraires = DevisHoraire::all();
        //first loop to get type
        foreach($devis as $devi) {
            // Check and get type
            foreach($devisBosses as $devisBosse) {
                if ($devi->id == $devisBosse->devis_id) {
                    $data[$devi->id]['type'] = 'bosse';
                }
            }
            foreach($devisHoraires as $devisHoraire) {
                if ($devi->id == $devisHoraire->devis_id) {
                    $data[$devi->id]['type'] = 'forfait';
                }
            }
        }
        //second loop to get data
        foreach($devis as $devi) {
            //global data
            $data[$devi->id]['id'] = $devi->id;
            $data[$devi->id]['numero'] = $devi->numero;
            $data[$devi->id]['total_ht'] = $devi->total_ht;
            $data[$devi->id]['total_tva'] = $devi->total_tva;
            $data[$devi->id]['total_ttc'] = $devi->total_ttc;
            $data[$devi->id]['commentaires'] = $devi->commentaires;
            $data[$devi->id]['taux_horaire'] = $devi->taux_horaire;
            $voiture = Voiture::find($devi->voiture_id);
            $data[$devi->id]['immat'] = $voiture->immat;
            $voitureModele = VoitureModele::find($voiture->voiture_modele_id);
            $data[$devi->id]['marque'] = $voitureModele->rappel_marque;
            $data[$devi->id]['modele'] = $voitureModele->rappel_modele;
            $clientVoiture = ClientVoiture::where('voiture_id','=',$voiture->id)->first();
            $client = Client::find($clientVoiture->client_id);
            $data[$devi->id]['societe'] = $client->societe;
            $data[$devi->id]['nom'] = $client->nom . ' ' . $client->prenom;
            $factureDevis = FactureDevis::where('devis_id', $devi->id)->first();
            if(!empty($factureDevis)) {
                $data[$devi->id]['facture'] = $factureDevis->facture_id;
            }

        }
        return $data;
    }


    public static function getAllDevisByClient($clientId) {
        //init new array for put data
        $tabDevis = array();
        $data = array();
        $clientVoitures = ClientVoiture::where('client_id', $clientId)->orderBy('id','desc')->get();
        foreach ($clientVoitures as $clientVoiture) {
            $dev = Devis::where('voiture_id',$clientVoiture->voiture_id)->first();
            if (isset($dev)) {
                $tabDevis[$dev->id] = $dev;
            }
        }
        // get all type of factures
        $devisBosses = DevisBosse::all();
        $devisHoraires = DevisHoraire::all();
        //first loop to get type
        foreach($tabDevis as $devi) {
            // Check and get type
            foreach($devisBosses as $devisBosse) {
                if ($devi->id == $devisBosse->devis_id) {
                    $data[$devi->id]['type'] = 'bosse';
                }
            }
            foreach($devisHoraires as $devisHoraire) {
                if ($devi->id == $devisHoraire->devis_id) {
                    $data[$devi->id]['type'] = 'forfait';
                }
            }
        }
        //second loop to get data
        foreach($tabDevis as $devi) {
            //global data
            $data[$devi->id]['id'] = $devi->id;
            $data[$devi->id]['numero'] = $devi->numero;
            $data[$devi->id]['total_ht'] = $devi->total_ht;
            $data[$devi->id]['total_tva'] = $devi->total_tva;
            $data[$devi->id]['total_ttc'] = $devi->total_ttc;
            $data[$devi->id]['commentaires'] = $devi->commentaires;
            $data[$devi->id]['taux_horaire'] = $devi->taux_horaire;
            $voiture = Voiture::find($devi->voiture_id);
            $data[$devi->id]['immat'] = $voiture->immat;
            $voitureModele = VoitureModele::find($voiture->voiture_modele_id);
            $data[$devi->id]['marque'] = $voitureModele->rappel_marque;
            $data[$devi->id]['modele'] = $voitureModele->rappel_modele;
            $clientVoiture = ClientVoiture::where('voiture_id','=',$voiture->id)->first();
            $client = Client::find($clientVoiture->client_id);
            $data[$devi->id]['societe'] = $client->societe;
            $data[$devi->id]['nom'] = $client->nom . ' ' . $client->prenom;
            $factureDevis = FactureDevis::where('devis_id', $devi->id)->first();
            if(!empty($factureDevis)) {
                $data[$devi->id]['facture'] = $factureDevis->facture_id;
            }
        }
        return $data;
    }

    public static function getDevisType ($devisID){
        $type='';
        if(DevisBosse::where('devis_id', $devisID )->first()===null){
            $type='horaire';
        }else{
            $type='bosse';
        }
        return $type;
    }

}
