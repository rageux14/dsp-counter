<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FactureLibre extends Model
{
    //link to bdd table
    protected $table = 'facture_libre';

    public $timestamps = false;

    protected $fillable = [
        'client_id','facture_id', 'frais_libelle', 'frais_prix'
    ];

}
