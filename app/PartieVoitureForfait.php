<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartieVoitureForfait extends Model
{
    //link to bdd table
    protected $table = 'partie_voiture_forfait';

    protected $fillable = [
        'devis_horaire_id','partie_voiture_id','prix','dap','alu','reserves', 'ut', 'hs','tradi', 'created_at'
    ];
    
    public $timestamps = false;

}
