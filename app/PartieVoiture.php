<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartieVoiture extends Model
{
    //link to bdd table
    protected $table = 'partie_voiture';

    public $timestamps = false;

    //relationship
    public function bosses() {
    	return $this->hasMany('PartieVoiture');
    }
}
