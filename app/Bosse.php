<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bosse extends Model
{
    //link to bdd table
    protected $table = 'bosse';

    public $timestamps = false;

    protected $fillable = [
        'partie_voiture_id','bosse1', 'bosse2', 'bosse3','dap','dap_value','alu','alu_value','ut','reserves','hs','tradi','taux_de_remise','created_at'
    ];

    //relationships
    public function partieVoiture() {
    	return $this->belongsTo('PartieVoiture');
    }

    public function devisBosses() {
    	return $this->belongsToMany('DevisBosse');
    }
}
