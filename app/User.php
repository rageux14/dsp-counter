<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    //link to bdd table
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom','prenom', 'email', 'password','nom','societe','logo','adresse','cp','ville','siren','telephone','capital_social', 'facture_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function factureLibre() {
        return $this->belongsTo('\App\FactureLibre');
    }

    public function devis() {
        return $this->belongsToMany('\App\Devis');
    }

    public function clients() {
        return $this->belongsToMany('\App\Client');
    }


    public function voiture() {
        return $this->belongsToMany('\App\Voiture');
    }
}
