<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FactureLibre as FactureLibre;
use App\FactureDevis as FactureDevis;
use App\FactureStatut as FactureStatut;
use App\Devis as Devis;
use App\Voiture as Voiture;
use App\VoitureModele as VoitureModele;
use App\Client as Client;
use App\ClientVoiture as ClientVoiture;
use App\VoitureFactureLibre as VoitureFactureLibre;

use Auth;
use Carbon\Carbon;

class Facture extends Model
{
    //link to bdd table
    protected $table = 'facture';

    public $timestamps = false;

    protected $fillable = [
        'facture_statut_id','numero', 'total_ht', 'total_tva','total_ttc','commentaires','user_id', 'nouveau_total_ht','pay_at'
    ];

    public function factureStatut() {
    	return $this->belongsTo('\App\FactureStatut');
    }

    public static function getAllFactureByUser($userId) {
    	//init new array for put data
    	$data = array();
    	// get all factures by user
    	$factures = Facture::where('user_id','=',$userId)->orderBy('id','desc')->get();
    	// get all type of factures
    	$factureLibres = FactureLibre::all();
    	$factureDevis = FactureDevis::all();
    	//first loop to get type
    	foreach($factures as $facture) {
    		// Check and get type
    		foreach($factureLibres as $factureLibre) {
    			if ($facture->id == $factureLibre->facture_id) {
    				$data[$facture->id]['type'] = 'libre';
    			}
    		}
    		foreach($factureDevis as $factureDevi) {
    			if ($facture->id == $factureDevi->facture_id) {
    				$data[$facture->id]['type'] = 'devis';
    			}
    		}
    	}
    	//second loop to get data
    	foreach($factures as $facture) {
    		//global data
    		$data[$facture->id]['id'] = $facture->id;
    		$data[$facture->id]['numero'] = $facture->numero;
    		$data[$facture->id]['total_ht'] = $facture->total_ht;
    		$data[$facture->id]['total_tva'] = $facture->total_tva;
    		$data[$facture->id]['total_ttc'] = $facture->total_ttc;
    		$data[$facture->id]['commentaires'] = $facture->commentaires;
    		// get statut libelle
    		$statut = FactureStatut::find($facture->facture_statut_id);
            switch($statut->id) {
                case 1:
                    $data[$facture->id]['color'] = 'white';
                    break;
                case 2:
                    $data[$facture->id]['color'] = '#ff6666';
                    break;
                case 3:
                    $data[$facture->id]['color'] = '#ff9933';
                    break;
                case 4:
                    $data[$facture->id]['color'] = '#ffcc66';
                    break;
                case 5:
                    $data[$facture->id]['color'] = '#99ff66';
                    if ($facture->pay_at == null)  {
                        // si pas de date renseigné
                        $data[$facture->id]['pay_at'] = 1;
                    } else {
                        $data[$facture->id]['pay_at'] = Carbon::parse($facture->pay_at)->format('d/m/Y');
                    }
                    break;
            }
    		$data[$facture->id]['statut'] = str_replace('_', ' ', $statut->libelle);
    		//specific data
    		if (isset($data[$facture->id]['type']) && $data[$facture->id]['type'] == 'devis') {
    			$factureDevi = FactureDevis::where('facture_id','=',$facture->id)->first();
    			$data[$facture->id]['devis_id'] = $factureDevi->devis_id;
    			$devis = Devis::find($factureDevi->devis_id);
    			$data[$facture->id]['num_devis'] = $devis->numero;
    			$data[$facture->id]['taux_horaire'] = $devis->taux_horaire;
    			$voiture = Voiture::find($devis->voiture_id);
                $clientVoiture = ClientVoiture::where('voiture_id','=',$voiture->id)->first();
                $client = Client::find($clientVoiture->client_id);
                $data[$facture->id]['societe'] = $client->societe;
                $data[$facture->id]['nom'] = $client->nom . ' ' . $client->prenom;
    		} else if (isset($data[$facture->id]['type']) && $data[$facture->id]['type'] == 'libre') {
    			$factureLibre = FactureLibre::where('facture_id','=',$facture->id)->first();
    			$client = Client::find($factureLibre->client_id);
    			$data[$facture->id]['client_id'] = $factureLibre->client_id;
    			$data[$facture->id]['societe'] = $client->societe;
    			$data[$facture->id]['nom'] = $client->nom . ' ' . $client->prenom;
    		}
    	}
    	return $data;
    }

     public static function getAllFactureByClient($clientId) {
        //init new array for put data
        $data = array();
        //get 2 types of factures in this array
        $factures = array();
        $factureLibres = FactureLibre::where('client_id',$clientId)->orderBy('id','desc')->get();
        foreach($factureLibres as $factureLibre) {
            $fac = Facture::findOrFail($factureLibre->facture_id);
            if (!empty($fac)) {
                 $factures[] = $fac;
            }
        }
        $clientVoitures = ClientVoiture::where('client_id',$clientId)->orderBy('id','desc')->get();
        $voitures = array();
        foreach ($clientVoitures as $clientVoiture) {
            $voit = Voiture::findOrFail($clientVoiture->voiture_id);
            if (!empty($voit)) {
                $voitures[] = $voit;
            }
        }
        $devis = array();
        foreach ($voitures as $voiture) {
            $dev = Devis::where('voiture_id', $voiture->id)->first();
            if (!empty($dev)) {
                $devis[] = $dev;
            }
        }
        $factureDevis = array();
        foreach ($devis as $devi) {
            $factureDevis[] = FactureDevis::where('devis_id','=', $devi->id)->orderBy('id','desc')->get();
        }
        foreach($factureDevis as $factureDevi){
            foreach( $factureDevi as $fact) {
                $factures[] = Facture::find($fact->facture_id);
            }
        }
        $user = Auth::user();
        foreach($factures as $facture) {
            if($facture->user_id == $user->id) {
                // Check and get type
                foreach($factureLibres as $factureLibre) {
                    if ($facture->id == $factureLibre->facture_id) {
                        $data[$facture->id]['type'] = 'libre';
                    }
                }
                foreach($factureDevis as $factureDevi) {
                    foreach( $factureDevi as $fact) {
                        if ($facture->id == $fact->facture_id) {
                            $data[$facture->id]['type'] = 'devis';
                        }
                    }
                }
            }
        }
        //second loop to get data

        foreach($factures as $facture) {
            //global data
            if($facture->user_id == $user->id) {
                $data[$facture->id]['id'] = $facture->id;
                $data[$facture->id]['numero'] = $facture->numero;
                $data[$facture->id]['total_ht'] = $facture->total_ht;
                $data[$facture->id]['total_tva'] = $facture->total_tva;
                $data[$facture->id]['total_ttc'] = $facture->total_ttc;
                $data[$facture->id]['commentaires'] = $facture->commentaires;
                // get statut libelle
                $statut = FactureStatut::find($facture->facture_statut_id);
                $data[$facture->id]['statut'] = str_replace('_', ' ', $statut->libelle);
                //specific data
                if (isset($data[$facture->id]['type']) && $data[$facture->id]['type'] == 'devis') {
                    $factureDevi = FactureDevis::where('facture_id','=',$facture->id)->first();
                    $data[$facture->id]['devis_id'] = $factureDevi->devis_id;
                    $devis = Devis::find($factureDevi->devis_id);
                    $data[$facture->id]['num_devis'] = $devis->numero;
                    $data[$facture->id]['taux_horaire'] = $devis->taux_horaire;
                    $voiture = Voiture::find($devis->voiture_id);
                    $clientVoiture = ClientVoiture::where('voiture_id','=',$voiture->id)->first();
                    $client = Client::find($clientVoiture->client_id);
                    $data[$facture->id]['client_id'] = $client->id;
                    $data[$facture->id]['societe'] = $client->societe;
                    $data[$facture->id]['nom'] = $client->nom . ' ' . $client->prenom;
                } else if (isset($data[$facture->id]['type']) && $data[$facture->id]['type'] == 'libre') {
                    $factureLibre = FactureLibre::where('facture_id','=',$facture->id)->first();
                    $client = Client::find($factureLibre->client_id);
                    $data[$facture->id]['client_id'] = $factureLibre->client_id;
                    $data[$facture->id]['societe'] = $client->societe;
                    $data[$facture->id]['nom'] = $client->nom . ' ' . $client->prenom;
                }
            }
        }
        return $data;
    }

    public static function getFactureType($factureID){
        $type='';
        if(FactureDevis::find($factureID)===null){
            $type='devis';
        }else{
            $type='libre';
        }
    }

    public function changeStatut($idStatut) {
        $facture = Facture::find($this->id);
        $statutSaved = $facture->facture_statut_id;
        if ($statutSaved != 4 && $statutSaved != 5 && $statutSaved != 3 && $idStatut != 1 ) {
            $facture = Facture::find($this->id);
            $facture->facture_statut_id = $idStatut;
            if (!$facture->save()) {
                return "Erreur d'enregistrement en base de données";
            }
        }    
    }
}
