<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB as DB;

class VoitureModele extends Model
{
    //link to bdd table
    protected $table = 'voiture_modele';

    public $timestamps = false;

    protected $fillable = [
        'rappel_marque', 'rappel_modele', 'version','generation','energie','pa'
    ];


    public static function getAllMarques(){
        $marques = VoitureModele::select('rappel_marque')
                                ->distinct()
                                ->get();
        return $marques;
    }

}

