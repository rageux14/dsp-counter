<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DevisHoraire extends Model
{
    //link to bdd table
    protected $table = 'devis_horaire';

    public $timestamps = false;

    protected $fillable = [
        'devis_id','nb_heures', 'total_dsp', 'degarnissage', 'frais_de_dossier'
    ];


    public function devis () {
    	return $this->hasOne('Devis');
    }
}
