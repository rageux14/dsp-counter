<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FactureDevis extends Model
{
    //link to bdd table
    protected $table = 'facture_devis';

    public $timestamps = false;

    protected $fillable = [
        'devis_id','facture_id'
    ];

    public function devis() {
    	return $this->hasOne('\App\Devis');
    }
}
