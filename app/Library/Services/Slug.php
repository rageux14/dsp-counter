<?php


namespace App\Library\Services;


class Slug
{

    public function slugify($string)
    {
        $string=str_replace(' ', '',$string);
        $string=str_replace('-', '',$string);
        $string=strtoupper($string);
        return $string;
    }

}
