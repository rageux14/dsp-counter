<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoitureFactureLibre extends Model
{

	protected $fillable = [
        'facture_libre_id','voiture_id', 'created_at'
    ];
    //link to bdd table
    protected $table = 'voiture_facture_libre';

    public $timestamps = false;

}
