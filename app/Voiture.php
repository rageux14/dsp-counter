<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voiture extends Model
{
    //link to bdd table
    protected $table = 'voiture';

    public $timestamps = false;

    protected $fillable = [
        'immat', 'voiture_modele_id' ,'assurance','kilometres','couleur','created_at', 'chassis', 'nom_client','user_id','slug'
    ];

    //relationship
    public function devis() {
    	return $this->hasOne('App\Devis');
    }

    public function client() {
    	return $this->belongsTo('App\Client');
    }



    public static function getAllVoitureByUser ($userId){
        $voitures = Voiture::where('user_id', '=', $userId )->get();
        return $voitures;
    }


}

