<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => "Merci de vérifier votre email ou votre mot de passe",
    'throttle' => "'Trop d'essais, merci de réessayer dans 30 secondes",

];
