<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Votre mot de passe doit contenir au moins 6 caractères.',
    'reset' => 'Votre mot de passe a bien été réinitialisé!',
    'sent' => 'Le lien de réinitialisation a été envoyé par mail.',
    'token' => 'Le token est invalide.',
    'user' => "Aucun utilisateur n'a été trouvé avec cette adresse mail.",

];
