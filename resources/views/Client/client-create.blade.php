@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-12">
        	<div class="col-md-6 col-form-label text-md-right section-subtitle">
        		Créer un nouveau client
        	</div>
        	<div class="content-form">
        	@if (session('status'))
			    <div class="alert alert-success alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('status') }}
			    </div>
			@elseif (session('error'))
				<div class="alert alert-danger alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('error') }}
			    </div>
			@endif
                @if(Route::currentRouteName()=="client-modify-voiture")
                    <form method="POST" action="{{ route('createfromvoiture', 'modifyVoit') }}" aria-label="{{ __('Create Client') }}">
                        @endif
                @if(Route::currentRouteName()=="client-create-voiture")
                    <form method="POST" action="{{ route('createfromvoiture', 'index') }}" aria-label="{{ __('Create Client') }}">

                    @else
	        	 <form method="POST" action="{{ route('client-create') }}" aria-label="{{ __('Create Client') }}">
                         @endif
	                @csrf
	                <input type="hidden" id="route" name="route" value="{{$route}}"/>
	                <div class="form-group row">
	                    <label for="societe" class="col-md-2 col-form-label text-md-right">{{ __('Société') }}</label>

	                    <div class="col-md-6">
	                        <input id="societe" font-size="16px" type="text" class="form-control{{ $errors->has('societe') ? ' is-invalid' : '' }}" name="societe" value="{{ old('societe') }}" autofocus>

	                        @if ($errors->has('societe'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('societe') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>
	                <div class="form-group row">
	                    <label for="nom" class="col-md-2 col-form-label text-md-right">{{ __('Nom') }}</label>

	                    <div class="col-md-6">
	                        <input id="nom" type="text" class="form-control{{ $errors->has('nom') ? ' is-invalid' : '' }}" name="nom" value="{{ old('nom') }}" autofocus>

	                        @if ($errors->has('nom'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('nom') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="prenom" class="col-md-2 col-form-label text-md-right">{{ __('Prénom') }}</label>

	                    <div class="col-md-6">
	                        <input id="prenom" type="text" class="form-control{{ $errors->has('prenom') ? ' is-invalid' : '' }}" name="prenom" value="{{ old('prenom') }}" autofocus>

	                        @if ($errors->has('prenom'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('prenom') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="adresse" class="col-md-2 col-form-label text-md-right">{{ __('Adresse') }}</label>

	                    <div class="col-md-6">
	                        <input id="adresse" type="text" class="form-control{{ $errors->has('adresse') ? ' is-invalid' : '' }}" name="adresse" value="{{ old('adresse') }}" required autofocus>

	                        @if ($errors->has('adresse'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('adresse') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="cp" class="col-md-2 col-form-label text-md-right">{{ __('Code postal') }}</label>

	                    <div class="col-md-6">
	                        <input id="cp" type="text" class="form-control{{ $errors->has('cp') ? ' is-invalid' : '' }}" name="cp" value="{{ old('cp') }}" required autofocus>

	                        @if ($errors->has('cp'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('cp') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="ville" class="col-md-2 col-form-label text-md-right">{{ __('Ville') }}</label>

	                    <div class="col-md-6">
	                        <input id="ville" type="text" class="form-control{{ $errors->has('ville') ? ' is-invalid' : '' }}" name="ville" value="{{ old('ville') }}" required autofocus>

	                        @if ($errors->has('ville'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('ville') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>


	                <div class="form-group row">
	                    <label for="siret" class="col-md-2 col-form-label text-md-right">{{ __('N° SIRET') }}</label>

	                    <div class="col-md-6">
	                        <input id="siret" type="text" class="form-control{{ $errors->has('siret') ? ' is-invalid' : '' }}" name="siret" value="{{ old('siret') }}" autofocus>

	                        @if ($errors->has('siret'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('siret') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="email" class="col-md-2 col-form-label text-md-right">{{ __('Adresse Email') }}</label>

	                    <div class="col-md-6">
	                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">

	                        @if ($errors->has('email'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('email') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="telephone" class="col-md-2 col-form-label text-md-right">{{ __('Téléphone') }}</label>

	                    <div class="col-md-6">
	                        <input id="telephone" type="text" class="form-control{{ $errors->has('telephone') ? ' is-invalid' : '' }}" name="telephone" value="{{ old('telephone') }}">

	                        @if ($errors->has('telephone'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('telephone') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

					 <div class="form-group row">
	                    <label for="taux" class="col-md-2 col-form-label text-md-right">{{ __('Taux de remise') }}</label>

	                    <div class="col-md-6">
	                        <input id="taux" type="text" class="form-control{{ $errors->has('taux') ? ' is-invalid' : '' }}" name="taux" value="{{ old('taux') }}" required autofocus>

	                        @if ($errors->has('taux'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('taux') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row form-button-group">
	                    <div class="col-md-8 offset-md-3">
	                        <button type="submit" class="btn btn-primary create-button">
	                            {{ __('Créer la fiche client') }}
	                        </button>
                            <a href="{{ URL::previous() }}">
				        		<button type="button" class="btn btn-danger return-button">
				        			Retour
				        		</button>
				        	</a>
	                    </div>
	                </div>
	            </form>
	        </div>
        </div>
    </div>
</div>

@endsection
