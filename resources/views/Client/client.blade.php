@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-9">
        	<div class="section-title">
        		Clients
        		<a href="{{ route('client-create') }}">
	        		<button type="button" class="btn btn-primary create-button">
	        			<span class="glyphicon glyphicon-plus"></span>
	        				Créer un nouveau client

	        		</button>
	        	</a>
        	</div>
        </div>
    </div>
    @if (session('status'))
	    <div class="alert alert-success alert-dismissible">
	    	<button type="button" class="close" data-dismiss="alert">&times;</button>
	        {{ session('status') }}
	    </div>
	@elseif (session('error'))
		<div class="alert alert-danger alert-dismissible">
	    	<button type="button" class="close" data-dismiss="alert">&times;</button>
	        {{ session('error') }}
	    </div>
	@endif

	<table class="table tableau-client">
	  <thead>
	    <tr>
	      <th scope="col">Société</th>
	      <th scope="col">Nom</th>
	      <th scope="col">Prénom</th>
	      <th scope="col">CP</th>
	      <th scope="col">Ville</th>
	      <th scope="col">Téléphone</th>
	      <th scope="col">Email</th>
	      <th scope="col">SIRET</th>
	      <th scope="col">Taux de remise</th>
	      <th scope="col">Actions</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($clients as $client )
	    <tr>
	      <td>{{$client->societe}}</td>
	      <td>{{$client->nom}}</td>
	      <td>{{$client->prenom}}</td>
	      <td>{{$client->CP}}</td>
	      <td>{{$client->ville}}</td>
	      <td>{{$client->telephone}}</td>
	      <td>{{$client->email}}</td>
	      <td>{{$client->siren}}</td>
	      <td>{{$client->taux_de_remise}}</td>
	      <td>
	      	<div class="" role="group" aria-label="Actions Group" style="color:white">
	      		<a href="{{ route('voiture',$client->id) }}">
		      		<button type="button" class="btn btn-lg m-1">
					  	<span title="Consulter les voitures, devis & factures" class="glyphicon glyphicon-user" style="color:green"></span>
					</button>
				</a>
	      		<a href="{{ route('client-get',$client->id) }}">
				  <button type="button" class="btn btn-lg m-1">
				  	<span class="glyphicon glyphicon-pencil" style="color:blue"></span>
				  </button>
				</a>
				  <button type="button" class="btn btn-lg m-1" data-toggle="modal" data-target="#confirmModal{{$client->id}}">
				  	<span class="glyphicon glyphicon-remove" style="color:red"></span>
				  </button>
				<!-- Modal -->
				<div class="modal fade" id="confirmModal{{$client->id}}" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="confirmModalLabel">Supprimer un client</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        Êtes-vous sur de vouloir supprimer cette fiche client ?
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
				        <a href="{{ route('client-delete',$client->id) }}">
							<button type="button" class="btn btn-danger">Supprimer</button>
						</a>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		  </td>
	    </tr>
	    @endforeach
	  </tbody>
	</table>
</div>

@endsection
