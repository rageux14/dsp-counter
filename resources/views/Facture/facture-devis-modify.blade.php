@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-12">
        	<div class="col-md-6 col-form-label text-md-right section-subtitle">
        		Modifier une facture
        	</div>
        	@if (session('status'))
			    <div class="alert alert-success alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('status') }}
			    </div>
			@elseif (session('error'))
				<div class="alert alert-danger alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('error') }}
			    </div>
			@endif
        	<div class="content-form">
	        	 <form method="POST" action="{{ route('facture-get',$facture['id']) }}" aria-label="{{ __('Modifier Facture') }}">
	                @csrf
	                <input type="hidden" name="facture_type" value="devis" />
	                @if (!empty($clientId))
	                	<input type="hidden" name="client_id" value="{{$clientId}}" />
	                @endif
	                <div class="form-group row">
	                    <label for="devis" class="col-md-2 col-form-label text-md-right">{{ __('Clients') }}</label>
	                    <div class="col-md-6">
	                    	<div class="devis-select">
		                        <select class="form-control" id="select_client" name="select_client" disabled>
		                        	<option value="">Sélectionner un client</option>
		                        	@foreach($clients as $client)
										<option value="{{$client->id}}" @if ($client->id == $facture['client_id']) selected="selected" @endif>{{ $client->societe}}</option>
									@endforeach
							    </select>
							    @if ($errors->has('select_client'))
		                            <span class="invalid-feedback" role="alert">
		                                <strong>{{ $errors->first('select_client') }}</strong>
		                            </span>
		                        @endif
							 </div>
						</div>
	                </div>

	                <div class="form-group row">
					    <label class="col-md-2 col-form-label text-md-right" for="select-devis-facture">Devis</label>
					    <div class="col-md-6">
					    	<div class="client-select">
							    <select multiple="multiple" class="form-control" id="select-devis-facture" name="select-devis-facture[]">
							    	@foreach ($devis as $devi)
							    		<option value="{{$devi['id']}}" @foreach($devisSelect as $devisSe) @if ($devi['id'] == $devisSe->id) selected="selected" @endif @endforeach>{{ $devi['text']}}</option>
							    	@endforeach
							    </select>
							</div>
						</div>
				  	</div>

	                <div class="form-group row">
	                    <label for="numero" class="col-md-2 col-form-label text-md-right">{{ __('Numéro') }}  </label>

	                    <div class="col-md-6">
	                        <input id="numero" type="text" class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }}" name="numero" value="{{ $facture['numero'] }}" required autofocus>

	                        @if ($errors->has('numero'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('numero') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="total_ht" class="col-md-2 col-form-label text-md-right">{{ __('Total HT') }}</label>

	                    <div class="col-md-6">
	                        <input id="total_ht" type="text" class="form-control{{ $errors->has('total_ht') ? ' is-invalid' : '' }}" name="total_ht" value="{{ $facture['total_ht']  }}" required autofocus>

	                        @if ($errors->has('total_ht'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('total_ht') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
					    <div class="col-md-2 col-form-label text-md-right">Appliquer le taux de remise ? </div>
					    <div class="col-sm-10">
					      <div class="form-check">
					        <input value="{{$facture['client_id']}}" class="form-check-input" type="checkbox" id="check_taux_remise" name="check_taux_remise" @if (isset($facture['new_total_ht'])) checked="checked" @endif>
					        <label class="form-check-label" for="check_taux_remise" id="label_taux_remise">
					        	Taux de remise {{$facture['taux_de_remise']}} %
					        </label>
					      </div>
					    </div>
					  </div>

					  <div class="new_total_ht" id="new_total_ht"> 
					  	@if (isset($facture['new_total_ht']))
					  		<div class="form-group row">
		                    <label for="new_total_ht" class="col-md-2 col-form-label text-md-right">{{ __('Nouveau total HT') }}</label>

		                    <div class="col-md-6">
		                        <input id="new_total_ht" type="text" class="form-control{{ $errors->has('new_total_ht') ? ' is-invalid' : '' }}" name="new_total_ht" value="{{ $facture['new_total_ht']  }}">
		                        @if ($errors->has('new_total_ht'))
		                            <span class="invalid-feedback" role="alert">
		                                <strong>{{ $errors->first('new_total_ht') }}</strong>
		                            </span>
		                        @endif
		                    </div>
		                </div>
		                @endif
					  </div>


	                <div class="form-group row">
	                    <label for="total_tva" class="col-md-2 col-form-label text-md-right">{{ __('Total TVA') }}</label>

	                    <div class="col-md-6">
	                        <input id="total_tva" type="text" class="form-control{{ $errors->has('total_tva') ? ' is-invalid' : '' }}" name="total_tva" value="{{ $facture['total_tva']  }}">

	                        @if ($errors->has('total_tva'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('total_tva') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="total_ttc" class="col-md-2 col-form-label text-md-right">{{ __('Total TTC') }}</label>

	                    <div class="col-md-6">
	                        <input id="total_ttc" type="text" class="form-control{{ $errors->has('total_ttc') ? ' is-invalid' : '' }}" name="total_ttc" value="{{ $facture['total_ttc'] }}">

	                        @if ($errors->has('total_ttc'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('total_ttc') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="commentaires" class="col-md-2 col-form-label text-md-right">{{ __('Commentaires') }}</label>

	                    <div class="col-md-6">
	                        <textarea id="commentaires" rows="3" class="form-control{{ $errors->has('commentaires') ? ' is-invalid' : '' }}" name="commentaires">{{ $facture['commentaires'] }}</textarea>

	                        @if ($errors->has('commentaires'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('commentaires') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

					 <div class="form-group row">
	                    <label for="date_reparation" class="col-md-2 col-form-label text-md-right">{{ __('Date de réparation') }}</label>

	                    <div class="col-md-6">
	                        <input id="date_reparation" type="date" class="form-control{{ $errors->has('date_reparation') ? ' is-invalid' : '' }}" name="date_reparation" value="{{ $facture['date_reparation']  }}">

	                        @if ($errors->has('date_reparation'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('date_reparation') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>
	                <div class="form-group row">
	                    <label for="statut" class="col-md-2 col-form-label text-md-right">{{ __('Statut') }}</label>
	                <div class="col-md-6">
	                    	<div class="statuts-select">
		                        <select class="form-control" id="statut" name="statut">
		                        	@foreach($factureStatuts as $factureStatut)
		                        		@if ($factureStatut->id == $facture['statut_id'])
											<option selected value="{{$factureStatut->id}}">{{ $factureStatut->libelle}}</option>
										@else
											<option value="{{$factureStatut->id}}">{{ $factureStatut->libelle}}</option>
										@endif
									@endforeach
							    </select>
							 </div>
	                        @if ($errors->has('statut'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('statut') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>
	                <div class="form-group row form-button-group">
	                    <div class="col-md-8 offset-md-3">
	                        <button type="submit" class="btn btn-primary create-button">
	                            {{ __('Modifier la facture') }}
	                        </button>
	                        <button type="button" class="btn btn-danger delete-button" data-toggle="modal" data-target="#confirmModal">Supprimer</button>
	                        @if (!empty($clientId))
	                        	<a href="{{ route('voiture', [$clientId]) }}">
	                        @else
	                        	<a href="{{ route('facture') }}">
	                        @endif
				        		<button type="button" class="btn btn-secondary return-button">
				        			Retour
				        		</button>
				        	</a>
							<!-- Modal -->
							<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="confirmModalLabel">Supprimer une facture</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        Êtes-vous sur de vouloir supprimer cette facture ? 
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
							        <a href="{{ route('facture-delete',[$facture['id'], $facture['type']]) }}">
										<button type="button" class="btn btn-danger">Supprimer</button>
									</a> 
							      </div>
							    </div>
							  </div>
							</div>
	                    </div>
	                </div>
	            </form>
	        </div>
        </div>
    </div>
</div>

@endsection