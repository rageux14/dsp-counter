@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-12">
        	<div class="col-md-6 col-form-label text-md-right section-subtitle">
        		Envoyez la facture par mail
        	</div>
        	@if (session('status'))
			    <div class="alert alert-success alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('status') }}
			    </div>
			@elseif (session('error'))
				<div class="alert alert-danger alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('error') }}
			    </div>
			@endif
        	<div class="content-form">
	        	 <form method="POST" action="{{ route('facture-send-mail',[$data['id'], $data['type']]) }}" aria-label="{{ __('Send Facture by Email') }}">
	                @csrf
	                <input type="hidden" id="factureId" name="factureId" value="{{ $data['id'] }}"/>
	                @if (!empty($clientId))
	                	<input type="hidden" name="client_id" value="{{$clientId}}" />
	                @endif
	                <div class="form-group row">
	                    <label for="email" class="col-md-2 col-form-label text-md-right">{{ __('Email du client') }}  </label>

	                    <div class="col-md-6">
	                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $data['client_email'] }}" required autofocus>

	                        @if ($errors->has('email'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('email') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="object" class="col-md-2 col-form-label text-md-right">{{ __('Objet du mail') }}</label>

	                    <div class="col-md-6">
	                        <input id="object" type="text" class="form-control{{ $errors->has('object') ? ' is-invalid' : '' }}" name="object" value="{{ 'Facture N°' . $data['numero'] }}">

	                        @if ($errors->has('object'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('object') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="content" class="col-md-2 col-form-label text-md-right">{{ __('Sujet du mail') }}</label>

	                    <div class="col-md-6">
	                        <textarea id="content" rows="8" class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}" name="content">{{'Bonjour ' . $data['client_prenom'] . ' ' . $data['client_nom'] . ',' . "\n" . 'Vous trouverez un pièce-jointe, la facture n°' . $data['numero'] . '.' . "\n" . "\n" . 'Cordialement,' . "\n" .  "\n" .  $data['user_nom'] . ' ' . $data['user_prenom'] . "\n" . $data['user_societe'] . "\n" . $data['user_email'] . "\n" . $data['user_telephone'] }}</textarea>

	                        @if ($errors->has('content'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('content') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row form-button-group">
	                    <div class="col-md-8 offset-md-3">
                        	<button type="submit" class="btn btn-primary create-button">
                            	{{ __('Envoyer') }}
                        	</button>
                        	@if(!empty($clientId))
                        		<a href="{{ route('voiture',[$clientId]) }}">
                        	@else
	                        	<a href="{{ route('facture') }}">
	                        @endif
			        		<button type="button" class="btn btn-danger return-button">
			        			Retour
			        		</button>
				        	</a>
	                    </div>
	                </div>
	            </form>
	        </div>
        </div>
    </div>
</div>

@endsection