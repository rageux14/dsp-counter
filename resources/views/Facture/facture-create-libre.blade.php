@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-12">
        	<div class="col-md-6 col-form-label text-md-right section-subtitle">
        		Créer une facture libre
        	</div>
        	@if (session('status'))
			    <div class="alert alert-success alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('status') }}
			    </div>
			@elseif (session('error'))
				<div class="alert alert-danger alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('error') }}
			    </div>
			@endif
        	<div class="content-form">
	        	 <form method="POST" action="{{ route('facture-create-libre') }}" aria-label="{{ __('Create Facture Libre') }}">
	                @csrf
	                <input type="hidden" name="devis" value="facturelibre" />
	                <div class="form-group row">
	                    <label for="devis" class="col-md-2 col-form-label text-md-right">{{ __('Clients') }}</label>
	                    <div class="col-md-6">
	                    	<div class="devis-select">
		                        <select class="form-control" id="select_client_voiture" name="select_client_voiture">
		                        	<option value="">Sélectionner un client</option>
		                        	@foreach($clients as $client)
		                        		@if (!empty($clientSelect))
		                        			@if($clientSelect->id == $client->id)
		                        				<option value="{{$client->id}}" selected="selected">@if (!empty($client->societe)) {{ $client->societe}} @else {{ $client->nom}} @endif </option>
		                        			@else
												<option value="{{$client->id}}">@if (!empty($client->societe)) {{ $client->societe}} @else {{ $client->nom}} @endif </option>
											@endif
										@else
											<option value="{{$client->id}}">@if (!empty($client->societe)) {{ $client->societe}} @else {{ $client->nom}} @endif </option>
										@endif
									@endforeach
							    </select>
							    @if ($errors->has('select_client'))
		                            <span class="invalid-feedback" role="alert">
		                                <strong>{{ $errors->first('select_client') }}</strong>
		                            </span>
		                        @endif
							 </div>
						</div>
	                </div>

	                 <div class="form-group row">
					    <label class="col-md-2 col-form-label text-md-right" for="select-voiture-facture">Voitures</label>
					    <div class="col-md-6">
					    	<div class="client-select">
							    <select multiple="multiple" class="form-control" id="select-voiture-facture" name="select-voiture-facture[]">
							    	@if(!empty($devis))
							    		@foreach ($devis as $devi)
							    			@if($devi['value'] == $devisSelect->id)
							    				<option value="{{$devi['value']}}" selected="selected">{{ $devi['text']}}</option>
							    			@else
							    				<option value="{{$devi['value']}}">{{ $devi['text']}}</option>
							    			@endif
							    		@endforeach
							    	@endif
							    </select>
							</div>
						</div>
				  	</div>

	                <div class="form-group row">
	                    <label for="numero" class="col-md-2 col-form-label text-md-right">{{ __('Numéro') }}  </label>

	                    <div class="col-md-6">
	                        <input id="numero" type="text" class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }}" name="numero" value="{{ $numero }}" required autofocus>

	                        @if ($errors->has('numero'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('numero') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="commentaires" class="col-md-2 col-form-label text-md-right">{{ __('Decription') }}</label>

	                    <div class="col-md-6">
	                        <textarea id="commentaires" rows="3" class="form-control{{ $errors->has('commentaires') ? ' is-invalid' : '' }}" name="commentaires">{{ old('commentaires') }}</textarea>

	                        @if ($errors->has('commentaires'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('commentaires') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>
	                
	                <div class="form-group row">
	                    <label for="frais_libelle" class="col-md-2 col-form-label text-md-right">{{ __('Frais supplémentaires') }}</label>

	                    <div class="col-md-6">
	                        <input id="frais_libelle" type="frais_libelle" class="form-control{{ $errors->has('frais_libelle') ? ' is-invalid' : '' }}" name="frais_libelle" value="{{ old('frais_libelle') }}" >

	                        @if ($errors->has('frais_libelle'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('frais_libelle') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="frais_prix" class="col-md-2 col-form-label text-md-right">{{ __('Prix des frais') }}</label>

	                    <div class="col-md-6">
	                        <input id="frais_prix" type="frais_prix" class="form-control{{ $errors->has('frais_prix') ? ' is-invalid' : '' }}" name="frais_prix" value="{{ old('frais_prix') }}" >

	                        @if ($errors->has('frais_prix'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('frais_prix') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="total_ht" class="col-md-2 col-form-label text-md-right">{{ __('Total HT (Frais inclus)') }}</label>

	                    <div class="col-md-6">
	                        <input id="total_ht" type="text" class="form-control{{ $errors->has('total_ht') ? ' is-invalid' : '' }}" name="total_ht" value="{{ old('total_ht') }}" required autofocus>

	                        @if ($errors->has('total_ht'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('total_ht') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
					    <div class="col-md-2 col-form-label text-md-right">Appliquer le taux de remise ? </div>
					    <div class="col-sm-10">
					      <div class="form-check">
					        <input class="form-check-input" type="checkbox" id="check_taux_remise" name="check_taux_remise">
					        <label class="form-check-label" for="check_taux_remise" id="label_taux_remise">
					          
					        </label>
					      </div>
					    </div>
					  </div>

					  <div class="new_total_ht" id="new_total_ht"> </div>

	                <div class="form-group row">
	                    <label for="total_tva" class="col-md-2 col-form-label text-md-right">{{ __('Total TVA') }}</label>

	                    <div class="col-md-6">
	                        <input id="total_tva" type="text" class="form-control{{ $errors->has('total_tva') ? ' is-invalid' : '' }}" name="total_tva" value="{{ old('total_tva') }}">

	                        @if ($errors->has('total_tva'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('total_tva') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="total_ttc" class="col-md-2 col-form-label text-md-right">{{ __('Total TTC') }}</label>

	                    <div class="col-md-6">
	                        <input id="total_ttc" type="text" class="form-control{{ $errors->has('total_ttc') ? ' is-invalid' : '' }}" name="total_ttc" value="{{ old('total_ttc') }}">

	                        @if ($errors->has('total_ttc'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('total_ttc') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row form-button-group">
	                    <div class="col-md-8 offset-md-3">
	                        <button type="submit" class="btn btn-primary create-button">
	                            {{ __('Créer la facture') }}
	                        </button>
	                        <a href="{{ route('facture') }}">
				        		<button type="button" class="btn btn-danger return-button">
				        			Retour
				        		</button>
				        	</a>
	                    </div>
	                </div>
	            </form>
	        </div>
        </div>
    </div>
</div>

@endsection