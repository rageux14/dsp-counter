@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-9">
        	<div class="section-title">
        		Factures
        		<button type="button" class="btn btn-primary create-button" data-toggle="modal" data-target="#createFactureModal">
        			<span class="glyphicon glyphicon-plus"></span>
        				Créer une nouvelle facture
        		</button>
	        	<!-- Modal -->
				<div class="modal fade" id="createFactureModal" tabindex="-1" role="dialog" aria-labelledby="createFactureModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="createFactureModalLabel">Créer une facture</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        <h3>Voulez-vous créer une facture à partir d'un devis existant ? </h3>
				      </div>
				      <div class="modal-footer">
				        <a href="{{ route('facture-create-devis') }}">
							<button type="button" class="btn btn-primary">Oui</button>
						</a>
						<a href="{{ route('facture-create-libre') }}">
							<button type="button" class="btn btn-danger">Non, facture libre</button>
						</a>
				      </div>
				    </div>
				  </div>
				</div>
        	</div>
        </div>
    </div>
    @if (session('status'))
	    <div class="alert alert-success alert-dismissible">
	    	<button type="button" class="close" data-dismiss="alert">&times;</button>
	        {{ session('status') }}
	    </div>
	@elseif (session('error'))
		<div class="alert alert-danger alert-dismissible">
	    	<button type="button" class="close" data-dismiss="alert">&times;</button>
	        {{ session('error') }}
	    </div>
	@endif
	<table class="table tableau-factures">
	  <thead>
	    <tr>
	      <th scope="col">Type</th>
	      <th scope="col">Numéro</th>
	      <th scope="col">Société</th>
	      <th scope="col">Client</th>
	      <th scope="col">Total HT</th>
	      <th scope="col">Total TTC</th>
	      <th scope="col">Statut</th>
	      <th scope="col">Actions</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($factures as $facture )
	  		@if (isset($facture['type']))

				    <tr bgcolor="{{$facture['color']}}">
				      <td>{{$facture['type']}}</td>
				      <td>{{$facture['numero']}}</td>
				      <td>{{$facture['societe']}}</td>
				      <td>{{$facture['nom']}}</td>
				      <td>{{$facture['total_ht'] . '€'}}</td>
				      <td>{{$facture['total_ttc'] . '€'}}</td>
				      <td>{{$facture['statut']}}</td>
				      <td>
				      	@if (empty($facture['pay_at']))
				      	<div class="" role="group" aria-label="Actions Group" style="color:white">
				      		<a href="{{ route('facture-get',$facture['id']) }}">
							  <button type="button" class="btn btn-lg m-1">
							  	<span title="Modifier" class="glyphicon glyphicon-pencil" style="color:blue"></span>
							  </button>
							</a>
						@endif
						<a href="{{ route('facture-pdf',[$facture['id'],'dl']) }}">
				      		<button type="button" class="btn btn-lg m-1">
							  	<span title="Télécharger" class="glyphicon glyphicon-download" style="color:green"></span>
							 </button>
							</a>
							<a target="_blank" href="{{ route('facture-pdf',[$facture['id'],'view']) }}">
				      		<button type="button" class="btn btn-lg m-1">
							  	<span title="Voir la facture en PDF" class="glyphicon glyphicon-eye-open" style="color:purple"></span>
							 </button>
							</a>
							<a href="{{ route('facture-send-mail',[$facture['id'], $facture['type']]) }}">
							  <button type="button" class="btn btn-lg m-1">
							  	<span title="Envoyer par mail" class="glyphicon glyphicon-envelope" style="color:black"></span>
							  </button>
							</a>
							  <button type="button" class="btn btn-lg m-1" data-toggle="modal" data-target="#confirmModal{{$facture['id']}}">
							  	<span title="Supprimer" class="glyphicon glyphicon-remove" style="color:red"></span>
							  </button>
							  @if (!empty($facture['pay_at']) && $facture['pay_at'] != 1 )
							  	<p><b> Payé le {{$facture['pay_at']}} </b></p>
							  @endif
							<!-- Modal -->
							<div class="modal fade" id="confirmModal{{$facture['id']}}" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="confirmModalLabel">Supprimer une facture</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        Êtes-vous sur de vouloir supprimer cette facture ?
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
							        <a href="{{ route('facture-delete',[$facture['id'], $facture['type']]) }}">
										<button type="button" class="btn btn-danger">Supprimer</button>
									</a>
							      </div>
							    </div>
							  </div>
							</div>
						</div>
					  </td>
				    </tr>
		  	@endif
	    @endforeach
	  </tbody>
	</table>
</div>

@endsection
