@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tableau de bord</div>
                @if (session('status'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ session('status') }}
                    </div>
                @elseif (session('error'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ session('error') }}
                    </div>
                @endif
                <div class="card-body">
                    @if ($nbDevis <= 1 )
                        {{ $nbDevis . ' devis généré' }}
                    @else     
                        {{ $nbDevis }} devis générés 
                    @endif 
                </div>
                <div class="card-body">     
                    @if ($nbFactureLibre <= 1 )
                        {{ $nbFactureLibre . '  facture libre' }}
                    @else     
                        {{ $nbFactureLibre }} factures libres 
                    @endif 
                </div>
                <div class="card-body">     
                    @if ($nbFactureDevis <= 1 )
                        {{ $nbFactureDevis . '  facture générée à partir de devis' }}
                    @else     
                        {{ $nbFactureDevis }} factures générées à partir de devis 
                    @endif 
                </div>
                <div class="card-body">
                    @if ($nbClients <= 1 )
                        {{ $nbClients . ' client enregistré' }}
                    @else     
                        {{ $nbClients }} clients enregistrés
                    @endif 
                </div>
            </div>
            <br>
            <br>
            <br>
            @if (!$userLogo)
            <center>
                <a href="{{ route('add-logo') }}">
                  <button type="button" class="btn btn-primary create-button">
                    <span class="glyphicon glyphicon-plus" style="color:white"></span>
                        Ajouter son logo
                  </button>
                </a> 
            </center>
            @endif
        </div>
    </div>
</div>
@endsection
