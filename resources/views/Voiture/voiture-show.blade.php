@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-left">
            <div class="col-md-9">
                <div class="section-title">
                  <span title="Créer un devis" class="iconVoiture fa fa-car"></span>
                    {{ $voiture['immat'] }}
                    <a href="{{ route('voiture-index')}}">
                        <button type="button" class="btn btn-danger return-button btn-lg">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            Voitures

                        </button>
                    </a>
                </div>
                @if (session('status'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ session('status') }}
                    </div>
                @elseif (session('error'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ session('error') }}
                    </div>
                @endif
    <div class="card w-75">
        <div class="card-body">
           <p><b>Marque : </b>  {{ $modele['rappel_marque'] }}</p>
           <p><b>Modèle :</b>  {{ $modele['rappel_modele'] }} {{ $modele['version'] }}    {{ $modele['energie'] }}</p>
           <p><b>Assurance : </b> {{ $voiture['assurance'] }}</p>
           <p><b>Kilometres : </b>{{ $voiture['kilometres'] }}</p>
           <p><b>Couleur : </b>{{ $voiture['couleur'] }}</p>
           <p><b>Devis :</b>
               @isset($devis)
                   numéro {{$devis ['numero']}} / {{$typeDevis}} </p>


                <a class="ml-2" href="@if ($typeDevis == 'bosse')
                {{ route('devis-modify-bosse',$devis['id']) }} @else
                    {{ route('devis-modify-horaire',$devis['id']) }}>
                            @endif">
                       <button type="button" class="btn btn-info btn-lg mb-3">
                           <span title="Modifier" class="glyphicon glyphicon-pencil" style="color:blue"></span>
                           Modifier
                       </button>
                   </a>
               <a class="" href="{{ route('devis-pdf',[$devis['id'],'dl']) }}">
                   <button type="button" class="btn btn-warning btn-lg mb-3">
                       <span title="Télécharger" class="glyphicon glyphicon-download" style="color:green"></span>
                       Télécharger
                   </button>
               </a>
               <a target="_blank" href="{{ route('devis-pdf',[$devis['id'],'view']) }}">
                   <button type="button" class="btn btn-success btn-lg mb-3">
                       <span title="Voir le devis en PDF" class="glyphicon glyphicon-eye-open" style="color:purple"></span>
                       Voir en Pdf
                   </button>
               </a>
               <a href="{{ route('devis-send-mail',[$devis['id']]) }}">
                   <button type="button" class="btn btn-primary btn-lg mb-3">
                       <span title="Envoyer par mail" class="glyphicon glyphicon-envelope" style="color:black"></span>
                       Mail
                   </button>
               </a>

                <a href="{{ route('devis-delete-showVoiture',[$devis['id'], $typeDevis, 'voiture']) }}">
                       <button type="button" onclick="alert('etes vous sur ?')" class="btn btn-danger btn-lg mb-3">Supprimer</button>
                   </a>

               @elseif ($client != false)
                <button type="button" class="btn btn-primary btn-lg m-auto" data-toggle="modal" data-target="#devisModal{{$voiture['id']}}">
                    <span title="Créer un devis" class="glyphicon glyphicon-plus"></span>Créer un devis
                </button>
                <!-- Modal -->
                <div class="modal fade" id="devisModal{{$voiture['id']}}" tabindex="-1" role="dialog" aria-labelledby="devisModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="devisModalLabel">Créer un devis</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <h3> Voulez-vous créer un devis avec chiffrage des bosses ? </h3>
                            </div>
                            <div class="modal-footer">
                                <a href="{{ route('devis-create-first-voiture',[$client->id, $voiture['id']]) }}">
                                    <button type="button" class="btn btn-secondary">Oui</button>
                                </a>
                                <a href="{{ route('devis-create-horaire-voiture',[$client->id, $voiture['id']]) }}">
                                    <button type="button" class="btn btn-danger">Non, un devis au forfait</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
              @else
                <a href="">
                  <button type="button" class="btn btn-primary btn-lg m-auto">
                      <span class="glyphicon glyphicon-plus"></span>
                      Ajouter ou associer un client
                  </button>
                </a>
              @endisset

           <p><b>Facture : </b>
               @isset($facture)
                   numéro {{$facture['numero']}}/ {{$typeFacture}}</p>
            <p >
                <a class="ml-2" href="{{ route('facture-get',$facture['id']) }}">
                    <button type="button" class="btn btn-info btn-lg mb-3">
                        <span title="Modifier" class="glyphicon glyphicon-pencil" style="color:blue"></span>
                        Modifier
                    </button>
                </a>
                <a href="{{ route('facture-pdf',[$facture['id'],'dl']) }}">
                    <button type="button" class="btn btn-warning btn-lg mb-3">
                        <span title="Télécharger" class="glyphicon glyphicon-download" style="color:green"></span>
                        Télécharger
                    </button>
                </a>
                <a target="_blank" href="{{ route('facture-pdf',[$facture['id'],'view']) }}">
                    <button type="button" class="btn btn-success btn-lg mb-3">
                        <span title="Voir la facture en PDF" class="glyphicon glyphicon-eye-open" style="color:purple"></span>
                        Voir en Pdf
                    </button>
                </a>
                <a href="{{ route('facture-send-mail',[$facture['id'], $typeFacture]) }}">
                    <button type="button" class="btn btn-primary btn-lg mb-3">
                        <span title="Envoyer par mail" class="glyphicon glyphicon-envelope" style="color:black"></span>
                        Mail
                    </button>
                </a>

                    <a href="{{ route('facture-delete-showVoiture',[$facture['id'], $typeFacture, "voiture", ]) }}">
                       <button type="button" onclick="alert('etes vous sur ?')" class="btn btn-danger btn-lg mb-3">Supprimer</button>
                   </a>

                   @else
                       @isset($devis)
                    <a href="{{ route('facture-create-devis-from-devis',$devis['id']) }}">
                        <button type="button" class="btn btn-primary btn-lg m-auto">
                          <span class="glyphicon glyphicon-plus"></span>
                           Générer une facture à partir du devis
                        </button>
                    </a>
                           @else
                        Pas de Facture d'enregistrée
                           @endisset

                   @endisset
           </p>

        </div>
    </div>




@endsection
