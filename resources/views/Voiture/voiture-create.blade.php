@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-12">

        	<div class="col-md-6 col-form-label text-md-right section-subtitle">
        		Ajouter une nouvelle voiture
        	</div>
        	<div class="content-form">
			@if (session('status'))
			    <div class="alert alert-success alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('status') }}
			    </div>
			@elseif (session('error'))
				<div class="alert alert-danger alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('error') }}
			    </div>
			@endif
	        	 <form method="POST" action="{{ route('voiture-create', $id, 'voiture') }}" aria-label="{{ __('Create Voiture') }}">
	                @csrf
                     @if(Route::currentRouteName()=="voiture-create-index")
                     <div class="form-group row">
                         <label for="choixClient" class="col-md-2 col-form-label text-md-right">{{ __('Choix du client') }}</label>
                         <div class="col-md-6 d-flex">
                             <select class="form-control" id="" name="client" required>
                                 <option value="null">Choix du client</option>
                                 @foreach($clients as $client)
                                     <option>{{$client->societe}}</option>
                                 @endforeach
                             </select>
                             <a href="{{ route('client-create-voiture', 'index') }}" class="text-white btn btn-primary mb-3 ml-2 ">Ajouter nouveau client</a>
                         </div>
                     </div>
                     @endif
	                <input type="hidden" name="client_id" value="{{$id}}" />
	                <div class="form-group row">
	                    <label for="immat" class="col-md-2 col-form-label text-md-right">{{ __('Immatriculation') }}</label>

	                    <div class="col-md-6">
	                        <input id="immat" font-size="16px" type="text" class="form-control{{ $errors->has('immat') ? ' is-invalid' : '' }}" name="immat" value="{{ old('immat') }}" required autofocus>

	                        @if ($errors->has('immat'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('immat') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>
	                <div class="form-group row">
	                    <label for="select_marque" class="col-md-2 col-form-label text-md-right">{{ __('Marque') }}</label>
	                    <div class="col-md-6">
	                    	<div class="marque-select">
		                        <select class="form-control" id="select_marque" name="select_marque">
		                        	<option value="">Sélectionner une marque</option>
		                        	@foreach($marques as $marque)
										<option value="{{$marque->rappel_marque}}" @if (old('select_marque') == $marque->rappel_marque) selected="selected" @endif>{{ $marque->rappel_marque}}</option>
									@endforeach
							    </select>
							 </div>
						</div>
	                </div>

	                <div class="form-group row voiture-modele">
	                    <label for="select_modele" class="col-md-2 col-form-label text-md-right">{{ __('Modele') }}</label>
	                    <div class="col-md-6">
	                    	<div class="modele-select">
		                        <select class="form-control" id="select_modele" name="select_modele">
							    </select>
							 </div>
						</div>
	                </div>

	                <div class="form-group row">
	                    <label for="assurance" class="col-md-2 col-form-label text-md-right">{{ __('Assurance') }}</label>

	                    <div class="col-md-6">
	                        <input id="assurance" type="text" class="form-control{{ $errors->has('assurance') ? ' is-invalid' : '' }}" name="assurance" value="{{ old('assurance') }}">

	                        @if ($errors->has('assurance'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('assurance') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="chassis" class="col-md-2 col-form-label text-md-right">{{ __('Numéro de chassis') }}</label>

	                    <div class="col-md-6">
	                        <input id="chassis" type="text" class="form-control{{ $errors->has('chassis') ? ' is-invalid' : '' }}" name="chassis" value="{{ old('chassis') }}">

	                        @if ($errors->has('chassis'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('chassis') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>


	                <div class="form-group row">
	                   {{--
	                    <label for="nom_client" class="col-md-2 col-form-label text-md-right">{{ __('Nom du client') }}</label>

	                   <div class="col-md-6">
	                        <input id="nom_client" type="text" class="form-control{{ $errors->has('nom_client') ? ' is-invalid' : '' }}" name="nom_client" value="{{ old('nom_client') }}">

	                        @if ($errors->has('nom_client'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('nom_client') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                    --}}
	                </div>

	                <div class="form-group row">
	                    <label for="kilometres" class="col-md-2 col-form-label text-md-right">{{ __('Kilomètres') }}</label>

	                    <div class="col-md-6">
	                        <input id="kilometres" type="text" class="form-control{{ $errors->has('kilometres') ? ' is-invalid' : '' }}" name="kilometres" value="{{ old('kilometres') }}">

	                        @if ($errors->has('kilometres'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('kilometres') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="couleur" class="col-md-2 col-form-label text-md-right">{{ __('Couleur') }}</label>

	                    <div class="col-md-6">
	                        <input id="couleur" type="text" class="form-control{{ $errors->has('couleur') ? ' is-invalid' : '' }}" name="couleur" value="{{ old('couleur') }}" autofocus>

	                        @if ($errors->has('couleur'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('couleur') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row form-button-group">
	                    <div class="col-md-8 offset-md-3">
	                        <button type="submit" class="btn btn-primary create-button">
	                            {{ __('Créer une voiture') }}
	                        </button>
	                        <a href="{{ URL::previous() }}">
				        		<button type="button" class="btn btn-danger return-button">
				        			Retour
				        		</button>
				        	</a>


	                    </div>
	                </div>
	            </form>
	        </div>
        </div>
    </div>
</div>

@endsection
