@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-9">
        	<div class="section-title">
        		{{$client->societe}}
        		<a href="{{ route('voiture-create',$client->id) }}">
	        		<button type="button" class="btn btn-primary create-button">
	        			<span class="glyphicon glyphicon-plus"></span>
	        			Ajouter une nouvelle voiture
	        		</button>
	        	</a>
	        	 <a href="{{ route('client')}}">
					<button type="button" class="btn btn-danger create-button">
						<span class="glyphicon glyphicon-chevron-left"></span>
					 	Clients
					</button>
				</a>
        	</div>
        </div>
    </div>
    @if (session('status'))
	    <div class="alert alert-success alert-dismissible">
	    	<button type="button" class="close" data-dismiss="alert">&times;</button>
	        {{ session('status') }}
	    </div>
	@elseif (session('error'))
		<div class="alert alert-danger alert-dismissible">
	    	<button type="button" class="close" data-dismiss="alert">&times;</button>
	        {{ session('error') }}
	    </div>
	@endif
	<ul class="nav nav-tabs" id="myTab" role="tablist">
	  <li class="nav-item">
	    <a class="nav-link nav-voiture-client active" id="voiture-tab" data-toggle="tab" href="#voiture" role="tab" aria-controls="voiture" aria-selected="true">Voitures</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link nav-voiture-client" id="devis-tab" data-toggle="tab" href="#devis" role="tab" aria-controls="devis" aria-selected="false">Devis</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link nav-voiture-client" id="facture-tab" data-toggle="tab" href="#facture" role="tab" aria-controls="facture" aria-selected="false">Factures</a>
	  </li>
	</ul>
	<div class="tab-content" id="myTabContent">
	  <div class="tab-pane fade show active" id="voiture" role="tabpanel" aria-labelledby="voiture-tab">
	  <table class="table tableau-client">
	  <thead>
	    <tr>
	      <th scope="col">Immat</th>
	      <th scope="col">Marque</th>
	      <th scope="col">Modèle</th>
	      <th scope="col">Assurance</th>
	      <th scope="col">Kilometres</th>
	      <th scope="col">Couleur</th>
	      <th scope="col">Actions</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($voitures as $voiture)
	    <tr>
	      <td>{{$voiture['immat']}}</td>
	      <td>{{$voiture['marque']}}</td>
	      <td>{{$voiture['modele']}}</td>
	      <td>{{$voiture['assurance']}}</td>
	      <td>{{$voiture['kilometres']}}</td>
	      <td>{{$voiture['couleur']}}</td>
	      <td>
	      	<div class="btn-" role="group" aria-label="Actions Group" style="color:white">
	      		@if ($voiture['devis'])
	      			@if($voiture['devis_type'] == 'bosse')
	      				<a href="{{ route('devis-modify-bosse-voiture',[$client->id, $voiture['devis']]) }}">
	      			@else
	      				<a href="{{ route('devis-modify-horaire-voiture',[$client->id, $voiture['devis']]) }}">
	      			@endif
					  <button type="button" class="btn btn-lg m-1">
					  	<span title="Modifier le devis" class="glyphicon glyphicon-edit" style="color:green"></span>
					  </button>
					</a>
				@else
				  <button type="button" class="btn btn-lg m-1" data-toggle="modal" data-target="#devisModal{{$voiture['id']}}">
				  	<span title="Créer un devis" class="glyphicon glyphicon-list-alt" style="color:green"></span>
				  </button>
				  <!-- Modal -->
				<div class="modal fade" id="devisModal{{$voiture['id']}}" tabindex="-1" role="dialog" aria-labelledby="devisModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="devisModalLabel">Créer un devis</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        <h3> Voulez-vous créer un devis avec chiffrage des bosses ? </h3>
				      </div>
				      <div class="modal-footer">
				      	<a href="{{ route('devis-create-first-voiture',[$client->id, $voiture['id']]) }}">
				        	<button type="button" class="btn btn-secondary ">Oui</button>
				        </a>
				        <a href="{{ route('devis-create-horaire-voiture',[$client->id, $voiture['id']]) }}">
							<button type="button" class="btn btn-danger">Non, un devis au forfait</button>
						</a>
				      </div>
				    </div>
				  </div>
				</div>
				@endif
	      		<a href="{{ route('voiture-get',[$client->id, $voiture['id']]) }}">
				  <button type="button" class="btn btn-lg m-1">
				  	<span class="glyphicon glyphicon-pencil" style="color:blue"></span>
				  </button>
				</a>
				  <button type="button" class="btn btn-lg m-1" data-toggle="modal" data-target="#confirmModal{{$voiture['id']}}">
				  	<span class="glyphicon glyphicon-remove" style="color:red"></span>
				  </button>
				<!-- Modal -->
				<div class="modal fade" id="confirmModal{{$voiture['id']}}" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="confirmModalLabel">Supprimer une voiture</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        Êtes-vous sur de vouloir supprimer cette voiture ?
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
				        <a href="{{ route('voiture-delete',[$client->id, $voiture['id']]) }}">
							<button type="button" class="btn btn-danger">Supprimer</button>
						</a>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		  </td>
	    </tr>
	    @endforeach
	  </tbody>
	</table>
</div>
<div class="tab-pane fade" id="devis" role="tabpanel" aria-labelledby="devis-tab">
	<table class="table tableau-factures">
	  <thead>
	    <tr>
	      <th scope="col">Type</th>
	      <th scope="col">Numéro</th>
	      <th scope="col">Total HT</th>
	      <th scope="col">Total TTC</th>
	      <th scope="col">Marque</th>
	      <th scope="col">Modèle</th>
	      <th scope="col">Immat</th>
	      <th scope="col">Actions</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($devis as $devi )
	  		@if (isset($devi['type']))
			    <tr>
			      <td>{{$devi['type']}}</td>
			      <td>{{$devi['numero']}}</td>
			      @if (!empty($devi['total_ht']))
			      	<td>{{$devi['total_ht'] . '€'}}</td>
			      @else
			      	<td>{{'0€'}}</td>
			      @endif
			      @if (!empty($devi['total_ttc']))
			      	<td>{{$devi['total_ttc'] . '€'}}</td>
			      @else
			      	<td>{{'0€'}}</td>
			      @endif
			      <td>{{$devi['marque']}}</td>
			      <td>{{$devi['modele']}}</td>
			      <td>{{$devi['immat']}}</td>
			      <td>
			      	<div class="btn-group" role="group" aria-label="Actions Group" style="color:white">
			      		@if(!empty($devi['facture']))
			      			<a href="{{ route('facture-get-voiture',[$client->id, $devi['facture']]) }}">
			      			<button type="button" class="btn btn-lg m-1">
						  		<span title="Modifier la facture" class="glyphicon glyphicon-edit" style="color:green"></span>
						 	</button>
			      		@else
				      		<a href="{{ route('facture-create-devis-from-devis-voiture',[$client->id, $devi['id']]) }}">
				      		<button type="button" class="btn btn-lg m-1-lg m-1">
							  	<span title="Générer une facture" class="glyphicon glyphicon-list-alt" style="color:green"></span>
							 </button>
						@endif
						</a>
			      		@if ($devi['type'] == 'bosse')
	      					<a href="{{ route('devis-modify-bosse-voiture',[$client->id, $devi['id']]) }}">
	      				@else
	      					<a href="{{ route('devis-modify-horaire-voiture',[$client->id, $devi['id']]) }}">
			      		@endif
						  <button type="button" class="btn btn-lg m-1">
						  	<span title="Modifier" class="glyphicon glyphicon-pencil" style="color:blue"></span>
						  </button>
						</a>
			      		<a href="{{ route('devis-pdf',[$devi['id'],'dl']) }}">
			      		<button type="button" class="btn btn-lg m-1">
						  	<span title="Télécharger" class="glyphicon glyphicon-download" style="color:green"></span>
						 </button>
						</a>
						<a target="_blank" href="{{ route('devis-pdf',[$devi['id'],'view']) }}">
			      		<button type="button" class="btn btn-lg m-1">
						  	<span title="Voir le devis en PDF" class="glyphicon glyphicon-eye-open" style="color:purple"></span>
						 </button>
						</a>
						<a href="{{ route('devis-send-mail-voiture',[$client->id, $devi['id']]) }}">
						  <button type="button" class="btn btn-lg m-1">
						  	<span title="Envoyer par mail" class="glyphicon glyphicon-envelope" style="color:black"></span>
						  </button>
						</a>
						  <button type="button" class="btn btn-lg m-1" data-toggle="modal" data-target="#confirmModal{{$devi['id']}}">
						  	<span title="Supprimer" class="glyphicon glyphicon-remove" style="color:red"></span>
						  </button>
						<!-- Modal -->
						<div class="modal fade" id="confirmModal{{$devi['id']}}" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="confirmModalLabel">Supprimer un devis</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						        Êtes-vous sur de vouloir supprimer ce devis ?
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-lg m-1 btn-secondary" data-dismiss="modal">Fermer</button>
						        <a href="{{ route('devis-delete-voiture',[$client->id, $devi['id'], $devi['type']]) }}">
									<button type="button" class="btn btn-lg m-1 btn-danger">Supprimer</button>
								</a>
						      </div>
						    </div>
						  </div>
						</div>
					</div>
				  </td>
			    </tr>
		  	@endif
	    @endforeach
	  </tbody>
	</table>
</div>
	  <div class="tab-pane fade" id="facture" role="tabpanel" aria-labelledby="facture-tab">
	  	<table class="table tableau-factures">
	  <thead>
	    <tr>
	      <th scope="col">Type</th>
	      <th scope="col">Numéro</th>
	      <th scope="col">Total HT</th>
	      <th scope="col">Total TTC</th>
	      <th scope="col">Statut</th>
	      <th scope="col">Actions</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($factures as $facture )
	  		@if (isset($facture['type']))
			    <tr>
			      <td>{{$facture['type']}}</td>
			      <td>{{$facture['numero']}}</td>
			      <td>{{$facture['total_ht'] . '€'}}</td>
			      <td>{{$facture['total_ttc'] . '€'}}</td>
			      <td>{{$facture['statut']}}</td>
			      <td>
			      	<div class="btn btn-lg m-1-group" role="group" aria-label="Actions Group" style="color:white">
			      		<a href="{{ route('facture-get-voiture',[$facture['client_id'], $facture['id']]) }}">
						  <button type="button" class="btn btn-lg m-1">
						  	<span title="Modifier" class="glyphicon glyphicon-pencil" style="color:blue"></span>
						  </button>
						</a>
			      		<a href="{{ route('facture-pdf',[$facture['id'],'dl']) }}">
			      		<button type="button" class="btn btn-lg m-1">
						  	<span title="Télécharger" class="glyphicon glyphicon-download" style="color:green"></span>
						 </button>
						</a>
						<a target="_blank" href="{{ route('facture-pdf',[$facture['id'],'view']) }}">
			      		<button type="button" class="btn btn-lg m-1">
						  	<span title="Voir la facture en PDF" class="glyphicon glyphicon-eye-open" style="color:purple"></span>
						 </button>
						</a>
						<a href="{{ route('facture-send-mail-voiture',[$facture['client_id'], $facture['id'], $facture['type']]) }}">
						  <button type="button" class="btn btn-lg m-1">
						  	<span title="Envoyer par mail" class="glyphicon glyphicon-envelope" style="color:black"></span>
						  </button>
						</a>
						  <button type="button" class="btn btn-lg m-1" data-toggle="modal" data-target="#confirmModal{{$facture['id']}}">
						  	<span title="Supprimer" class="glyphicon glyphicon-remove" style="color:red"></span>
						  </button>
						<!-- Modal -->
						<div class="modal fade" id="confirmModal{{$facture['id']}}" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="confirmModalLabel">Supprimer une facture</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						        Êtes-vous sur de vouloir supprimer cette facture ?
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-lg m-1 btn-secondary" data-dismiss="modal">Fermer</button>
						        <a href="{{ route('facture-delete-voiture',[$facture['client_id'], $facture['id'], $facture['type']]) }}">
									<button type="button" class="btn btn-lg m-1 btn-danger">Supprimer</button>
								</a>
						      </div>
						    </div>
						  </div>
						</div>
					</div>
				  </td>
			    </tr>
		  	@endif
	    @endforeach
	  </tbody>
	</table>
	</div>
</div>

@endsection
