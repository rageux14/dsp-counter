@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-12">
        	<div class="col-md-6 col-form-label text-md-right section-subtitle">
        		Modifier une voiture
        	</div>
        	<div class="content-form">
        	@if(session()->has('message'))
			    <div class="alert alert-success">
			        {{ session()->get('message') }}
			    </div>
			@endif
	        	 <form method="POST" action="{{ route('voiture-get', [$client->id, $voiture->id]) }}" aria-label="{{ __('Modify Voiture') }}">
	                @csrf

	                <input type="hidden" name="voiture_id" value="{{$voiture->id}}" />
                     <div class="form-group row">
                         <label for="choixClient" class="col-md-2 col-form-label text-md-right">{{ __('Choix du client') }}</label>
                         <div class="col-md-6 d-flex">
                             <select class="form-control" id="" name="client_id">
                                 <option value="{{$client->id}}">{{$client->societe}}</option>
                                 @foreach($clients as $client)
                                     <option value="{{$client->id}}">{{$client->societe}}</option>
                                 @endforeach
                             </select>
                             <a href="{{ route('client-modify-voiture', 'modifyVoit') }}" class="text-white btn btn-primary mb-3 ml-2 ">Ajouter un nouveau client</a>
                         </div>
                     </div>
	                <div class="form-group row">
	                    <label for="immat" class="col-md-2 col-form-label text-md-right">{{ __('Immatriculation') }}</label>

	                    <div class="col-md-6">
	                        <input id="immat" font-size="16px" type="text" class="form-control{{ $errors->has('immat') ? ' is-invalid' : '' }}" name="immat" value="{{ $voiture->immat }}" required autofocus>

	                        @if ($errors->has('immat'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('immat') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>
	                <div class="form-group row">
	                    <label for="select_marque" class="col-md-2 col-form-label text-md-right">{{ __('Marque') }}</label>
	                    <div class="col-md-6">
	                    	<div class="marque-select">
		                        <select class="form-control" id="select_marque" name="select_marque">
		                        	<option value="">Sélectionner une marque</option>
		                        	@foreach($marques as $marque)
										<option value="{{$marque->rappel_marque}}" @if ($selectMarque== $marque->rappel_marque) selected="selected" @endif>{{ $marque->rappel_marque}}</option>
									@endforeach
							    </select>
							 </div>
						</div>
	                </div>

	                <div class="form-group row voiture-modele">
	                    <label for="select_modele" class="col-md-2 col-form-label text-md-right">{{ __('Modele') }}</label>
	                    <div class="col-md-6">
	                    	<div class="modele-select">
		                        <select class="form-control" id="select_modele" name="select_modele">
		                        	<option value="">Sélectionner un modèle</option>
		                        	@foreach($modeles as $modele)
										<option value="{{$modele->id}}" @if ($voiture->voiture_modele_id == $modele->id) selected="selected" @endif>{{ $modele->rappel_modele}}</option>
									@endforeach
							    </select>
							 </div>
						</div>
	                </div>

	                <div class="form-group row">
	                    <label for="assurance" class="col-md-2 col-form-label text-md-right">{{ __('Assurance') }}</label>

	                    <div class="col-md-6">
	                        <input id="assurance" type="text" class="form-control{{ $errors->has('assurance') ? ' is-invalid' : '' }}" name="assurance" value="{{ $voiture->assurance }}">

	                        @if ($errors->has('assurance'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('assurance') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>

	                 <div class="form-group row">
	                    <label for="chassis" class="col-md-2 col-form-label text-md-right">{{ __('Numéro de chassis') }}</label>

	                    <div class="col-md-6">
	                        <input id="chassis" type="text" class="form-control{{ $errors->has('chassis') ? ' is-invalid' : '' }}" name="chassis" value="{{ $voiture->chassis }}">

	                        @if ($errors->has('chassis'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('chassis') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>



	                <div class="form-group row">
	                    <label for="kilometres" class="col-md-2 col-form-label text-md-right">{{ __('Kilomètres') }}</label>

	                    <div class="col-md-6">
	                        <input id="kilometres" type="text" class="form-control{{ $errors->has('kilometres') ? ' is-invalid' : '' }}" name="kilometres" value="{{ $voiture->kilometres }}">

	                        @if ($errors->has('kilometres'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('kilometres') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="couleur" class="col-md-2 col-form-label text-md-right">{{ __('Couleur') }}</label>

	                    <div class="col-md-6">
	                        <input id="couleur" type="text" class="form-control{{ $errors->has('couleur') ? ' is-invalid' : '' }}" name="couleur" value="{{ $voiture->couleur }}" autofocus>

	                        @if ($errors->has('couleur'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('couleur') }}</strong>
	                            </span>

	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row form-button-group">
	                    <div class="col-md-8 offset-md-3">
	                        <button type="submit" class="btn btn-primary create-button">
	                            {{ __('Modifier la voiture') }}
	                        </button>
	                        <button type="button" class="btn btn-danger delete-button" data-toggle="modal" data-target="#confirmModal">Supprimer</button>

                            @if(Route::currentRouteName()=='voiture-get-index')
                                <a href="{{route('voiture-index')}}">
                                @else
                                <a href="{{ URL::previous() }}">
                                @endif
                                <button type="button" class="btn btn-secondary return-button">
				        			Retour
				        		</button>
				        	</a>
				        	<!-- Modal -->
							<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="confirmModalLabel">Supprimer une voiture</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        Êtes-vous sur de vouloir supprimer cette voiture ?
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
							        <a href="{{ route('voiture-delete',[$client->id, $voiture->id]) }}">
							        <a href="{{ route('voiture-delete',[$client->id, $voiture->id]) }}">
										<button type="button" class="btn btn-danger">Supprimer</button>
									</a>
							      </div>
							    </div>
							  </div>
							</div>
	                    </div>
	                </div>
	            </form>
	        </div>
        </div>
    </div>
</div>

@endsection
