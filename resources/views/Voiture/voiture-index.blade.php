@extends('layouts.app')

@section('content')

<div class="container">
<div class="row justify-content-left">
<div class="col-md-9">
    <div class="section-title">
        Voitures


        <a href="{{ route('voiture-create-index',[$client->id, 'index'] ) }}">
            <button type="button" class="btn btn-primary create-button">
                <span class="glyphicon glyphicon-plus"></span>
                Ajouter une nouvelle voiture
            </button>
        </a>

    </div>
    @if (session('status'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ session('status') }}
        </div>
    @elseif (session('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ session('error') }}
        </div>
    @endif


    <table class="tableVoiture mt-5 table tableau-client w-100" style="vertical-align: middle;">
        <thead>
        <tr>
            <th scope="col">Immat</th>
            <th scope="col">Marque</th>
            <th scope="col">Modèle</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
            @foreach($voitures as $voiture)

            <tr>
                <td class="mx-0">{{$voiture['immat']}}</td>
                <td>{{$voiture['modele']['rappel_marque']}}</td>
                <td>{{$voiture['modele']['rappel_modele']}}</td>
                <td>
                    <div class="btn-rg d-flex  flex-wrap" role="group" aria-label="Actions Group" style="background-color:#f5f8fa">

                        <a class="m-1" href="{{route ('voiture-show',[$voiture['id']])}}">
                            <button type="button" class="btn btn-lg">
                                <span title="Consulter les devis & factures"
                                      class="glyphicon glyphicon-zoom-in" style="color:green"></span>
                            </button>
                        </a>
                        <button type="button" class="btn btn-lg m-1" data-toggle="modal"
                                data-target="#confirmModal{{$voiture['id']}}">
                            <span class="glyphicon glyphicon-remove" style="color:red"></span>
                        </button>
                        @if($voiture['client']==true)
                        <a class="m-1" href="{{route ('voiture',[$voiture['client_id']])}}">
                            <button type="button" class="btn btn-lg">
                                <span title="Consulter la fiche client" class="glyphicon glyphicon-user" style="color:blue"></span>
                            </button>
                        </a>
                            <a class="m-1" href="{{ route('voiture-get-index',[$voiture['client_id'], $voiture['id'], 'index']) }}">
                                <button type="button" class="btn btn-lg">
                                    <span title="Modifier la voiture" class="glyphicon glyphicon-pencil" style="color:blue"></span>
                                </button>
                            </a>
                            @else
                            <a class="m-1" href="{{ route('voiture-get-index',[0, $voiture['id'], 'index']) }}">
                                <button type="button" class="btn btn-lg">
                                    <span title="Modifier la voiture" class="glyphicon glyphicon-pencil" style="color:blue"></span>
                                </button>
                            </a>
                        @endif


                        <!-- Modal -->
                        <div class="modal fade" id="confirmModal{{$voiture['id']}}" tabindex="-1"
                             role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="confirmModalLabel">Supprimer une
                                            voiture</h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Êtes-vous sur de vouloir supprimer cette voiture ?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Fermer
                                        </button>
                                        <a href="{{ route('voiture-delete-index',[$client->id, $voiture['id'], 'index']) }}">
                                            <button type="button" class="btn btn-danger btn-lg">Supprimer</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>

</div>




@endsection
