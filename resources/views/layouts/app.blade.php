<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'DSP Counter') }}</title>

    <!-- Scripts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet" type="text/css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="apple-touch-icon" href="{{public_path('img/favicon.png')}}">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'DSP Counter') }}
            </a>

            @auth()
            {{-- @if(Route::current()->getName() == 'voiture-index' or Route::current()->getName() == 'facture' or Route::current()->getName() == 'facture'
               or Route::current()->getName() =='client') --}}
            <form autocomplete="off" action="{{route ('voiture-search')}}" class=" ml-5">
                <div id="custom-search-input">
                    <div class=" input-group">
                        <input autocomplete="off" id="searchIt" placeholder="Recherche voiture par immat" name="search"
                               class="form-control " type="text">
                        <span class="input-group-append ">
                            <button class="btn btn-secondary" id="searchBtn" type="submit">
                                 <span class="glyphicon glyphicon-zoom-in "></span>
                            </button>
                        </span>

                    </div>
                </div>

            </form>
            @endauth


            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Connexion') }}</a>
                        </li>
                    <!--  <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Inscription') }}</a>
                            </li> -->
                    @else

                        <li class="nav-item ">
                            <!-- menu mobile -->
                            <div class="nav-link dropdown-toggle    " id="NavBarbtn2" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('devis') }}">
                                    {{ __('Devis') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('facture') }}">
                                    {{ __('Factures') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('client') }}">
                                    {{ __('Clients') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('voiture-index') }}">
                                    {{ __('Voitures') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('profil') }}">
                                    {{ __('Profil') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Déconnexion') }}
                                </a>

                                {{-- <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>--}}
                            </div>

                            <!-- menu desktop -->
                            <div class="dropdown" id="NavBarbtn">
                                <a id="navbarDropdown" class="flecheMenu nav-link dropdown-toggle" href="#"
                                   role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->nom . ' ' . Auth::user()->prenom }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('devis') }}">
                                        {{ __('Devis') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('facture') }}">
                                        {{ __('Factures') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('client') }}">
                                        {{ __('Clients') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('voiture-index') }}">
                                        {{ __('Voitures') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('profil') }}">
                                        {{ __('Profil') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Déconnexion') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>

                        </li>
                    @endguest
                </ul>


            </div>

        </div>


    </nav>
    <div class="text-center menuTelephone ">
        <div class="btn-group">
            <a class="btn btn-lg btn-outline-info" href="{{ route('devis') }}">
                {{ __('Devis') }}
            </a>
            <a class="btn btn-lg btn-outline-info" href="{{ route('facture') }}">
                {{ __('Factures') }}
            </a>
            <a class="btn btn-lg btn-outline-info" href="{{ route('client') }}">
                {{ __('Clients') }}
            </a>
            <a class="btn btn-lg btn-outline-info" href="{{ route('voiture-index') }}">
                {{ __('Voitures') }}
            </a>

        </div>


    </div>


    <main class="py-4">
        @yield('content')
    </main>
</div>
@auth
    <script type="text/javascript">
        var autocomplete_route = "{{ url('autocomplete', Auth::user()->id) }}";

    </script>
@endauth
<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>


</body>

</html>
