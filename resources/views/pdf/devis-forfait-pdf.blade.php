<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Devis_{{$data['numero']}}</title>
    <link href="{{ public_path('css/pdf.css') }}" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <main>
      <div class="devis-header">
        @if ($data['user_logo'])
          <div id="logo">
             <img src="{{public_path('storage/' . $data['user_logo'])}}">
          </div>
        @endif
        <div class="societe-header">
          <h1>{{$data['user_societe']}}</h1>
        </div>
      </div>
      <div id="details">
          <br>
        <div id="devis-informations">
          @if (!empty($data['nom_expert']))
            <div class="name">Nom de l'expert : {{$data['nom_expert']}}</div>
          @endif
          <div class="address">Date expertise : {{$data['date_expertise']}}</div>
          @if (!empty($data['numero_sinistre']))
            <div class="address">Numero sinistre : {{$data['numero_sinistre']}}</div>
          @endif
          <div class="address">Deviseur : {{$data['user_nom'] . ' ' . $data['user_prenom']}}</div>
          <div class="address">Tel deviseur : {{$data['user_telephone']}}</div>
        </div>
        <div id="client-devis">
          <div class="name"><b>{{$data['client_societe']}}</b></div>
          <div class="address">{{$data['client_adresse']}}</div>
          <div class="address">{{$data['client_CP'] . ' ' . $data['client_ville']}}</div>
          <div class="email"><a href="{{$data['client_email']}}">{{$data['client_email']}}</a></div>
          <div> SIRET {{$data['client_siren']}}</div>
        </div>
        <br><br>
          <div class="date-devis">
            <div class="date-title"> A {{$data['user_ville']}}, le {{$data['date_expertise']}}</div>
          </div>
          <div class="to">Devis N° {{$data['numero']}}</div>
      </div>
       
      <div class="global-tab">
        <div class="div-table-bosse">
          <table class="table-info" border="0" cellspacing="0" cellpadding="0">
          <thead>
            <tr>
              <th class="unit"><h3>Description</h3></th>
              <th class="qty"><h3>Marque</h3></th>
              <th class="unit"><h3>Modèle</h3></th>
              <th class="qty"><h3>Immat</h3></th>
              @if (!empty($data['chassis']))
                <th class="qty"><h3>N° de chassis</h3></th>
              @endif
              @if (!empty($data['nom_client']))
                <th class="qty"><h3>Nom du client</h3></th>
              @endif
            </tr>
          </thead>
          <tbody>
              <td class="unit">Réparation véhicule grêlé</td>
              <td class="qty">{{$data['marque']}}</td>
              <td class="unit">{{$data['modele']}}</td>
              <td class="qty">{{$data['immat']}}</td>
              @if (!empty($data['chassis']))
                <td class="qty">{{$data['chassis']}}</td>
              @endif
              @if (!empty($data['nom_client']))
                <td class="qty">{{$data['nom_client']}}</td>
              @endif
            </tr>
          </tbody>
        </table>
      <br>
      <br>
      <div class="image-voiture">
         <img width="480px" height="390px" src="{{public_path('img/voiture_calculation' . $data['numero'] .  '.png')}}">
      </div>
      <br><br><br><br><br><br><br><br>
      <table class="table-prix">
        <tbody>
          <tr>
            <td class="unit">Taux horaire</td>
            <td class="qty">1h = {{$data['taux_horaire'] . ' €'}}</td>
          </tr>
          <tr>
            <td class="unit">Nombre d'heures</td>
            <td class="qty">{{$data['nb_heures']}}</td>
          </tr>
           <tr>
            <td class="unit">Total DSP</td>
            <td class="qty">{{$data['dsp']}} €</td>
          </tr>
          <tr>
            <td class="unit">Frais de dossier</td>
            <td class="qty">{{$data['frais_de_dossier'] . ' €'}}</td>
          </tr>
          <tr>
            <td class="unit">Dégarnissage</td>
            <td class="qty">{{$data['degarnissage'] . ' €'}}</td>
          </tr>
          @if(!empty($data['frais_libelle']))
            <tr>
              <td class="unit">{{$data['frais_libelle']}}</td>
              <td class="qty">{{$data['frais_prix'] . ' €'}}</td>
            </tr>
          @endif
          <tr>
            <td class="unit">Total HT
            <td class="qty">{{$data['total_ht'] . ' €'}}</td>
          </tr>
          <tr>
            <td class="unit">TVA (20%) 
            <td class="qty">{{$data['total_tva'] . ' €'}}</td>
          </tr>
          <tr>
            <td class="unit">Total TTC 
            <td class="qty">{{$data['total_ttc'] . ' €'}}</td>
          </tr>
        </tbody>
      </table>
          </div>
      </div>
      <br>
    <div id="notices">
        <div class="notice">{{$data['commentaires']}}</div>
      </div>
    </main>
    <footer>
      <div>{{$data['user_societe']}}</div>
      <div>{{$data['user_adresse']}} {{$data['user_cp'] . ' ' . $data['user_ville']}}</div>
      <div><a href="{{$data['user_email']}}">{{$data['user_email']}}</a></div>
      <div> SIRET : {{$data['user_siren']}}</div>
      <div> Capital social de {{$data['user_capital_social']}} €</div>
    </footer>
  </body>
</html>