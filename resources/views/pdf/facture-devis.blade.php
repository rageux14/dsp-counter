<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Facture_{{$data['numero']}}</title>
    <link href="{{ public_path('css/pdf.css') }}" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <main>
      <div class="devis-header">
        @if ($data['user_logo'])
          <div id="logo">
             <img src="{{public_path('storage/' . $data['user_logo'])}}">
          </div>
        @endif
        <div class="societe-header">
          <h1>{{$data['user_societe']}}</h1>
        </div>
      </div>
      <div id="details" class="clearfix">
          <br>
        <div id="client">
          <div class="name"><b>{{$data['client_societe']}}</b></div>
          <div class="address">{{$data['client_adresse']}}</div>
          <div class="address">{{$data['client_CP'] . ' ' . $data['client_ville']}}</div>
          <div class="email"><a href="{{$data['client_email']}}">{{$data['client_email']}}</a></div>
          <div> SIRET {{$data['client_siren']}}</div>
        </div>
        <br><br>
          <div class="date-facture">
            <div class="date-title"> A {{$data['user_ville']}}, le {{$data['date_reparation']}}</div>
          </div>
          <br><br>
          <div class="to">Facture N° {{$data['numero']}}</div>
          <br><br>
      </div>
      <table class="table-info" border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="unit"><h3>Description</h3></th>
            <th class="qty"><h3>Marque</h3></th>
            <th class="unit"><h3>Modèle</h3></th>
            <th class="qty"><h3>Immat</h3></th>
            <th class="qty"><h3>Prix</h3></th>
          </tr>
        </thead>
        <tbody>
        @foreach($devisData as $devis)
          <tr>
            <td class="unit">Réparation véhicule 
            @if ($data['type'] == 'devis')
              selon devis {{$devis['devis_numero']}}</td>
            @endif
            <td class="qty">{{$devis['marque']}}</td>
            <td class="unit">{{$devis['modele']}}</td>
            <td class="qty">{{$devis['immat']}}</td>
            <td class="qty">{{$devis['total_ht'] . ' €'}}</td>
          </tr>
         @endforeach
        </tbody>
      </table>
      <br>
      <br>
      <br>
      <br>
      <table class="table-prix">
        <tbody>
          <tr>
            <td class="unit">TOTAL HT
            <td class="qty">{{$data['total_ht'] . ' €'}}</td>
          </tr>
          
          @if (!empty($data['new_total_ht']))
            <tr>
              <td class="unit">Remise ({{$data['client_taux_de_remise'] . '%'}})
              <td class="qty">{{$data['remise'] . ' €'}}</td>
            </tr>
            <tr>
              <td class="unit">NOUVEAU TOTAL HT
              <td class="qty">{{$data['new_total_ht'] . ' €'}}</td>
            </tr> 
          @endif
          <tr>
            <td class="unit">TVA (20%) 
            <td class="qty">{{$data['total_tva'] . ' €'}}</td>
          </tr>
          <tr>
            <td class="unit">Total TTC 
            <td class="qty">{{$data['total_ttc'] . ' €'}}</td>
          </tr>
        </tbody>
      </table>
      <div id="notices">
        <div class="notice">{{$data['commentaires']}}</div>
        
      </div>

    </main>
    <footer id='footerfact'>
      <div id='loi'>
        Merci d'envoyer votre réglement par chèque à l'ordre de la société. Conformément au décret numéro 2012-1115 du 2 Octobre 2012, et dans le cas d'une facture émise vers un professionnel, le montant de l'indemnité forfaitaire pour frais de recouvrement due au créancier en cas de retard de paiement est fixé à 40 euros.Pénalités de retard au taux annuel de 8% Le réglement des factures doit s'éffectuer au comptant et sans escompte à 30 jours date de facture sauf accord préalable avec le prestataire.
      </div>
      <div>{{$data['user_societe']}}</div>
      <div>{{$data['user_adresse']}} {{$data['user_cp'] . ' ' . $data['user_ville']}}</div>
      <div><a href="{{$data['user_email']}}">{{$data['user_email']}}</a></div>
      <div> SIRET : {{$data['user_siren']}}</div>
      <div> Capital social de {{$data['user_capital_social']}} €</div>
    </footer>
  </body>
</html>