@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-12">
        	<div class="col-md-6 col-form-label text-md-right section-subtitle">
        	@if ($logo)
        		Modifier son logo
        	@else
        		Ajouter son logo
        	@endif
        	</div>
        	<div class="col-md-6 col-form-label text-md-right file-helper">
        		Extension PNG ou JPG. Taille du fichier < 2MO.
        	</div>
        	@if ($logo)
        		<div class="user-logo" >
        			<img src="{{public_path('storage/'. $logo)}}" height="200" width="200">
        		</div>
        	@endif
        	<br><br>
	        	@if (session('status'))
				    <div class="alert alert-success alert-dismissible">
				    	<button type="button" class="close" data-dismiss="alert">&times;</button>
				        {{ session('status') }}
				    </div>
				@elseif (session('error'))
					<div class="alert alert-danger alert-dismissible">
				    	<button type="button" class="close" data-dismiss="alert">&times;</button>
				        {{ session('error') }}
				    </div>
				@elseif (session('info'))
					<div class="alert alert-info alert-dismissible">
				    	<button type="button" class="close" data-dismiss="alert">&times;</button>
				        {{ session('info') }}
				    </div>
				@endif
	        	 <form method="POST" action="{{ route('add-logo') }}" aria-label="{{ __('Add Logo') }}" enctype='multipart/form-data'>
	                @csrf
                  	<div class="form-group row">
	                    <label for="image" class="col-md-4 col-form-label text-md-right">Logo</label>
	                    <div class="col-md-6">
	                        <input type="file" id="image" name="image" class="form-control-file" enctype="multipart/form-data">
	                         @if ($errors->has('image'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('image') }}</strong>
	                            </span>
	                        @endif
	                    </div>
                    </div>
	                <div class="form-group row form-button-group ">
	                    <div class="col-md-6 text-md-right">
	                        <button type="submit" class="btn btn-primary create-button">
	                            {{ __('Enregistrer') }}
	                        </button>
	                        <a href="{{ route('home') }}">
				        		<button type="button" class="btn btn-danger return-button">
				        			Retour
				        		</button>
				        	</a>
	                    </div>
	                </div>
	            </form>
	        </div>
        </div>
    </div>
</div>

@endsection