@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-12">
        	<div class="col-md-6 col-form-label text-md-right section-subtitle">
        		Choix des bosses
        	</div>
        	<div class="js-response" >
	        	@if (session('status'))
				    <div class="alert alert-success alert-dismissible">
				    	<button type="button" class="close" data-dismiss="alert">&times;</button>
				        {{ session('status') }}
				    </div>
				@elseif (session('error'))
					<div class="alert alert-danger alert-dismissible">
				    	<button type="button" class="close" data-dismiss="alert">&times;</button>
				        {{ session('error') }}
				    </div>
				@endif
			</div>
        	<div class="content-form form-choix-bosses">
        		<form method="POST" action="{{ route('devis-create-bosse-second') }}" aria-label="{{ __('Create Devis Bosse') }}">
        			@csrf
        			<input type="hidden" name="devisBosseId" value="{{$devisBosseId}}" />
        			<input type="hidden" name="devisId" value="{{$devisId}}" />
        			@if (!empty($clientId))
        				<input type="hidden" name="client_id" value="{{$clientId}}" />
        			@endif
		        	<div class="select-car-part">
		        		<h2>Sélectionner une partie de la voiture</h2>
		        		<br>
			        	<div class="div-table-choix-bosses">
	            			<table class="table-choix-bosses" width="280" cellspacing="2" cellpadding="2">
              					<tbody>
						        		@foreach($partiesVoitures as $partieVoiture)
						        		<tr>
											  <td class="choix-bosses-check" width="50">
											  	<input class="form-check-input radio-choix-bosses" type="radio" name="inlineRadioOptions" id="inlineRadio" value="{{$partieVoiture['id']}}" onclick="return loadTab({{$partieVoiture['id']}},{{$devisId}}, {{$devisBosseId}})">
											  </td>
											  @if(!empty($partieVoiture['exist']))
											  	<td class="choix-bosses-name" id="linePartVoiture{{$partieVoiture['id']}}" style="background-color:#8aff6d">{{$partieVoiture['name']}}</td>
											  @else
											  	<td class="choix-bosses-name" id="linePartVoiture{{$partieVoiture['id']}}">{{$partieVoiture['name']}}</td>
											  @endif
										</tr>
										@endforeach
								</tbody>
							</table>
						</div>
						
						<div class="action-buttons">
			                <div class="form-group row form-button-group">
			                    <div class="col-md-8 offset-md-3">
			                        <button type="submit" class="btn btn-primary create-button">
			                            {{ __('Etape Suivante') }}
			                        </button>
			                        @if (!empty($clientId))
			                        <a href="{{ route('devis-modify-bosse-voiture',[$clientId, $devisId]) }}">
			                        @else
			                        <a href="{{ route('devis-modify-bosse',[$devisId]) }}">
			                        @endif
						        		<button type="button" class="btn btn-danger return-button">
						        			Retour
						        		</button>
						        	</a>
			                    </div>
			                </div>
			            </div>
			        </div>
			        <div class="tab-container-choix-bosse">
						</div>
	            </form>
	        </div>
        </div>
    </div>
</div>
<script type="text/javascript"> 
	function loadTab(partieVoitureId, devisId, devisBosseId){
		$(".tab-container-choix-bosse").empty();
		$(".action-buttons").empty();
		$.ajax({
	       url:'/generateTab/{partieVoitureId}/{devisId}/{devisBosseId}',
	       type:'GET',
	       data:{partieVoitureId: partieVoitureId, devisId : devisId, devisBosseId : devisBosseId},
	       success:function(data){
	           $(".tab-container-choix-bosse").append(data);
	    	}
	    });
	}
</script>

@endsection