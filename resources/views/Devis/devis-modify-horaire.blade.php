@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-12">
        	<div class="col-md-6 col-form-label text-md-right section-subtitle">
        		Modifier devis n°{{$devis['numero']}}
        	</div>
        	@if (session('status'))
			    <div class="alert alert-success alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('status') }}
			    </div>
			@elseif (session('error'))
				<div class="alert alert-danger alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('error') }}
			    </div>
			@endif
        	<div class="content-form">
	        	 <form method="POST" action="{{ route('devis-modify-horaire',[$devis['id']]) }}" aria-label="{{ __('Modify Devis Horaire') }}">
	                @csrf
	                <input type="hidden" name="typedevis" value="horaire" />
	                <input type="hidden" name="devis_id" value="{{$devis['id']}}" />
	                <input type="hidden" name="devis_horaire_id" value="{{$devis['devis_horaire_id']}}" />
	                @if (!empty($clientId))
	                	<input type="hidden" name="client_id" value="{{$clientId}}" />
	                @endif
	                <div class="form-group row">
	                    <label for="select_voiture" class="col-md-2 col-form-label text-md-right">{{ __('Voiture') }}</label>
	                    <div class="col-md-6">
	                    	<div class="voiture-select">
		                        <select class="form-control" id="select_voiture" name="select_voiture" disabled="disabled">
		                        	@foreach($voitures as $voiture)
										<option value="{{$voiture['id']}}" @if ($devis['voiture_id'] == $voiture['id']) selected="selected" @endif>{{ $voiture['societe'] . " - " . $voiture['immat']}}</option>
									@endforeach
							    </select>
							 </div>
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="numero" class="col-md-2 col-form-label text-md-right">{{ __('Numéro de devis') }}  </label>

	                    <div class="col-md-6">
	                        <input id="numero" type="text" class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }}" name="numero" value="{{$devis['numero'] }}" required autofocus>

	                        @if ($errors->has('numero'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('numero') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="date_expertise" class="col-md-2 col-form-label text-md-right">{{ __("Date d'expertise") }}</label>

	                    <div class="col-md-6">
	                        <input id="date_expertise" type="date" class="form-control{{ $errors->has('date_expertise') ? ' is-invalid' : '' }}" name="date_expertise" value="{{ $devis['date_expertise']}}">

	                        @if ($errors->has('date_expertise'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('date_expertise') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="expert" class="col-md-2 col-form-label text-md-right">{{ __("Nom de l'expert") }}</label>

	                    <div class="col-md-6">
	                        <input id="expert" type="text" class="form-control{{ $errors->has('expert') ? ' is-invalid' : '' }}" name="expert" value="{{ $devis['expert'] }}">

	                        @if ($errors->has('expert'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('expert') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="sinistre" class="col-md-2 col-form-label text-md-right">{{ __("N° de sinistre") }}</label>

	                    <div class="col-md-6">
	                        <input id="sinistre" type="text" class="form-control{{ $errors->has('sinistre') ? ' is-invalid' : '' }}" name="sinistre" value="{{ $devis['sinistre'] }}">

	                        @if ($errors->has('sinistre'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('sinistre') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="horaire" class="col-md-2 col-form-label text-md-right">{{ __('Taux horaire') }}</label>
		                <div class="col-md-6">
	                    	<div class="taux_horaire">
		                        <select class="form-control" id="horaire" name="horaire">
		                        	<option value="10" @if ($devis['taux_horaire'] == '10') selected="selected" @endif>10</option>
		                        	<option value="12"@if ($devis['taux_horaire'] == '12') selected="selected" @endif>12</option>
		                        	<option value="15"@if ($devis['taux_horaire'] == '15') selected="selected" @endif>15</option>
		                        	<option value="18"@if ($devis['taux_horaire'] == '18') selected="selected" @endif>18</option>
		                        	<option value="20"@if ($devis['taux_horaire'] == '20') selected="selected" @endif>20</option>
							    </select>
							    <i><h5> *Séléctionnez un taux horaire puis le nombre d'heure estimé pour chaque partie de la voiture</h5></i>
						 	</div>
						</div>
					</div>

	                @foreach($partieVoitures as $partieVoiture)
	                <div class="form-group row">
	                    <label for="partie_voiture{{$partieVoiture['id']}}" class="col-md-2 col-form-label text-md-right">{{ $partieVoiture['name'] }}</label>

	                    <div class="col-md-6 voiture-part">
	                        <input id="partie_voiture{{$partieVoiture['id']}}" type="text" class="form-control input-partie-voiture" @if (!empty($partieVoiture['prix'])) value="{{$partieVoiture['prix']}}" @endif name="partie_voiture{{$partieVoiture['id']}}"/>
						    <label class="form-check-label" for="check_dap" id="label_dap">
						    	<input class="form-check-input" type="checkbox" id="check_dap" value="1" @if (!empty($partieVoiture['dap'])) checked="checked" @endif name="check_dap{{$partieVoiture['id']}}"/>
						    	DAP
						    </label>
						    <label class="form-check-label" for="check_reserves" id="label_reserves">
						    	<input class="form-check-input" type="checkbox" id="check_reserves" value="1" @if (!empty($partieVoiture['reserves'])) checked="checked" @endif  name="check_reserves{{$partieVoiture['id']}}"/>
						    	Réserves
						    </label>
						    <label class="form-check-label" for="check_alu" id="label_alu">
						    	<input class="form-check-input" type="checkbox" id="check_alu" value="1" @if (!empty($partieVoiture['alu'])) checked="checked" @endif  name="check_alu{{$partieVoiture['id']}}"/>
						    	Alu
						    </label>
						    <label class="form-check-label" for="check_tradi" id="label_tradi">
						    	<input class="form-check-input" type="checkbox" id="check_tradi" value="1" @if (!empty($partieVoiture['tradi'])) checked="checked" @endif  name="check_tradi{{$partieVoiture['id']}}"/>
						    	Tradi
						    </label>
	                    </div>
	                </div>
	                @endforeach

	                <div class="form-group row">
	                    <label for="total_dsp" class="col-md-2 col-form-label text-md-right">{{ __("Total DSP") }}</label>

	                    <div class="col-md-6">
	                        <input id="total_dsp" type="text" class="form-control{{ $errors->has('total_dsp') ? ' is-invalid' : '' }}" name="total_dsp" value="{{ $devis['total_dsp'] }}">
	                        <i><h5> *Cliquer sur le champ "Total DSP" pour obtenir la somme total du chiffage</h5></i>
	                        @if ($errors->has('total_dsp'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('total_dsp') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="select_degarnissage" class="col-md-2 col-form-label text-md-right">{{ __('Dégarnissage') }}</label>
	                    <div class="col-md-6">
	                    	<div class="degarnissage-select">
		                        <select class="form-control" id="select_degarnissage" name="select_degarnissage">
		                        	<option value="">Sélectionner un tarif</option>
									<option value="0" @if ($devis['degarnissage'] == '0') selected="selected" @endif>0€ - Pas de dégarnissage</option>
									<option value="120" @if ($devis['degarnissage'] == '120') selected="selected" @endif>120€ - Dégarnissage CAT A</option>
									<option value="140" @if ($devis['degarnissage'] == '140') selected="selected" @endif>140€ - Dégarnissage CAT B</option>
									<option value="180" @if ($devis['degarnissage'] == '180') selected="selected" @endif>180€ -Dégarnissage CAT C</option>
							    </select>
							 </div>
						</div>
	                </div>

	                <div class="form-group row">
	                    <label for="select_frais_de_dossier" class="col-md-2 col-form-label text-md-right">{{ __('Frais de dossier') }}</label>
	                    <div class="col-md-6">
	                    	<div class="frais_de_dossier-select">
		                        <select class="form-control" id="select_frais_de_dossier" name="select_frais_de_dossier">
									<option value="150" @if ($devis['frais_de_dossier'] == '150') selected="selected" @endif>Oui - 150€</option>
									<option value="0" @if ($devis['frais_de_dossier'] != '150') selected="selected" @endif>Non - 0€</option>
							    </select>
							 </div>
						</div>
	                </div>

	                <div class="form-group row">
	                    <label for="frais_libelle" class="col-md-2 col-form-label text-md-right">{{ __('Frais supplémentaire(s)') }}</label>

	                    <div class="col-md-6">
	                        <input id="frais_libelle" type="text" class="form-control{{ $errors->has('frais_libelle') ? ' is-invalid' : '' }}" name="frais_libelle" value="{{ $devis['frais_libelle'] }}" >

	                        @if ($errors->has('frais_libelle'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('frais_libelle') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="frais_prix" class="col-md-2 col-form-label text-md-right">{{ __('Prix des frais supplémentaire(s)') }}</label>

	                    <div class="col-md-6">
	                        <input id="frais_prix" type="text" class="form-control{{ $errors->has('frais_prix') ? ' is-invalid' : '' }}" name="frais_prix" value="{{ $devis['frais_prix'] }}" >

	                        @if ($errors->has('frais_prix'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('frais_prix') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="total_ht" class="col-md-2 col-form-label text-md-right">{{ __("Total HT") }}</label>

	                    <div class="col-md-6">
	                        <input id="total_ht_horaire" type="text" class="form-control{{ $errors->has('total_ht') ? ' is-invalid' : '' }}" name="total_ht" value="{{ $devis['total_ht'] }}">

	                        @if ($errors->has('total_ht'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('total_ht') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="total_tva" class="col-md-2 col-form-label text-md-right">{{ __("Total TVA") }}</label>

	                    <div class="col-md-6">
	                        <input id="total_tva" type="text" class="form-control{{ $errors->has('total_tva') ? ' is-invalid' : '' }}" name="total_tva" value="{{ $devis['total_tva'] }}">

	                        @if ($errors->has('total_tva'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('total_tva') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="total_ttc" class="col-md-2 col-form-label text-md-right">{{ __("Total TTC") }}</label>

	                    <div class="col-md-6">
	                        <input id="total_ttc" type="text" class="form-control{{ $errors->has('total_ttc') ? ' is-invalid' : '' }}" name="total_ttc" value="{{ $devis['total_ttc'] }}">

	                        @if ($errors->has('total_ttc'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('total_ttc') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="commentaires" class="col-md-2 col-form-label text-md-right">{{ __('Commentaires') }}</label>

	                    <div class="col-md-6">
	                        <textarea id="commentaires" rows="3" class="form-control{{ $errors->has('commentaires') ? ' is-invalid' : '' }}" name="commentaires">{{ $devis['commentaires'] }}</textarea>

	                        @if ($errors->has('commentaires'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('commentaires') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="date_reparation" class="col-md-2 col-form-label text-md-right">{{ __('Date de réparation') }}</label>

	                    <div class="col-md-6">
	                        <input id="date_reparation" type="date" class="form-control{{ $errors->has('date_reparation') ? ' is-invalid' : '' }}" name="date_reparation" value="{{ $devis['date_reparation']}}">

	                        @if ($errors->has('date_reparation'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('date_reparation') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>
	                
	                <div class="form-group row form-button-group">
	                    <div class="col-md-8 offset-md-3">
	                        <button type="submit" class="btn btn-primary create-button">
	                            {{ __('Enregistrer') }}
	                        </button>
	                        <button type="button" class="btn btn-danger delete-button" data-toggle="modal" data-target="#confirmModal">Supprimer</button>
	                        @if (!empty($clientId))
	                        	<a href="{{ route('voiture',$clientId) }}">
	                        @else
	                        	<a href="{{ route('devis') }}">
	                        @endif
				        		<button type="button" class="btn btn-secondary return-button">
				        			Retour
				        		</button>
				        	</a>
							<!-- Modal -->
							<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="confirmModalLabel">Supprimer un devis</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        Êtes-vous sur de vouloir supprimer ce devis? 
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
							        <a href="{{ route('devis-delete',[$devis['id'], $devis['type']]) }}">
										<button type="button" class="btn btn-danger">Supprimer</button>
									</a> 
							      </div>
							    </div>
							  </div>
							</div>
	                    </div>
	                </div>
	            </form>
	        </div>
        </div>
    </div>
</div>

@endsection