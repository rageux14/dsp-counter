@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-9">
        	<div class="section-title">
        		Devis
        		<button type="button" class="btn btn-primary create-button" data-toggle="modal" data-target="#createDevisModal">
        			<span class="glyphicon glyphicon-plus"></span>
        				Créer un nouveau devis
        		</button>
	        	<!-- Modal -->
				<div class="modal fade" id="createDevisModal" tabindex="-1" role="dialog" aria-labelledby="createDevisModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="createFactureModalLabel">Créer un devis</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        <h3>Voulez-vous créer un devis avec chiffrage des bosses ? </h3>
				      </div>
				      <div class="modal-footer">
				        <a href="{{ route('devis-create-bosse-first') }}">
							<button type="button" class="btn btn-primary">Oui</button>
						</a>
						<a href="{{ route('devis-create-horaire') }}">
							<button type="button" class="btn btn-danger">Non, un devis forfait</button>
						</a>
				      </div>
				    </div>
				  </div>
				</div>
        	</div>
        </div>
    </div>
    @if (session('status'))
	    <div class="alert alert-success alert-dismissible">
	    	<button type="button" class="close" data-dismiss="alert">&times;</button>
	        {{ session('status') }}
	    </div>
	@elseif (session('error'))
		<div class="alert alert-danger alert-dismissible">
	    	<button type="button" class="close" data-dismiss="alert">&times;</button>
	        {{ session('error') }}
	    </div>
	@endif
	<table class="table tableau-factures">
	  <thead>
	    <tr>
	      <th scope="col">Type</th>
	      <th scope="col">Numéro</th>
	      <th scope="col">Société</th>
	      <th scope="col">Client</th>
	      <th scope="col">Total HT</th>
	      <th scope="col">Total TTC</th>
	      <th scope="col">Marque</th>
	      <th scope="col">Modèle</th>
	      <th scope="col">Immat</th>
	      <th scope="col">Actions</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($devis as $devi )
	  		@if (isset($devi['type']))
			    <tr>
			      <td>{{$devi['type']}}</td>
			      <td>{{$devi['numero']}}</td>
			      <td>{{$devi['societe']}}</td>
			      <td>{{$devi['nom']}}</td>
			      @if (!empty($devi['total_ht']))
			      	<td>{{$devi['total_ht'] . '€'}}</td>
			      @else
			      	<td>{{'0€'}}</td>
			      @endif
			      @if (!empty($devi['total_ttc']))
			      	<td>{{$devi['total_ttc'] . '€'}}</td>
			      @else
			      	<td>{{'0€'}}</td>
			      @endif
			      <td>{{$devi['marque']}}</td>
			      <td>{{$devi['modele']}}</td>
			      <td>{{$devi['immat']}}</td>
			      <td>
			      	<div class="" role="group" aria-label="Actions Group" style="color:white">
			      		@if(!empty($devi['facture']))
			      			<a href="{{ route('facture-get',$devi['facture']) }}">
			      			<button type="button" class="btn btn-lg m-1">
						  		<span title="Modifier la facture" class="glyphicon glyphicon-edit" style="color:green"></span>
						 	</button>
			      		@else
				      		<a href="{{ route('facture-create-devis-from-devis',$devi['id']) }}">
				      		<button type="button" class="btn btn-lg m-1-lg m-1">
							  	<span title="Générer une facture" class="glyphicon glyphicon-list-alt" style="color:green"></span>
							 </button>
						@endif
						</a>
						@if ($devi['type'] == 'bosse')
	      					<a href="{{ route('devis-modify-bosse',$devi['id']) }}">
	      				@else
	      					<a href="{{ route('devis-modify-horaire',$devi['id']) }}">
			      		@endif
						  <button type="button" class="btn btn-lg m-1">
						  	<span title="Modifier" class="glyphicon glyphicon-pencil" style="color:blue"></span>
						  </button>
						</a>
			      		<a href="{{ route('devis-pdf',[$devi['id'],'dl']) }}">
			      		<button type="button" class="btn btn-lg m-1">
						  	<span title="Télécharger" class="glyphicon glyphicon-download" style="color:green"></span>
						 </button>
						</a>
						<a target="_blank" href="{{ route('devis-pdf',[$devi['id'],'view']) }}">
			      		<button type="button" class="btn btn-lg m-1">
						  	<span title="Voir le devis en PDF" class="glyphicon glyphicon-eye-open" style="color:purple"></span>
						 </button>
						</a>
						<a href="{{ route('devis-send-mail',[$devi['id']]) }}">
						  <button type="button" class="btn btn-lg m-1">
						  	<span title="Envoyer par mail" class="glyphicon glyphicon-envelope" style="color:black"></span>
						  </button>
						</a>
						  <button type="button" class="btn btn-lg m-1" data-toggle="modal" data-target="#confirmModal{{$devi['id']}}">
						  	<span title="Supprimer" class="glyphicon glyphicon-remove" style="color:red"></span>
						  </button>
						<!-- Modal -->
						<div class="modal fade" id="confirmModal{{$devi['id']}}" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="confirmModalLabel">Supprimer un devis</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						        Êtes-vous sur de vouloir supprimer ce devis ?
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
						        <a href="{{ route('devis-delete',[$devi['id'], $devi['type']]) }}">
									<button type="button" class="btn btn-danger">Supprimer</button>
								</a>
						      </div>
						    </div>
						  </div>
						</div>
					</div>
				  </td>
			    </tr>
		  	@endif
	    @endforeach
	  </tbody>
	</table>
</div>

@endsection
