@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-12">
        	<div class="col-md-6 col-form-label text-md-right section-subtitle">
        		Créer un devis bosse
        	</div>
        	@if (session('status'))
			    <div class="alert alert-success alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('status') }}
			    </div>
			@elseif (session('error'))
				<div class="alert alert-danger alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('error') }}
			    </div>
			@endif
        	<div class="content-form">
	        	 <form method="POST" action="{{ route('devis-create-bosse-first') }}" aria-label="{{ __('Create Devis Bosse') }}">
	                @csrf
	                <input type="hidden" name="typedevis" value="bosse" />
	                @if (!empty($clientId))
	                	<input type="hidden" name="client_id" value="{{$clientId}}">
	                @endif
	                <div class="form-group row">
	                    <label for="select_voiture" class="col-md-2 col-form-label text-md-right">{{ __('Voiture') }}</label>
	                    <div class="col-md-6">
	                    	<div class="voiture-select">
		                        <select class="form-control" id="select_voiture" name="select_voiture">
		                        	<option value="">Sélectionner une voiture</option>
		                        	@foreach($voitures as $voiture)
										<option value="{{$voiture['id']}}" @if (old('select_voiture') == $voiture['id']) selected="selected" @endif @if (!empty($voitureSelect)) @if ($voitureSelect->id == $voiture['id']) selected="selected" @endif @endif>{{ $voiture['societe'] . " - " . $voiture['immat']}}</option>
									@endforeach
							    </select>
							 </div>
							 <div class="btn-add-client">
							    <a href="{{ route('client') }}">
								  <button type="button" class="btn btn-success">
								  	<span class="glyphicon glyphicon-plus"></span>
								  </button>
								</a>
							</div>
	                        @if ($errors->has('client'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('client') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>
	                <div class="form-group row">
	                    <label for="numero" class="col-md-2 col-form-label text-md-right">{{ __('Numéro de devis') }}  </label>

	                    <div class="col-md-6">
	                        <input id="numero" type="text" class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }}" name="numero" @if (old('numero')) value="{{ old('numero') }}" @else value="{{$numDevis}}" @endif required autofocus>

	                        @if ($errors->has('numero'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('numero') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="date_expertise" class="col-md-2 col-form-label text-md-right">{{ __("Date d'expertise") }}</label>

	                    <div class="col-md-6">
	                        <input id="date_expertise" type="date" class="form-control{{ $errors->has('date_expertise') ? ' is-invalid' : '' }}" name="date_expertise" value="{{ old('date_expertise') }}">

	                        @if ($errors->has('date_expertise'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('date_expertise') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="date_reparation" class="col-md-2 col-form-label text-md-right">{{ __('Date de réparation') }}</label>

	                    <div class="col-md-6">
	                        <input id="date_reparation" type="date" class="form-control{{ $errors->has('date_reparation') ? ' is-invalid' : '' }}" name="date_reparation" value="{{ old('date_reparation') }}">

	                        @if ($errors->has('date_reparation'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('date_reparation') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>


	                <div class="form-group row">
	                    <label for="expert" class="col-md-2 col-form-label text-md-right">{{ __("Nom de l'expert") }}</label>

	                    <div class="col-md-6">
	                        <input id="expert" type="text" class="form-control{{ $errors->has('expert') ? ' is-invalid' : '' }}" name="expert" value="{{ old('expert') }}">

	                        @if ($errors->has('expert'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('expert') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="sinistre" class="col-md-2 col-form-label text-md-right">{{ __('N° de Sinistre') }}</label>

	                    <div class="col-md-6">
	                        <input id="sinistre" type="text" class="form-control{{ $errors->has('sinistre') ? ' is-invalid' : '' }}" name="sinistre" value="{{ old('sinistre') }}">

	                        @if ($errors->has('sinistre'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('sinistre') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="select_degarnissage" class="col-md-2 col-form-label text-md-right">{{ __('Dégarnissage') }}</label>
	                    <div class="col-md-6">
	                    	<div class="degarnissage-select">
		                        <select class="form-control" id="select_degarnissage" name="select_degarnissage">
		                        	<option value="">Sélectionner un tarif</option>
									<option value="0" @if (old('select_degarnissage') == '0') selected="selected" @endif>0€ - Pas de dégarnissage</option>
									<option value="120" @if (old('select_degarnissage') == '120') selected="selected" @endif>120€ - Dégarnissage CAT A</option>
									<option value="140" @if (old('select_degarnissage') == '140') selected="selected" @endif>140€ - Dégarnissage CAT B</option>
									<option value="180" @if (old('select_degarnissage') == '180') selected="selected" @endif>180€ -Dégarnissage CAT C</option>
							    </select>
							 </div>
						</div>
	                </div>

	                <div class="form-group row">
	                    <label for="select_frais_de_dossier" class="col-md-2 col-form-label text-md-right">{{ __('Frais de dossier') }}</label>
	                    <div class="col-md-6">
	                    	<div class="frais_de_dossier-select">
		                        <select class="form-control" id="select_frais_de_dossier" name="select_frais_de_dossier">
									<option value="150" @if (old('select_frais_de_dossier') == '150') selected="selected" @endif>Oui - 150€</option>
									<option value="0" @if (old('select_frais_de_dossier') != '150') selected="selected" @endif>Non - 0€</option>
							    </select>
							 </div>
						</div>
	                </div>

	                <div class="form-group row">
	                    <label for="frais_libelle" class="col-md-2 col-form-label text-md-right">{{ __('Frais supplémentaire(s)') }}</label>

	                    <div class="col-md-6">
	                        <input id="frais_libelle" type="text" class="form-control{{ $errors->has('frais_libelle') ? ' is-invalid' : '' }}" name="frais_libelle" value="{{ old('frais_libelle') }}" >

	                        @if ($errors->has('frais_libelle'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('frais_libelle') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="frais_prix" class="col-md-2 col-form-label text-md-right">{{ __('Prix des frais supplémentaire(s)') }}</label>

	                    <div class="col-md-6">
	                        <input id="frais_prix" type="text" class="form-control{{ $errors->has('frais_prix') ? ' is-invalid' : '' }}" name="frais_prix" value="{{ old('frais_prix') }}" >

	                        @if ($errors->has('frais_prix'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('frais_prix') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="select_taux_horaire" class="col-md-2 col-form-label text-md-right">{{ __('Taux horaire (Unité de temps)') }}</label>
	                    <div class="col-md-6">
	                    	<div class="taux_horaire-select">
		                        <select class="form-control" id="select_taux_horaire" name="select_taux_horaire">
		                        	<option value="">Sélectionner un taux horaire</option>
									<option value="10" @if (old('taux_horaire') == '10') selected="selected" @endif>10</option>
									<option value="12" @if (old('taux_horaire') == '12') selected="selected" @endif>12</option>
									<option value="15" @if (old('taux_horaire') == '15') selected="selected" @endif>15</option>
									<option value="18" @if (old('taux_horaire') == '18') selected="selected" @endif>18</option>
									<option value="20" @if (old('taux_horaire') == '20') selected="selected" @endif>20</option>

							    </select>
							 </div>
						</div>
	                </div>
	                
	                <div class="form-group row form-button-group">
	                    <div class="col-md-8 offset-md-3">
	                        <button type="submit" class="btn btn-primary create-button">
	                            {{ __('Continuer') }}
	                        </button>
	                        @if (!empty($voitureSelect))
	                        <a href="{{ route('voiture', [$clientId]) }}">
				        		<button type="button" class="btn btn-danger return-button">
				        			Retour
				        		</button>
				        	</a>
				        	@else
				        	<a href="{{ route('devis') }}">
				        		<button type="button" class="btn btn-danger return-button">
				        			Retour
				        		</button>
				        	</a>
				        	@endif
	                    </div>
	                </div>
	            </form>
	        </div>
        </div>
    </div>
</div>

@endsection