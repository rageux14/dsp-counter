@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-12">
        	<div class="col-12 col-form-label section-subtitle">
        		Récapitulatif devis n°{{$devis['numero']}}
        	</div>
        	@if (session('status'))
			    <div class="alert alert-success alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('status') }}
			    </div>
			@elseif (session('error'))
				<div class="alert alert-danger alert-dismissible">
			    	<button type="button" class="close" data-dismiss="alert">&times;</button>
			        {{ session('error') }}
			    </div>
			@endif
        	<div class="content-form">
	        	<table class="table tableau-devis">
				  <thead>
				    <tr>
				      <th scope="col">Partie</th>
				      <th scope="col">Bosse 1</th>
				      <th scope="col">Bosse 2</th>
				      <th scope="col">Bosse 3</th>
				      <th scope="col">DAP</th>
				      <th scope="col">Alu</th>
				      <th scope="col">Reserves</th>
				      <th scope="col">HS</th>
				      <th scope="col">Total</th>
				    </tr>
				  </thead>
				  <tbody>
				  	@foreach($bosses as $bosse)
					    <tr>
					      <td>{{$bosse['name']}}</td>
					      <td>{{$bosse['bosse1']}}</td>
					      <td>{{$bosse['bosse2']}}</td>
					      <td>{{$bosse['bosse3']}}</td>
					      <td>{{$bosse['dap']}}</td>
					      <td>{{$bosse['alu']}}</td>
					      <td>{{$bosse['reserves']}}</td>
					      <td>{{$bosse['hs']}}</td>
					      <td>{{$bosse['total']}}</td>
					      <td>
					    </tr>
				    @endforeach
				  </tbody>
				</table>
				<br><br>
				<div class="content-form">
	        	 <form method="POST" action="{{ route('devis-create-bosse-third') }}" aria-label="{{ __('Create Devis Bosse') }}">
	                @csrf
	                <input type="hidden" name="devis_id" value="{{$devis['devis_id']}}" />
	                <input type="hidden" name="devis_bosse_id" value="{{$devis['devis_bosse_id']}}" />
	                @if (!empty($clientId))
	                	<input type="hidden" name="client_id" value="{{$clientId}}" />
	                @endif
	                <div class="form-group row">
	                    <label for="total_dsp_bosse_third" class="col-md-2 col-form-label text-md-left">{{ __('Total DSP') }}  </label>

	                    <div class="col-md-6">
	                        <input id="total_dsp_bosse_third" type="text" class="form-control{{ $errors->has('total_dsp_bosse_third') ? ' is-invalid' : '' }}" name="total_dsp_bosse_third" value="{{$devis['total_dsp']}}"  required autofocus>

	                        @if ($errors->has('total_dsp_bosse_third'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('total_dsp_bosse_third') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="frais_de_dossier" class="col-md-2 col-form-label text-md-left">{{ __('Frais de dossier') }}  </label>

	                    <div class="col-md-6">
	                        <input id="frais_de_dossier" type="text" class="form-control{{ $errors->has('frais_de_dossier') ? ' is-invalid' : '' }}" name="frais_de_dossier" value="{{$devis['frais_de_dossier']}}" disabled required autofocus>

	                        @if ($errors->has('frais_de_dossier'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('frais_de_dossier') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="degarnissage" class="col-md-2 col-form-label text-md-left">{{ __('Dégarnissage') }}  </label>

	                    <div class="col-md-6">
	                        <input id="degarnissage" type="text" class="form-control{{ $errors->has('degarnissage') ? ' is-invalid' : '' }}" name="degarnissage" value="{{$devis['degarnissage']}}" disabled required autofocus>

	                        @if ($errors->has('degarnissage'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('degarnissage') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                @if(!empty($devis['frais_libelle']))
		                <div class="form-group row">
		                    <label for="frais_prix" class="col-md-2 col-form-label text-md-left">{{ __($devis['frais_libelle']) }}</label>

		                    <div class="col-md-6">
		                        <input id="frais_prix" type="text" class="form-control{{ $errors->has('frais_prix') ? ' is-invalid' : '' }}" name="frais_prix" value="{{ $devis['frais_prix'] }}">

		                        @if ($errors->has('frais_prix'))
		                            <span class="invalid-feedback" role="alert">
		                                <strong>{{ $errors->first('frais_prix') }}</strong>
		                            </span>
		                        @endif
		                    </div>
		                </div>
		              @endif

	                <div class="form-group row">
	                    <label for="total_ht" class="col-md-2 col-form-label text-md-left">{{ __('Total HT') }}  </label>

	                    <div class="col-md-6">
	                        <input id="total_ht" type="text" class="form-control{{ $errors->has('total_ht') ? ' is-invalid' : '' }}" name="total_ht" value="{{$devis['total_ht']}}"   required autofocus>

	                        @if ($errors->has('total_ht'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('total_ht') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="total_tva" class="col-md-2 col-form-label text-md-left">{{ __('Total TVA') }}  </label>

	                    <div class="col-md-6">
	                        <input id="total_tva" type="text" class="form-control{{ $errors->has('total_tva') ? ' is-invalid' : '' }}" name="total_tva" value="{{$devis['total_tva']}}"  required autofocus>

	                        @if ($errors->has('total_tva'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('total_tva') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="total_ttc" class="col-md-2 col-form-label text-md-left">{{ __('Total TTC') }}  </label>

	                    <div class="col-md-6">
	                        <input id="total_ttc" type="text" class="form-control{{ $errors->has('total_ttc') ? ' is-invalid' : '' }}" name="total_ttc" value="{{$devis['total_ttc']}}"  required autofocus>

	                        @if ($errors->has('total_ttc'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('total_ttc') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="commentaires" class="col-md-2 col-form-label text-md-left">{{ __('Ajouter un commentaire') }}</label>

	                    <div class="col-md-6">
	                        <textarea id="commentaires" rows="3" class="form-control{{ $errors->has('commentaires') ? ' is-invalid' : '' }}" name="commentaires">{{ old('commentaires') }}</textarea>

	                        @if ($errors->has('commentaires'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('commentaires') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                 <div class="form-group row form-button-group">
	                    <div class="col-md-8 offset-md-3">
	                        <button type="submit" class="btn btn-primary create-button">
	                            {{ __('Valider') }}
	                        </button>
	                        @if (!empty($clientId))
	                        	<a href="{{ route('devis-create-bosse-second',['devisId' => $devis['devis_id'], 'devisBosseId' => $devis['devis_bosse_id'], 'clientId' => $clientId])}}">
	                        @else
	                        	<a href="{{ route('devis-create-bosse-second',['devisId' => $devis['devis_id'], 'devisBosseId' => $devis['devis_bosse_id']])}}">
	                       	@endif
				        		<button type="button" class="btn btn-danger return-button">
				        			Retour
				        		</button>
				        	</a>
	                    </div>
	                </div>
	            </form>
	        </div>
        </div>
    </div>
</div>

@endsection