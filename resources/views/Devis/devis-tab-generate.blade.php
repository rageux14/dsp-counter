
<br><br><br>
<div class="generate-tab">
	<h2 class="choix-bosse-title">{{$name}}</h2>
	 <form method="POST" action="" aria-label="{{ __('Create Devis Bosse') }}">
	    @csrf
	    <input type="hidden" id="devisBosseId" name="devisBosseId" value="{{$devisBosseId}}">
	    <input type="hidden" id="devisId" name="devisId" value="{{$devisId}}">
	    <input type="hidden" id="partieVoitureId" name="partieVoitureId" value="{{$partieVoitureId}}">
	    <div class="form-group row">
	        <label for="bosse1" class="col-md-2 col-form-label text-md-right">{{ __('Bosses < 2,5cm') }}  </label>

	        <div class="col-md-6">
	            <input id="bosse1" type="text" class="form-control{{ $errors->has('bosse1') ? ' is-invalid' : '' }}" name="bosse1" value="{{ ($bosse) ? $bosse->bosse1 : '' }}" required autofocus>

	            @if ($errors->has('bosse1'))
	                <span class="invalid-feedback" role="alert">
	                    <strong>{{ $errors->first('bosse1') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>
	    <div class="form-group row">
	        <label for="bosse2" class="col-md-2 col-form-label text-md-right">{{ __('Bosses < 5cm') }}  </label>

	        <div class="col-md-6">
	            <input id="bosse2" type="text" class="form-control{{ $errors->has('bosse2') ? ' is-invalid' : '' }}" name="bosse2" value="{{ ($bosse) ? $bosse->bosse2 : '' }}" required autofocus>

	            @if ($errors->has('bosse2'))
	                <span class="invalid-feedback" role="alert">
	                    <strong>{{ $errors->first('bosse2') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>
	     <div class="form-group row">
	        <label for="bosse3" class="col-md-2 col-form-label text-md-right">{{ __('Bosses > 5cm') }}  </label>

	        <div class="col-md-6">
	            <input id="bosse3" type="text" class="form-control{{ $errors->has('bosse3') ? ' is-invalid' : '' }}" name="bosse3" value="{{ ($bosse) ? $bosse->bosse3 : '' }}" required autofocus>

	            @if ($errors->has('bosse3'))
	                <span class="invalid-feedback" role="alert">
	                    <strong>{{ $errors->first('bosse3') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group row">
	        <label for="alu" class="col-md-2 form-check-label text-md-right">{{ __('Alu') }}</label>

	        <div class="col-md-6">
	            @if (isset($bosse))
	        		@if ($bosse->alu)
		            	<input class="alu" id="alu" type="checkbox" class="form-check-input{{ $errors->has('alu') ? ' is-invalid' : '' }}" value="1" name="alu" checked required autofocus>
		            @else
		            	<input class="alu" id="alu" type="checkbox" class="form-check-input{{ $errors->has('alu') ? ' is-invalid' : '' }}" value="1" name="alu" required autofocus>
		            @endif
		        @else
		        	<input class="alu" id="alu" type="checkbox" class="form-check-input{{ $errors->has('alu') ? ' is-invalid' : '' }}" value="1" name="alu" required autofocus>
		        @endif

	            @if ($errors->has('alu'))
	                <span class="invalid-feedback" role="alert">
	                    <strong>{{ $errors->first('alu') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group row">
	        <label for="dsp" class="col-md-2 form-check-label text-md-right">{{ __('DAP') }}</label>

	        <div class="col-md-6">
	        	@if (isset($bosse))
	        		@if ($bosse->dap)
		            	<input class="dsp" id="dsp" type="checkbox" class="form-check-input{{ $errors->has('dsp') ? ' is-invalid' : '' }}" value="1" name="dsp" checked required autofocus>
		            @else
		            	<input class="dsp" id="dsp" type="checkbox" class="form-check-input{{ $errors->has('dsp') ? ' is-invalid' : '' }}" value="1" name="dsp" required autofocus>
		            @endif
		        @else
		        	<input class="dsp" id="dsp" type="checkbox" class="form-check-input{{ $errors->has('dsp') ? ' is-invalid' : '' }}" value="1" name="dsp" required autofocus>
		        @endif
	            @if ($errors->has('dsp'))
	                <span class="invalid-feedback" role="alert">
	                    <strong>{{ $errors->first('dsp') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group row">
	        <label for="hs" class="col-md-2 form-check-label text-md-right">{{ __('HS') }}</label>

	        <div class="col-md-6">
	            @if (isset($bosse))
	        		@if ($bosse->hs)
		            	<input class="hs" id="hs" type="checkbox" class="form-check-input{{ $errors->has('hs') ? ' is-invalid' : '' }}" value="1" name="hs" onclick="desactivateAll()" checked required autofocus>
		            @else
		            	<input class="hs" id="hs" type="checkbox" class="form-check-input{{ $errors->has('hs') ? ' is-invalid' : '' }}" value="1" name="hs" onclick="desactivateAll()" required autofocus>
		            @endif
		        @else
		        	<input class="hs" id="hs" type="checkbox" class="form-check-input{{ $errors->has('hs') ? ' is-invalid' : '' }}" value="1" name="hs" onclick="desactivateAll()" required autofocus>
		        @endif

	            @if ($errors->has('hs'))
	                <span class="invalid-feedback" role="alert">
	                    <strong>{{ $errors->first('hs') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group row">
	        <label for="reserve" class="col-md-2 form-check-label text-md-right">{{ __('Réserves') }}</label>

	        <div class="col-md-6">
	            @if (isset($bosse))
	        		@if ($bosse->reserves)
		            	<input class="reserve" id="reserve" type="checkbox" class="form-check-input{{ $errors->has('reserve') ? ' is-invalid' : '' }}" value="1" name="reserve" checked required autofocus>
		            @else
		            	<input class="reserve" id="reserve" type="checkbox" class="form-check-input{{ $errors->has('reserve') ? ' is-invalid' : '' }}" value="1" name="reserve" required autofocus>
		            @endif
		        @else
		        	<input class="reserve" id="reserve" type="checkbox" class="form-check-input{{ $errors->has('reserve') ? ' is-invalid' : '' }}" value="1" name="reserve" required autofocus>
		        @endif

	            @if ($errors->has('reserve'))
	                <span class="invalid-feedback" role="alert">
	                    <strong>{{ $errors->first('reserve') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group row">
	        <label for="tradi" class="col-md-2 form-check-label text-md-right">{{ __('Tradi') }}</label>

	        <div class="col-md-6">
	            @if (isset($bosse))
	        		@if ($bosse->tradi)
		            	<input class="tradi" id="tradi" type="checkbox" class="form-check-input{{ $errors->has('tradi') ? ' is-invalid' : '' }}" value="1" name="tradi" checked required autofocus>
		            @else
		            	<input class="tradi" id="tradi" type="checkbox" class="form-check-input{{ $errors->has('tradi') ? ' is-invalid' : '' }}" value="1" name="tradi" required autofocus>
		            @endif
		        @else
		        	<input class="tradi" id="tradi" type="checkbox" class="form-check-input{{ $errors->has('tradi') ? ' is-invalid' : '' }}" value="1" name="tradi" required autofocus>
		        @endif

	            @if ($errors->has('tradi'))
	                <span class="invalid-feedback" role="alert">
	                    <strong>{{ $errors->first('tradi') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group row form-button-group">
	        <div class="col-md-8 offset-md-3">
	            <button type="button" onclick="saveBosse()" class="btn btn-primary create-button">
	                {{ __('Enregistrer') }}
	            </button>
	        		<button type="button" onclick="closeTab()" class="btn btn-danger return-button">
	        			Annuler
	        		</button>
	        	</a>
	        </div>
	    </div>
	</form>
</div>
<script type="text/javascript"> 
	function saveBosse(){
		devisbosseid = $('#devisBosseId').val();
		devisid = $('#devisId').val();
		partieVoitureid = $('#partieVoitureId').val();
		bosse1 = $('#bosse1').val();
		bosse2 = $('#bosse2').val();
		bosse3 = $('#bosse3').val();
		if ($('.alu').is(':checked')) {
			alu = $('.alu:checked').val();
		} else {
			alu = 0;
		}
		if ($('.dsp').is(':checked')) {
			dsp = $('.dsp:checked').val();
		} else {
			dsp = 0;
		}
		if ($('.hs').is(':checked')) {
			hs = $('.hs:checked').val();
		} else {
			hs = 0;
		}
		if ($('.reserve').is(':checked')) {
			reserve = $('.reserve:checked').val();
		} else {
			reserve = 0;
		}
		if ($('.tradi').is(':checked')) {
			tradi = $('.tradi:checked').val();
		} else {
			tradi = 0;
		}
		$.ajax({
	       url:'/saveBosse',
	       type:'GET',
	       data:{devisbosseid: devisbosseid, devisid : devisid, partieVoitureid : partieVoitureid, bosse1 : bosse1, bosse2 : bosse2, bosse3 : bosse3, alu : alu, dsp : dsp, hs : hs, reserve : reserve, tradi : tradi},
	       success:function(data){
	           $(".tab-container-choix-bosse").empty();
	           $(".action-buttons").append("<div class='form-group row form-button-group'><div class='col-md-8 offset-md-3'><button type='submit' class='btn btn-primary create-button'>{{ __('Etape Suivante') }}</button><a href='{{ route('devis') }}'><button type='button' class='btn btn-danger return-button'>Retour</button></a></div></div>");
	           if (data['statut'] == 'error') {
	           	$(".js-response").append("<div class='alert alert-danger alert-dismissible'><button type='button' class='close'data-dismiss='alert'>&times;</button>" + data['message'] + "</div>");
	           } else if (data['statut'] == 'success') {
	           	$(".js-response").append("<div class='alert alert-success alert-dismissible'><button type='button' class='close'data-dismiss='alert'>&times;</button>" + data['message'] + "</div>");
	           	$('#linePartVoiture' + data['id']).css('background-color', '#8aff6d');
	           }
	    	}
	    });
	}

	function closeTab(){
		$(".generate-tab").empty();
		$(".action-buttons").append("<div class='form-group row form-button-group'><div class='col-md-8 offset-md-3'><button type='submit' class='btn btn-primary create-button'>{{ __('Etape Suivante') }}</button><a href='{{ route('devis') }}'><button type='button' class='btn btn-danger return-button'>Retour</button></a></div></div>");
	}

	function desactivateAll(){
		if ($('.hs').is(':checked')) {
			$("#bosse1").prop('disabled',true);
			$("#bosse2").prop('disabled',true);
			$("#bosse3").prop('disabled',true);
			$("#alu").prop('disabled',true);
			$("#dsp").prop('disabled',true);
			$("#reserve").prop('disabled',true);
			$("#tradi").prop('disabled',true);
		} else {
			$("#bosse1").removeAttr('disabled');
			$("#bosse2").removeAttr('disabled');
			$("#bosse3").removeAttr('disabled');
			$("#alu").removeAttr('disabled');
			$("#dsp").removeAttr('disabled');
			$("#reserve").removeAttr('disabled');
			$("#tradi").removeAttr('disabled');
		}
	}
</script>