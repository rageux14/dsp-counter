<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script"  rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- Styles -->
        <style>
            body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
                color: #1836CA;
                font-family: 'Dancing Script', cursive;
            }

            .links{
                color: #4A4C57;
                padding: 0 25px;
                font-size: 20px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                margin-bottom: 20px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .form-group{
                text-align:center;
                margin-left: 70%;
                transform: translateX(-118%);
            }
        </style>
    </head>
    <body>
        <div class="content">
            <div class="title m-b-md">
                DSP Counter
            </div>

            <div class="links">
                Connexion
            </div>
            <form>
              <div class="form-group">
                <input type="email" class="form-control" id="emailAdress" placeholder="Adresse email">
              </div>
              <div class="form-group">
                <input type="password" class="form-control" id="password" placeholder="Mot de passe">
              </div>
              <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Se rappeler de moi</label>
              </div>
              <button type="submit" class="btn btn-primary">Se connecter</button>
            </form>
        </div>
    </body>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</html>
