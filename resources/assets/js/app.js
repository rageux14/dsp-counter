
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./typeahead')

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});

//add white color for button group
$('.btn-group-primary .btn').addClass('btn-primary');

//get data of devis for facture
$('#select_client').change(function()
{
    if ($(this).val() != '') {
      $('#check_taux_remise').val($('#select_client').val());
      $.ajax({
       url:'/getTauxRemise/{client}',
       type:'GET',
       dataType:'json',
       data:{client: this.value},
       success:function(data){
        $('#label_taux_remise').text("Taux de remise " + data.data + "%");
        }
      });
      $('#select-devis-facture').empty();
      $.ajax({
         url:'/getDevisByClient/{id}',
         type:'GET',
         data:{id: this.value},
         success:function(data){
            console.log(data);
            var text = '';
            var devisId = '';
            $.each(data,function() {
              $.each(this,function(k,v) {
                $.each(v,function(k,v) {
                  console.log(k);
                  if (k == 'devis') {
                    text = v;
                  }
                  if (k == 'devis_id') {
                    devisId = v;
                  }
                });
                $('#select-devis-facture').append(new Option(text, devisId));
              });
            });
        }
      });
    }

});

$('#select-devis-facture').change(function()
{
  var count = $('#select-devis-facture :selected').length;
  if (count == 0) {
    $('#total_ht').val('');
    $('#total_tva').val('');
    $('#total_ttc').val('');
  }
  if (count == 1) {
    $.ajax({
         url:'/getDevisInformations/{id}',
         type:'GET',
         data:{id: this.value},
         success:function(data){
          $('#total_ht').val(data.data.total_ht);
          $('#total_tva').val(data.data.total_tva);
          $('#total_ttc').val(data.data.total_ttc);
         }
    });
  } else {
    var totalTtc = 0;
    var totalHt = 0;
    var totalTva = 0;
    var val = $('#select-devis-facture').val();

    var callback = function () {
     console.log(totalTtc);
       $('#total_ht').val(totalHt.toFixed(2));
       $('#total_tva').val(totalTva.toFixed(2));
       $('#total_ttc').val(totalTtc.toFixed(2));
    };
    var valLength = val.length;
    var counterAjax = 0;

    $.each(val, function(){
       $.ajax({
         url:'/getDevisInformations/{id}',
         type:'GET',
         data:{id: this},
         success:function(data){
           counterAjax++;
           totalHt += parseFloat(data.data.total_ht);
           totalTva += parseFloat(data.data.total_tva);
           totalTtc += parseFloat(data.data.total_ttc);
           if (counterAjax == valLength) {
             callback();
           }
         }
      });
     });
  }
});

//facture libre taux de remise && voiture select
$('#select_client_voiture').change(function()
{
    if ($(this).val() != '') {
      $('#check_taux_remise').val($('#select_client_voiture').val());
      $.ajax({
       url:'/getTauxRemise/{client}',
       type:'GET',
       dataType:'json',
       data:{client: this.value},
       success:function(data){
        $('#label_taux_remise').text("Taux de remise " + data.data + "%");
        }
      });
      $('#select-voiture-facture').empty();
      $.ajax({
         url:'/getVoituresByClient/{id}',
         type:'GET',
         data:{id: this.value},
         success:function(data){
            console.log(data);
            var text = '';
            var devisId = '';
            $.each(data,function() {
              $.each(this,function(k,v) {
                $.each(v,function(k,v) {
                  console.log(k);
                  if (k == 'voiture') {
                    text = v;
                  }
                  if (k == 'voiture_id') {
                    devisId = v;
                  }
                });
                $('#select-voiture-facture').append(new Option(text, devisId));
              });
            });
        }
      });
    }

});


$('#check_taux_remise').on('click tap',function(e)
{
  if ($('#check_taux_remise').is(':checked')){
     $.ajax({
       url:'/getTauxRemise/{client}',
       type:'GET',
       dataType:'json',
       data:{client: this.value},
       success:function(data){
         if($('#total_ht').val() != '' && data.data != 0) {
            console.log('HEY YOOO  ' + data.data);
            var remise = ($('#total_ht').val() * data.data) / 100;
            var new_total_ht = (parseFloat($('#total_ht').val()) - remise).toFixed(2);
            $('#new_total_ht').val(new_total_ht);
            $('#new_total_ht').append("<div class='form-group row'><label for='new_total_ht' class='col-md-2 col-form-label text-md-right'>Nouveau total HT</label><div class='col-md-6'><input id='new_total_ht' type='text' class='form-control' name='new_total_ht' value='" + new_total_ht + "'></div></div>");
            $('#total_tva').val(parseFloat($('#new_total_ht').val() * 0.20).toFixed(2));
            var total_ttc = parseFloat($('#new_total_ht').val()) + parseFloat($('#total_tva').val());
            $('#total_ttc').val(total_ttc.toFixed(2));
         }
       }
    });
  } else {
    $('#new_total_ht').empty();
    if($.isNumeric($('#total_ht').val())) {
        $('#total_tva').val(parseFloat($('#total_ht').val() * 0.20).toFixed(2));
        var total_ttc = parseFloat($('#total_ht').val()) + parseFloat($('#total_tva').val());
        $('#total_ttc').val(total_ttc.toFixed(2));
      }
  }
});



 var helpers =
{
    buildDropdown: function(result, dropdown, emptyMessage)
    {
        // Remove current options
        dropdown.html('');
        // Add the empty option with the empty message
        dropdown.append('<option value="">' + emptyMessage + '</option>');
        // Check result isnt empty
        if(result != '')
        {
            // Loop through each of the results and append the option to the dropdown
            $.each(result, function(k, v) {
                dropdown.append('<option value="' + v.id + '">' + v.rappel_modele + '</option>');
            });
        }
    }
}


//get modele by a marque
$('#select_marque').change(function()
{
    $.ajax({
       url:'/getModeleByMarque/{marque}',
       type:'GET',
       dataType:'json',
       data:{marque: this.value},
       success:function(data){
        helpers.buildDropdown(data,$('#select_modele'),'Sélectionner un modèle');
       }
    });
});

$('#select_marque').on('click tap',function(e)
{
    $.ajax({
       url:'/getModeleByMarque/{marque}',
       type:'GET',
       dataType:'json',
       data:{marque: this.value},
       success:function(data){
        helpers.buildDropdown(data,$('#select_modele'),'Sélectionner un modèle');
       }
    });
});







$('#select_taux_horaire').on('click tap',function(e){
  if($('#total_dsp').val() != '') {
      if($.isNumeric($('#total_dsp').val())) {
        $('#nb_heures_horaire').val($('#total_dsp').val() / $('#select_taux_horaire').val());
      }
       } else {
      if($('#nb_heures_horaire').val() != '') {
         $('#total_ht_horaire').val(null);
      }
    }
})
/*
$('#total_dsp').on('change',function(e){
  if($('#total_dsp').val() != '' && $('#select_taux_horaire').val() != '') {
      if($.isNumeric($('#total_dsp').val())) {
        $('#nb_heures_horaire').val($('#total_dsp').val() / $('#select_taux_horaire').val());
      }
       } else {
      if($('#nb_heures_horaire').val() != '') {
         $('#total_ht_horaire').val(null);
      }
    }
})
*/

$('#total_dsp').on('click tap',function(e){
  var i;
  var total_dsp = 0;
  var taux_horaire = $('#horaire').val();
  for (i = 1; i < 15; i++) {
    if($('#partie_voiture' + i).val() != '') {
      var str = "" + $('#partie_voiture' + i).val();
      var resultat = str.replace(",",".");
      total_dsp += parseFloat(resultat);
    }
  }
  var total = total_dsp * taux_horaire * 10;
  $('#total_dsp').val(total);
   if($.isNumeric($('#select_frais_de_dossier').val()) && $.isNumeric($('#select_degarnissage').val())) {
      if($.isNumeric($('#frais_prix').val())) {
        $('#total_ht_horaire').val(parseFloat($('#total_dsp').val()) + parseFloat($('#select_degarnissage').val()) + parseFloat($('#select_frais_de_dossier').val())  + parseFloat($('#frais_prix').val()) );
      } else {
        $('#total_ht_horaire').val(parseFloat($('#total_dsp').val()) + parseFloat($('#select_degarnissage').val()) + parseFloat($('#select_frais_de_dossier').val()) );
      }
        $('#total_tva').val(parseFloat($('#total_ht_horaire').val() * 0.20).toFixed(2));
        var total_ttc = parseFloat($('#total_ht_horaire').val()) + parseFloat($('#total_tva').val());
        $('#total_ttc').val(total_ttc);
      }
})


$('#select_frais_de_dossier').on('click',function(e){
  if($('#total_dsp').val() != '') {
      if($.isNumeric($('#total_dsp').val()) && $.isNumeric($('#select_degarnissage').val())) {
        $('#total_ht_horaire').val(parseFloat($('#total_dsp').val()) + parseFloat($('#select_degarnissage').val()) + parseFloat($('#select_frais_de_dossier').val()) );
        $('#total_tva').val(parseFloat($('#total_ht_horaire').val() * 0.20).toFixed(2));
        var total_ttc = parseFloat($('#total_ht_horaire').val()) + parseFloat($('#total_tva').val());
        $('#total_ttc').val(total_ttc);
      }
    }
  if ($('#select_frais_de_dossier').val() == '') {
    $('#total_ht_horaire').val('');
    $('#total_tva').val('');
    $('#total_ttc').val('');
  }
})

$('#select_degarnissage').on('click',function(e){
  if($('#total_dsp').val() != '') {
      if($.isNumeric($('#total_dsp').val()) && $.isNumeric($('#select_frais_de_dossier').val())) {
        $('#total_ht_horaire').val(parseFloat($('#total_dsp').val()) + parseFloat($('#select_degarnissage').val()) + parseFloat($('#select_frais_de_dossier').val()) );
        $('#total_tva').val(parseFloat($('#total_ht_horaire').val() * 0.20).toFixed(2));
        var total_ttc = parseFloat($('#total_ht_horaire').val()) + parseFloat($('#total_tva').val());
        $('#total_ttc').val(total_ttc);
      }
    }
  if ($('#select_degarnissage').val() == '') {
    $('#total_ht_horaire').val('');
    $('#total_tva').val('');
    $('#total_ttc').val('');
  }
})

$('#frais_prix').on('change',function(e){
  if($('#frais_prix').val() != '') {
      if($.isNumeric($('#total_dsp').val()) && $.isNumeric($('#select_degarnissage').val()) && $.isNumeric($('#select_frais_de_dossier').val()) && $.isNumeric($('#frais_prix').val())) {
        $('#total_ht_horaire').val(parseFloat($('#total_dsp').val()) + parseFloat($('#select_degarnissage').val()) + parseFloat($('#select_frais_de_dossier').val()) + parseFloat($('#frais_prix').val()));
        $('#total_tva').val(parseFloat($('#total_ht_horaire').val() * 0.20).toFixed(2));
        var total_ttc = parseFloat($('#total_ht_horaire').val()) + parseFloat($('#total_tva').val());
        $('#total_ttc').val(total_ttc);
      }
    } else {
      if($.isNumeric($('#total_dsp').val()) && $.isNumeric($('#select_degarnissage').val()) && $.isNumeric($('#select_frais_de_dossier').val())) {
        $('#total_ht_horaire').val(parseFloat($('#total_dsp').val()) + parseFloat($('#select_degarnissage').val()) + parseFloat($('#select_frais_de_dossier').val()));
        $('#total_tva').val(parseFloat($('#total_ht_horaire').val() * 0.20).toFixed(2));
        var total_ttc = parseFloat($('#total_ht_horaire').val()) + parseFloat($('#total_tva').val());
        $('#total_ttc').val(total_ttc);
      }
  }
})

//event on change of input, calculate ht price
$('#nb_heures_horaire').on('input',function(e){
    if($('#taux_horaire_horaire').val() != '') {
      if($.isNumeric($('#nb_heures_horaire').val()) && $.isNumeric($('#taux_horaire_horaire').val())) {
        $('#total_ht_horaire').val($('#nb_heures_horaire').val() * $('#taux_horaire_horaire').val());
        $('#total_tva').val(parseFloat($('#total_ht_horaire').val() * 0.20).toFixed(2));
        var total_ttc = parseFloat($('#total_ht_horaire').val()) + parseFloat($('#total_tva').val());
        $('#total_ttc').val(total_ttc);
      }
       } else {
      if($('#nb_heures_horaire').val() != '') {
         $('#total_ht_horaire').val(null);
      }
    }
});

$('#taux_horaire_horaire').on('input',function(e){
    if($('#nb_heures_horaire').val() != '') {
      if($.isNumeric($('#nb_heures_horaire').val()) && $.isNumeric($('#taux_horaire_horaire').val())) {
        $('#total_ht_horaire').val($('#nb_heures_horaire').val() * $('#taux_horaire_horaire').val());
        $('#total_tva').val(parseFloat($('#total_ht_horaire').val() * 0.20).toFixed(2));
        var total_ttc = parseFloat($('#total_ht_horaire').val()) + parseFloat($('#total_tva').val());
        $('#total_ttc').val(total_ttc);
      }
    } else {
      if($('#taux_horaire_horaire').val() != '') {
         $('#total_ht_horaire').val(null);
      }
    }
});

//for calcul TVA and final price
$('#total_ht_horaire').on('click',function(e){
    if($('#total_ht_horaire').val() != '') {
      if($.isNumeric($('#total_ht_horaire').val())) {
        $('#total_tva').val(parseFloat($('#total_ht_horaire').val() * 0.20).toFixed(2));
        var total_ttc = parseFloat($('#total_ht_horaire').val()) + parseFloat($('#total_tva').val());
        $('#total_ttc').val(total_ttc);
      }
    } else {
      ('#total_ttc').val(" ");
      ('#total_tva').val(" ");
    }
});

$('#remise_technicien').on('input',function(e){
    if($('#remise_technicien').val() != '') {
      if($.isNumeric($('#remise_technicien').val())) {
        var total_ttc = parseFloat($('#total_ttc').val()) - parseFloat($('#remise_technicien').val().toPrecision(3));
        $('#new_ttc').val(total_ttc);
      }
    } else {
      ('#new_ttc').val(" ");
    }
});


//for calcul TVA and final price
$('#total_ht').on('input',function(e){
    if($('#total_ht').val() != '') {
      if($.isNumeric($('#total_ht').val())) {
        $('#total_tva').val(parseFloat($('#total_ht').val() * 0.20).toFixed(2));
        var total_ttc = parseFloat($('#total_ht').val()) + parseFloat($('#total_tva').val());
        $('#total_ttc').val(total_ttc);
      }
    } else {
      ('#total_ttc').val(" ");
      ('#total_tva').val(" ");
    }
});


//for devis bosse third
$('#total_dsp_bosse_third').on('input',function(e){
   if($.isNumeric($('#frais_de_dossier').val()) && $.isNumeric($('#degarnissage').val())) {
      if($.isNumeric($('#frais_prix').val())) {
        $('#total_ht').val(parseFloat($('#total_dsp_bosse_third').val()) + parseFloat($('#degarnissage').val()) + parseFloat($('#frais_de_dossier').val())  + parseFloat($('#frais_prix').val()) );
      } else {
        $('#total_ht').val(parseFloat($('#total_dsp_bosse_third').val()) + parseFloat($('#degarnissage').val()) + parseFloat($('#frais_de_dossier').val()) );
      }
        $('#total_tva').val(parseFloat($('#total_ht').val() * 0.20).toFixed(2));
        var total_ttc = parseFloat($('#total_ht').val()) + parseFloat($('#total_tva').val());
        $('#total_ttc').val(total_ttc.toFixed(2));
      }
})



//autocomplete


$('#searchIt').typeahead({
    },
    {
        display:'immat',
        source: function (term, syncResults, asyncResults) {
        return $.get(autocomplete_route, { immat: term }, function (data) {
            return asyncResults(data);
        });
    }
}).bind('typeahead:select', function (ev, suggestion){
    document.getElementById('searchBtn').click();
});

