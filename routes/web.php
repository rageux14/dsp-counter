<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Devis
Route::get('/devis','DevisController@index')->name('devis');

Route::get('/devis/delete/{id}/type/{type}/{previous}','DevisController@delete')->name('devis-delete-showVoiture');
Route::get('/devis/delete/{id}/type/{type}/','DevisController@delete')->name('devis-delete');


Route::get('/devis/sendmail/{id}','DevisController@sendMail')->name('devis-send-mail');
Route::post('/devis/sendmail/{id}','DevisController@sendDevis');

//devis bosse
Route::get('/devis/createbosse','DevisController@indexCreateBosseFirst')->name('devis-create-bosse-first');
Route::post('/devis/createbosse','DevisController@createBosseFirst');



Route::get('/devis/bosse/{id}','DevisController@getDevisBosse')->name('devis-modify-bosse');
Route::post('/devis/bosse/{id}','DevisController@setDevisBosse');

Route::get('/devis/createbosse/car','DevisController@indexCreateBosseSecond')->name('devis-create-bosse-second');
Route::post('/devis/createbosse/car','DevisController@createBosseSecond');

Route::get('/devis/createbosse/last','DevisController@indexCreateBosseThird')->name('devis-create-bosse-third');
Route::post('/devis/createbosse/last','DevisController@createBosseThird');

//devis horaire
Route::get('/devis/createhoraire','DevisController@indexCreateHoraire')->name('devis-create-horaire');
Route::post('/devis/createhoraire','DevisController@createHoraire');



Route::get('/devis/horaire/{id}','DevisController@getDevisHoraire')->name('devis-modify-horaire');
Route::post('/devis/horaire/{id}','DevisController@setDevisHoraire');

//PDF
Route::get('devis/downloadPdf/{id}/action/{name}', 'DevisController@downloadPdf')->name('devis-pdf');


// Profil
Route::get('/profil','ProfilController@index')->name('profil');
Route::post('/profil','ProfilController@modify');

//Client
Route::get('/client','ClientController@index')->name('client');
//Route::get('/client/{slug}','ClientController@isearch')->name('client-search');


Route::get('/client/create','ClientController@indexCreate')->name('client-create');

Route::get('/client/create/{previous}','ClientController@indexCreate')->name('client-create-voiture');
Route::get('/client/create/{previous}/{m?}','ClientController@indexCreate')->name('client-modify-voiture');

Route::get('/client/create/{route}','ClientController@indexCreateOther')->name('client-create-other');
Route::post('/client/create','ClientController@create');
Route::post('/client/create/{route}','ClientController@create')->name('createfromvoiture');

Route::get('/client/{id}', 'ClientController@getClient')->name('client-get');
Route::post('/client/{id}', 'ClientController@modify');

Route::get('/client/delete/{id}','ClientController@delete')->name('client-delete');

//-----------Voiture----------------------//
Route::get('autocomplete/{id}', 'VoitureController@autocomplete')->name('autocomplete');

Route::get('client/voiture/index', 'VoitureController@index2')->name('voiture-index');
Route::get('client/voiture/search', 'VoitureController@search')->name('voiture-search');
Route::get('client/voiture/show/{voitureId}', 'VoitureController@show')->name('voiture-show');


Route::get('/client/{id}/voiture','VoitureController@index')->name('voiture');

Route::get('/client/{id}/voiture/create','VoitureController@indexCreate')->name('voiture-create');
Route::get('/client/{id}/voiture/create/{previous}','VoitureController@indexCreate')->name('voiture-create-index');
Route::post('/client/{id}/voiture/create','VoitureController@create');

Route::get('/client/{clientId}/voiture/{voitureId}', 'VoitureController@getVoiture')->name('voiture-get');
Route::get('/client/{clientId}/voiture/{voitureId}/{index}', 'VoitureController@getVoiture')->name('voiture-get-index');
Route::post('/client/{clientId}/voiture/{voitureId}', 'VoitureController@modify');

Route::get('/client/{clientId}/delete/{voitureId}','VoitureController@delete')->name('voiture-delete');
Route::get('/client/{clientId}/delete/{voitureId}/{page}','VoitureController@delete')->name('voiture-delete-index');

//----DEVIS----
Route::get('/client/{id}/voiture/devis/createbosseclient/{voiture}','DevisController@indexCreateFirstVoiture')->name('devis-create-first-voiture');
Route::post('/client/{id}/voiture/devis/createbosseclient/{voiture}','DevisController@createBosseFirst');

Route::get('/client/{id}/voiture/devis/createhoraire/{voiture}','DevisController@indexCreateHoraireVoiture')->name('devis-create-horaire-voiture');
Route::post('/client/{id}/voiture/devis/createhoraire/{voiture}','DevisController@createHoraire');

Route::get('/client/{clientId}/voiture/devis/bosse/{id}','DevisController@getDevisBosseVoiture')->name('devis-modify-bosse-voiture');
Route::post('/client/{clientId}/voiture/devis/bosse/{id}','DevisController@setDevisBosse');

Route::get('/client/{clientId}/voiture/devis/horaire/{id}','DevisController@getDevisHoraireVoiture')->name('devis-modify-horaire-voiture');
Route::post('/client/{clientId}/voiture/devis/horaire/{id}','DevisController@setDevisHoraire');

Route::get('/client/{clientId}/voiture/devis/delete/{id}/type/{type}','DevisController@deleteVoiture')->name('devis-delete-voiture');

Route::get('/client/{clientId}/voiture/devis/sendmail/{id}','DevisController@sendMailVoiture')->name('devis-send-mail-voiture');
Route::post('/client/{clientId}/voiture/devis/sendmail/{id}','DevisController@sendDevis');

//---FACTURE---

Route::get('/client/{clientId}/voiture/facture/createdevisfromdevis/{devisId}','FactureController@indexCreateDevisFromDevisVoiture')->name('facture-create-devis-from-devis-voiture');


Route::get('/client/{clientId}/facture/{id}', 'FactureController@getFactureVoiture')->name('facture-get-voiture');
Route::post('/client/{clientId}/facture/{id}', 'FactureController@modify');

Route::get('/client/{clientId}/voiture/facture/delete/{id}/type/{type}','FactureController@deleteVoiture')->name('facture-delete-voiture');

Route::get('/client/{clientId}/voiture/facture/sendmail/{id}/type/{type}','FactureController@sendMailVoiture')->name('facture-send-mail-voiture');
Route::post('/client/{clientId}/voiture/facture/sendmail/{id}/type/{type}','FactureController@sendFacture');

//Facture
Route::get('/facture','FactureController@index')->name('facture');


Route::get('/facture/createlibre','FactureController@indexCreateLibre')->name('facture-create-libre');
Route::post('/facture/createlibre','FactureController@createLibre');

Route::get('/facture/createdevis','FactureController@indexCreateDevis')->name('facture-create-devis');

Route::get('/facture/createdevisfromdevis/{devisId}','FactureController@indexCreateDevisFromDevis')->name('facture-create-devis-from-devis');


Route::get('/facture/{id}', 'FactureController@getFacture')->name('facture-get');
Route::post('/facture/{id}', 'FactureController@modify');
Route::get('/facture/delete/{id}/type/{type}','FactureController@delete')->name('facture-delete');
Route::get('/facture/delete/{id}/type/{type}/{previous}','FactureController@delete')->name('facture-delete-showVoiture');

Route::get('/facture/sendmail/{id}/type/{type}','FactureController@sendMail')->name('facture-send-mail');
Route::post('/facture/sendmail/{id}/type/{type}','FactureController@sendFacture');


//PDF
Route::get('/downloadPdf/{id}/{action}', 'FactureController@downloadPdf')->name('facture-pdf');


// AJAX
Route::get('getDevisByClient/{id}', 'FactureController@getDevisByClient');

Route::get('getVoituresByClient/{id}', 'FactureController@getVoituresByClient');

Route::get('getDevisInformations/{id}', 'FactureController@getDevisInformations');

Route::get('getTauxRemise/{client}', 'FactureController@getTauxRemise');

Route::get('getModeleByMarque/{marque}', 'VoitureController@getModeleByMarque');

Route::get('getAllClient/{userId}', 'ClientController@getAllClientByUser');

//devis
Route::get('/generateTab/{partieVoitureId}/{devisId}/{devisBosseId}', 'DevisController@generateTab');
Route::get('/saveBosse', 'DevisController@saveBosse');

//Logo
Route::get('/addlogo','HomeController@indexAddLogo')->name('add-logo');
Route::post('/addlogo','HomeController@addLogo');



